@extends('admin.layouts.app')

@section('select2-script')
  <!-- Select2 -->
  <link rel="stylesheet" href="{{ asset('AdminLTE-2.4.5/bower_components/select2/dist/css/select2.min.css') }}">
@endsection

@section('head-script')
	<link rel="stylesheet" href="{{asset('AdminLTE-2.4.5/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endsection

@section('end-script')
	<script src="{{asset('AdminLTE-2.4.5/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
	<script src="{{asset('AdminLTE-2.4.5/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
	<script src="{{asset('js/timbangan.js')}}"></script>
	<script type="text/javascript">
        $('.select2').css('width', '100%').select2();
		var table;
		$(function() {
		    table = $('#data-table').DataTable({
		        processing: true,
		        serverSide: true,
		        ajax: '{{$ajax}}',
        		order: [],
		        columns: [
		            { data: 'id', searchable: false, orderable: false},
		            { data: 'employee_id', searchable: false, orderable: false},
		            { data: 'salary', searchable: false, orderable: false},
                    { data: 'status', searchable: false, orderable: false},
		            { data: 'salarydate', searchable: false, orderable: false},
		            { data: 'action', searchable: false, orderable: false}
		        ],
		        columnDefs: [{
		          "targets": 0,
		          "searchable": false,
		          "orderable": false,
		          "data": null,
		          // "title": 'No.',
		          "render": function (data, type, full, meta) {
		              return meta.settings._iDisplayStart + meta.row + 1;
		          }
		        }],
		    });
		});
	</script>
@endsection

@section('content')
<div class="box">
	<div class="box-header with-border">
        <h3 class="box-title">Penggajian :  {{date('F').' - '.date('Y')}}</h3>
        {{-- <a href="#" class="btn btn-success pull-right" data-toggle="modal" data-target="#myModal" style="margin-left: 5px;"><i class="fa fa-fw fa-sliders"></i> Filter</a> --}}
        {{-- <a href="#" class="btn btn-info pull-right" data-toggle="modal" data-target="#myModal"><i class="fa fa-fw fa-download"></i> Download</a> --}}
        {{-- <a href="#" class="btn btn-info pull-right" data-toggle="modal" data-target="#myModal"><i class="fa fa-fw fa-download"></i> Download</a> --}}
	  	{{-- <h3 class="box-title">Title</h3> --}}
        {{-- <a href="{{$create}}" class="btn btn-primary"><i class="fa fa-fw fa-plus"></i> Create</a> --}}
	</div>
	<div class="box-body">
	  	<table id="data-table" class="table table-bordered table-hover">
            <thead>
	            <tr>
	              <th>#</th>
	              <th>Nama</th>
                  <th>Gaji</th>
                  <th>Status</th>
                  <th>Tanggal Gajian</th>
	              <th>Action</th>
	            </tr>
            </thead>
            <tbody>
	        </tbody>
	    </table>
	</div>
</div>
<div class="modal fade" id="myModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Filter Penggajian</h4>
			</div>
			<div class="modal-body">
				<form action="" id="form1" method="POST" class="form-horizontal">
					@csrf
	              <div class="box-body">
	                <div class="form-group">
	                  <label class="col-sm-2 control-label">Year</label>

	                  <div class="col-sm-10">
	                    <select class="form-control select2" name="year" required>
	                    	@foreach(range(2017, date('Y')) as $row)
		                    	<option value="{{$row}}" {{($row==date('Y')) ? 'selected': ''}}>{{ $row }}</option>
	                    	@endforeach
	                    </select>
	                  </div>
	                </div>
	                <div class="form-group">
	                  <label class="col-sm-2 control-label">Month</label>

	                  <div class="col-sm-10">
	                    <select class="select2 form-control" name="month" required>
	                    	@for($i=1; $i <= 12; $i++)
		                    	<option value="{{$i}}" {{($i==date('n')) ? 'selected': ''}}>{{ date('F', strtotime(date('Y').'-'.$i.'-01')) }}</option>
	                    	@endfor
	                    </select>
	                  </div>
	                </div>
	                <div class="form-group">
	                  <label class="col-sm-2 control-label">Karyawan</label>

	                  <div class="col-sm-10">
	                    <select class="select2 form-control" name="employee_id" required>
	                    	@for($i=1; $i <= 12; $i++)
		                    	<option value="{{$i}}" {{($i==date('n')) ? 'selected': ''}}>{{ date('F', strtotime(date('Y').'-'.$i.'-01')) }}</option>
	                    	@endfor
	                    </select>
	                  </div>
	                </div>
	                {{-- <div class="form-group">
	                  <label class="col-sm-2 control-label">Status</label>

	                  <div class="col-sm-10">
	                    <select class="form-control select2" name="status" required>
	                    	<option value="1">DITERIMA</option>
	                    	<option value="2">DITOLAK</option>
	                    	<option value="3">SEDANG PROSES</option>
	                    	<option value="4">SELESAI</option>
	                    	<option value="0">BARU</option>
	                    </select>
	                  </div>
	                </div> --}}
	                {{-- <div class="form-group">
	                  <label class="col-sm-2 control-label">Type</label>
	                  <div class="col-sm-10">
	                    <div class="radio">
		                    <label>
		                      <input type="radio" name="document_type" id="document_type" value="1" checked="">
		                      PDF
		                    </label>
		                </div>
	                    <div class="radio">
		                    <label>
		                      <input type="radio" name="document_type" id="document_type" value="2">
		                      Excel
		                    </label>
		                </div>
	                  </div>
	                </div> --}}
	              </div>
	              <!-- /.box-footer -->
	            </form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
				<button type="submit" form="form1" class="btn btn-primary">Submit</button>
			</div>
		</div>
	<!-- /.modal-content -->
	</div>
<!-- /.modal-dialog -->
</div>
@endsection
