@extends('admin.layouts.app')

@section('end-script')
    <script src="{{ asset('js/timbangan.js') }}"></script>
@endsection
@section('content')
    <div class="box">
        <div class="box-header with-border">
            <a href="{{$url}}" class="btn btn-warning"><i class="fa fa-fw fa-arrow-left"></i> Back</a>
        </div>
        <form method="POST" action="{{$store}}" accept-charset="UTF-8" enctype="multipart/form-data" class="form-horizontal">
        @csrf
            <div class="box-body">
                <div class="form-group">
                    <label class="col-sm-2 control-label">Pemilik</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" name="name" value="{{$about->name}}" data-validation="required">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Perusahaan</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" name="company" value="{{$about->company}}" data-validation="required">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Email</label>
                    <div class="col-sm-8">
                        <input type="email" class="form-control" name="email" value="{{$about->email}}" data-validation="required">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Nomor Telefon</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" name="phone" value="{{$about->phone}}" data-validation="required">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Logo</label>
                    <div class="col-sm-8">
                        <input type="file" class="form-control" name="logo">
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <div class="col-sm-8 col-sm-offset-2">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </form>
    </div>
@endsection
