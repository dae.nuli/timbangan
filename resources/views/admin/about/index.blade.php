@extends('admin.layouts.app')

@section('content')
<div class="row">

<div class="col-md-3">
<div class="box box-primary">
  <div class="box-body box-profile">
      @if(!empty($about->image))
          <img class="profile-user-img img-responsive" style="width:200px" src="{{asset('logo/'.$about->image)}}" alt="User profile picture">
      @else
            <img class="profile-user-img img-responsive img-circle" src="{{asset('AdminLTE-2.4.5/dist/img/avatar3.png')}}" alt="User profile picture">
        @endif
    <h3 class="profile-username text-center"></h3>
    <ul class="list-group list-group-unbordered">
      <li class="list-group-item">
        <b>Pemilik</b> <a class="pull-right">{{$about->name}}</a>
      </li>
      <li class="list-group-item">
        <b>Nama Perusahaan</b> <a class="pull-right">{{$about->company}}</a>
      </li>
      <li class="list-group-item">
        <b>Email</b> <a class="pull-right">{{$about->email}}</a>
      </li>
      <li class="list-group-item">
        <b>Nomor Telefon</b> <a class="pull-right">{{$about->phone}}</a>
      </li>
    </ul>
    @if(auth()->user()->can('about_edit'))
        <a href="{{$edit}}" class="btn btn-primary btn-block"><b>Edit</b></a>
    @endif
  </div>
  <!-- /.box-body -->
</div>
</div>
</div>
@endsection
