@extends('admin.layouts.app')

@section('content')
<div class="box">
	<div class="box-header with-border">
	  	<h3 class="box-title">{{$title}}</h3>
	</div>
	<div class="box-body">
	  	<table id="data-table" class="table table-bordered table-hover">
            <thead>
	            <tr>
					<th style="width:10px">#</th>
					<th>Nama Perkiraan</th>
	            </tr>
            </thead>
            <tbody>
            @foreach($data as $i => $row)
                <tr>
                    <td>{{($i+1)}}</td>
                    <td><b>{{($row->name)}}</b></td>
                </tr>
                @foreach($row->sub as $j => $sub)
                    <tr>
                        <td></td>
                        <td>{{($j+1)}}. {{$sub->name}}</td>
                    </tr>
                @endforeach
            @endforeach
	        </tbody>
	    </table>
	</div>
</div>
@endsection
