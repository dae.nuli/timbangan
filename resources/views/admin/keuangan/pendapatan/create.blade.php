@extends('admin.layouts.app')

@section('select2-script')
  <!-- Select2 -->
  <link rel="stylesheet" href="{{ asset('AdminLTE-2.4.5/bower_components/select2/dist/css/select2.min.css') }}">
@endsection

@section('head-script')
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="{{asset('AdminLTE-2.4.5/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
@endsection

@section('end-script')
<!-- bootstrap datepicker -->
<script src="{{asset('AdminLTE-2.4.5/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{ asset('js/timbangan.js') }}"></script>
<script type="text/javascript">
  $('#datepicker').datepicker({
      format:'yyyy-mm-dd',
      startDate: '0d',
      autoclose: true
  });
</script>
@endsection

@section('content')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="box">
	<div class="box-header with-border">
        <a href="{{$url}}" class="btn btn-warning"><i class="fa fa-fw fa-arrow-left"></i> Back</a>
	</div>
    <form method="POST" action="{{$store}}" accept-charset="UTF-8" class="form-horizontal">
        @csrf
    	<div class="box-body">
            <div class="form-group">
                <label class="col-sm-2 control-label">Pendapatan</label>
                <div class="col-sm-8">
                    {{-- <select class="form-control" v-model="clientID" v-on:change="changeClient">
                        <option disabled value="">- Please Select -</option>
                        <option v-for="client in clients" v-bind:value="client.id">@{{client.name}}</option>
                    </select> --}}
                    <select class="form-control select22" style='width:100%' name="pendapatan_sub_id">
                    <option disabled value="" selected>- Please Select -</option>
                      @foreach($main as $row)
                        <option value="{{$row->id}}" style="color:#000; font-weight:bold" disabled>{{$row->name}}</option>
                        @foreach($row->sub as $sub)
                            <option value="{{$sub->id}}">{{$sub->name}}</option>
                        @endforeach
                      @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Tanggal</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" name="date" data-validation="required" id="datepicker">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Jumlah Uang</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control number" name="amount" data-validation="required">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Keterangan</label>
                <div class="col-sm-8">
                    <textarea class="form-control" name="note"></textarea>
                </div>
            </div>
    	</div>
    	<div class="box-footer">
    		<div class="col-sm-8 col-sm-offset-2">
            <button type="submit" class="btn btn-primary">Submit</button>
    		</div>
    	</div>
    </form>
</div>
@endsection
