@extends('admin.layouts.app')

@section('select2-script')
  <!-- Select2 -->
  <link rel="stylesheet" href="{{ asset('AdminLTE-2.4.5/bower_components/select2/dist/css/select2.min.css') }}">
@endsection

@section('end-script')
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script src="{{ asset('js/timbangan.js') }}"></script>

    <script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var app = new Vue({
        el: "#app-tbs",
        data: {
            tara: null,
            potongan_wajib: null,
            potongan_tangkai: null,
            potongan_air: null,
            potongan_sampah: null,
            potongan_pasir: null,
            potongan_mutu: null,
            bruto: {{$tbs->bruto}},
            price: {{$tbs->item_price}},
            // price:null,
            readonly: 0
        },
        computed: {
            netto: function() {
                return (this.tara != null) ? (this.bruto - this.tara) : null;
            },
            potongan: function() {
                return Math.round(((this.potongan_wajib*this.netto)/100) + ((this.potongan_tangkai*this.netto)/100) + ((this.potongan_air*this.netto)/100) + ((this.potongan_sampah*this.netto)/100) + ((this.potongan_pasir*this.netto)/100) + ((this.potongan_mutu*this.netto)/100));
            },
            berat_bersih: function() {
                return (this.netto - this.potongan);
            },
            total: function() {
                return this.$options.filters.currency(this.price*this.berat_bersih);
            }
        },
        mounted: function () {
            // axios.get("{{$api_product_price}}").then(response => (this.price = response.data))
        },
        methods: {
            getTara: function () {
                this.readonly = 1;
                axios.get("{{$api_get_tara}}").then(response => {
                    this.tara = response.data
                })
            }
        },
        filters: {
            currency(amount) {
              // let val = (value/1).toFixed(2).replace('.', ',')
              return 'Rp '+amount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
              // return 'Rp '+numeral(amount).format('0,0');
            }
        }

    });
    $(document).on('change', '.clients', function() {
      $.ajax({
        type: "POST",
        url: "{{$api_transports}}",
        data: {
          client: $(".clients").val()
        },
        success: function(data) {
          $('.truck-number').html(data);
        }
      })
    });
    // $(document).on('click', '.fetchWeight', function() {
    //   $.ajax({
    //     type: "GET",
    //     url: "{{$api_weight}}",
    //     success: function(data) {
    //       $('input[name=bruto]').val(data);
    //       $('input[name=tara]').val(data);
    //       $('input[name=netto]').val(data);
    //     }
    //   })
    // })
    </script>
@endsection

@section('content')
    <form method="POST" id="app-tbs" action="{{$store}}" accept-charset="UTF-8" class="form-horizontal">
    @csrf
<div class="row">
<div class="col-md-7">
    <div class="box">
        {{-- <div class="row"> --}}
            {{-- <div class="col-md-5"> --}}
                <div class="box-header with-border">
                    <a href="{{$url}}" class="btn btn-warning"><i class="fa fa-fw fa-arrow-left"></i> Back</a>
                    {{-- <a href="javascript:void(0)" class="btn btn-success" v-on:click="getTara"><i class="fa fa-fw fa-cloud-download"></i> Get Tara</a> --}}
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Suplier</label>
                                <div class="col-sm-7">
                                    <input class="form-control" disabled value="{{$tbs->client->name}}">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-sm-4 control-label">No. Plat</label>
                                <div class="col-sm-7">
                                    <input class="form-control" disabled value="{{$tbs->transports->plate_number}}">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Sopir</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" disabled value="{{$tbs->driver_name}}">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Product</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" disabled value="{{$tbs->item_name}}">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Bruto</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" disabled v-model="bruto" name="bruto" data-validation="required">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Tara</label>
                                <div class="col-sm-7">
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="tara" v-model="tara" required readonly>
                                        <span class="input-group-btn">
                                          <button type="button" v-on:click="getTara" class="btn btn-success btn-flat"><i class="fa fa-fw fa-cloud-download"></i></button>
                                        </span>
                                    </div>
                                    {{-- <input type="text" class="form-control" v-model="tara" name="tara" data-validation="required" readonly> --}}
                                    {{-- <input type="text" class="form-control" v-model="tara" name="tara" data-validation="required" :readonly="readonly == 1 ? true : false"> --}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Netto</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" v-model="netto" name="netto" data-validation="required" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Potongan</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" readonly v-model="potongan" name="potongan">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-sm-5 control-label">Berat Bersih</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" readonly v-model="berat_bersih" name="berat_bersih">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Total</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" readonly v-model="total">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Keterangan</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" value="{{$tbs->note}}" name="note">
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="col-sm-8 col-sm-offset-2">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </div>
            </div>
            {{-- </div> --}}
            <div class="col-md-5">
            <div class="box">
                <div class="box-header with-border">
                    <h4>Potongan</h4>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-sm-6 control-label">Wajib <small>(%)</small></label>
                                <div class="col-sm-5">
                                    <input type="number" class="form-control" v-model="potongan_wajib" name="potongan_wajib">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-6 control-label">Tangkai <small>(%)</small></label>
                                <div class="col-sm-5">
                                    <input type="number" class="form-control" v-model="potongan_tangkai" name="potongan_tangkai">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-6 control-label">Air <small>(%)</small></label>
                                <div class="col-sm-5">
                                    <input type="number" class="form-control" v-model="potongan_air" name="potongan_air">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-sm-6 control-label">Sampah <small>(%)</small></label>
                                <div class="col-sm-5">
                                    <input type="number" class="form-control" v-model="potongan_sampah" name="potongan_sampah">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-6 control-label">Pasir <small>(%)</small></label>
                                <div class="col-sm-5">
                                    <input type="number" class="form-control" v-model="potongan_pasir" name="potongan_pasir">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-6 control-label">Mutu <small>(%)</small></label>
                                <div class="col-sm-5">
                                    <input type="number" class="form-control" v-model="potongan_mutu" name="potongan_mutu">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
        {{-- </div> --}}
    <div class="row">
        <div class="col-md-12">
            {{-- <div class="box-footer">
                <div class="col-sm-8 col-sm-offset-2">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div> --}}
        </div>
    </div>
</form>
@endsection
