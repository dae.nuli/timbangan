@extends('admin.layouts.app')

@section('select2-script')
  <!-- Select2 -->
  <link rel="stylesheet" href="{{ asset('AdminLTE-2.4.5/bower_components/select2/dist/css/select2.min.css') }}">
@endsection


@section('head-script')
	<link rel="stylesheet" href="{{asset('AdminLTE-2.4.5/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">

    <link rel="stylesheet" href="{{asset('AdminLTE-2.4.5/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css')}}" />
@endsection

@section('end-script')
    <script type="text/javascript" src="{{asset('AdminLTE-2.4.5/bower_components/moment/min/moment.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('AdminLTE-2.4.5/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js')}}"></script>

	<script src="{{asset('AdminLTE-2.4.5/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
	<script src="{{asset('AdminLTE-2.4.5/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
	<script src="{{asset('js/timbangan.js')}}"></script>
	<script type="text/javascript">
      // $('#datepicker').datetimepicker({
      //     format:'yyyy-mm-dd hh:ii',
      //     startDate: '0d',
      //     autoclose: true
      // });
      $(document).on('click','.close-modal',function(e){
        $(".pay-conf")[0].reset();
        $("input[name='id']").val('');
        $('.pay-conf').attr('data-action', null);
        // e.preventDefault();
      });
    $('#datetimepicker1').datetimepicker({
        format: 'YYYY-MM-DD HH:mm'
    });
    $(document).on('click', '.payment-button',function() {
        var ID = $(this).data('id');
        var URLPayment = '{{$urlCheckPayment}}';
        var URL = $(this).data('url');
        var CSRF_TOKEN = $('input[name="_token"]').attr('value');
        $.ajax({
            url: URLPayment,
            type: 'POST',
            dataType: 'json',
            data: {_token: CSRF_TOKEN, id: ID },
            success: function(data) {
                // console.log(data);
              // console.log($('input[name="id"]').val());
              if (data.payment_status != 1) {
                  $('#myModal').modal({backdrop: 'static', keyboard: false, show:true});
                  $('.pay-conf').attr('data-action', URL);
                  $('input[name="id"]').val(ID);
                  console.log('PB : '+URL);
              }
            }
        });
        // $('.payment-date').focus();
        // $.getJSON( $(this).data('url'), function( data ) {
        //   var items = [];
        //   // $.each( data, function( key, val ) {
        //   //   items.push( "<li id='" + key + "'>" + val + "</li>" );
        //   // });
        //   console.log(data);
        //
        //   // $( "<ul/>", {
        //   //   "class": "my-new-list",
        //   //   html: items.join( "" )
        //   // }).appendTo( "body" );
        // });
    });

    $(document).on('click', '.submit-payment',function( event ) {
    // $('.pay-conf').submit(function( event ) {
        event.preventDefault();
        var CSRF_TOKEN = $('input[name="_token"]').attr('value');
        var paymentDate = $("input[name='payment_date']").val();
        var paymentAmount = $("input[name='amount']").val();
        var paymentType = $("input[name='payment_type']:checked").val();
        var bank = $(".bank-id").val();
        var id = $('input[name="id"]').val();
        var url = '{{$urls}}/'+id;
        console.log('Modal : '+url);
        console.log('ID : '+id);

        if (paymentDate !="" && paymentAmount !="" && paymentType !="") {
            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'json',
                data: {_token: CSRF_TOKEN, date: paymentDate, amount: paymentAmount, type: paymentType, bank_account_id: bank },
                success: function(data) {
                    // console.log(data);
                  // console.log($('input[name="id"]').val());
                  if (data.payment_status == 1) {
                      $('[data-payment="'+id+'"]').html('<small class="label bg-green">PAID</small></span>');
                  } else {
                      $('[data-payment="'+id+'"]').html('<small class="label bg-red">UNPAID</small>');
                  }
                  $('.pay-conf').attr('data-action', null);
                  $(".pay-conf")[0].reset();
                  $("input[name='id']").val('');
                }
            }).done(function( data ) {
                $('#myModal').modal('hide');
            });
        } else {
            alert('Complete the form')
        }
    })

		$('.select2').css('width', '100%').select2();
		var table;
		$(function() {
		    table = $('#data-table').DataTable({
		        processing: true,
		        serverSide: true,
		        ajax: '{{$ajax}}',
        		order: [[7,'desc']],
		        columns: [
		            { data: 'id', searchable: false, orderable: false},
		            { data: 'client_id', searchable: false, orderable: false},
		            { data: 'transports_id', searchable: false, orderable: false},
                    { data: 'driver_name', searchable: false, orderable: false},
					{ data: 'total', searchable: false, orderable: false},
                    { data: 'status', searchable: false, orderable: false},
		            { data: 'payment_status', searchable: false, orderable: false},
		            { data: 'created_at', searchable: false, orderable: true},
		            { data: 'action', searchable: false, orderable: false}
		        ],
		        columnDefs: [{
		          "targets": 0,
		          "searchable": false,
		          "orderable": false,
		          "data": null,
		          // "title": 'No.',
		          "render": function (data, type, full, meta) {
		              return meta.settings._iDisplayStart + meta.row + 1;
		          }
		        }],
		    });
		});
	</script>
@endsection

@section('content')
<div class="box">
	<div class="box-header with-border">
	  	{{-- <h3 class="box-title">Title</h3> --}}
		@if(auth()->user()->can('tbs_create'))
	        <a href="{{$create}}" class="btn btn-primary"><i class="fa fa-fw fa-plus"></i> Create</a>
		@endif
        {{-- <a href="#" class="btn btn-success pull-right" data-toggle="modal" data-target="#myModalTwo" style="margin-left: 5px;"><i class="fa fa-fw fa-sliders"></i> Filter</a>
        <a href="#" class="btn btn-info pull-right" data-toggle="modal" data-target="#myModal"><i class="fa fa-fw fa-download"></i> Download</a> --}}
	</div>
	<div class="box-body">
	  	<table id="data-table" class="table table-bordered table-hover">
            <thead>
	            <tr>
	              <th>#</th>
	              <th>Pelanggan</th>
	              <th>Nomor Mobil</th>
				  <th>Nama Sopir</th>
                  <th>Total</th>
                  <th>Status</th>
	              <th>Pembayaran</th>
	              <th>Date</th>
	              <th>Action</th>
	            </tr>
            </thead>
            <tbody>
	        </tbody>
	    </table>
	</div>
</div>

<div class="modal fade" id="myModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Payment Confirmation</h4>
			</div>
			<div class="modal-body">
				<form method="POST" class="form-horizontal pay-conf">
					@csrf
                <input type="hidden" name="id" value="">
	              <div class="box-body">
	                <div class="form-group">
	                  <label class="col-sm-4 control-label">Tanggal Pembayaran</label>

	                  <div class="col-sm-7">
	                    <input type="text" class="form-control payment-date" name="payment_date" id="datetimepicker1"/>
	                  </div>
	                </div>

  	                <div class="form-group">
  	                  <label class="col-sm-4 control-label">Jumlah Pembayaran</label>

  	                  <div class="col-sm-7">
  	                    <input class="form-control number total-payment" name="amount"/>
  	                  </div>
  	                </div>

  	                <div class="form-group">
  	                  <label class="col-sm-4 control-label">Bank Tujuan</label>

  	                  <div class="col-sm-7">
	                    <select class="form-control bank-id" name="bank_id" required>
                            {{-- <option value="">- Silahkan Pilih -</option> --}}
	                    	@foreach($bank as $row)
		                    	<option value="{{$row->id}}"><small>{{ $row->bank->name .' - ( '. $row->number.' )' }}</small></option>
	                    	@endforeach
	                    </select>
  	                  </div>
  	                </div>

	                <div class="form-group">
	                  <label class="col-sm-4 control-label">Type</label>
	                  <div class="col-sm-7">
	                    <div class="radio">
		                    <label>
		                      <input type="radio" name="payment_type" class="payment_type" value="1" checked="">
		                      Cash
		                    </label>
		                </div>
	                    <div class="radio">
		                    <label>
		                      <input type="radio" name="payment_type" class="payment_type" value="2">
		                      Transfer
		                    </label>
		                </div>
	                  </div>
	                </div>
	              </div>
	            </form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-left close-modal" data-dismiss="modal">Close</button>
				<button type="submit" form="form1" class="btn btn-primary submit-payment">Submit</button>
			</div>
		</div>
	</div>
</div>
@endsection
