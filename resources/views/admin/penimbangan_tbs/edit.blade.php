@extends('admin.layouts.app')

@section('select2-script')
  <!-- Select2 -->
  <link rel="stylesheet" href="{{ asset('AdminLTE-2.4.5/bower_components/select2/dist/css/select2.min.css') }}">
@endsection

@section('end-script')
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script src="{{ asset('js/timbangan.js') }}"></script>

    <script type="text/javascript">
    var apiClient = "{{$clients}}";
    var apiEdit = "{{$apiEdit}}";
    var apiTransport = "{{$transports}}";
    var listProduct = "{{$products}}";
    var clientID = {{$clientID}};
    var app = new Vue({
        el: "#app-tbs",
        data() {
            return {
                clients: [],
                transports: [],
                products: [],
                info: {
                    client_id: '',
                    transports_id: '',
                    products_id: '',
                    driver_name: '',
                    bruto: null,
                    tara: null,
                    netto: null,
                    potongan: null,
                    berat_bersih: null,
                    note: null,
                    potongan_wajib: null,
                    potongan_tangkai: null,
                    potongan_air: null,
                    potongan_sampah: null,
                    potongan_pasir: null,
                    potongan_mutu: null,
                    potongan_mentah: null,
                    potongan_tankos: null,
                    potongan_brondol: null,
                    potongan_busuk: null,
                    other_potongan: null,
                    potongan_dura: null,
                    berat_tandan: null,
                    jumlah_tandan: null,
                    // method:'PUT'
                }
            }
        },
        created: function() {
            this.fetchEdit();
            this.fetchClient();
            this.fetchProduct();
            this.fetchTransport();
        },
        // afterCreate: function() {
        //     this.fetchTransport();
        // },
        methods: {
            fetchClient: function () {
                axios.get(apiClient).then(response => {
                    this.clients = response.data
                });
            },
            fetchEdit: function () {
                axios.get(apiEdit).then(response => {
                    this.info = response.data;
                    // this.client_id = response.data.client_id;
                });
                // console.log(this.info.client_id);
            },
            fetchTransport: function () {
                axios.get(apiTransport+'/'+clientID).then(response => {
                    this.transports = response.data
                });
                // console.log(this.info);
            },
            fetchProduct: function () {
                axios.get(listProduct).then(response => {
                    this.products = response.data
                });
            },
            changeClient: function () {
                this.info.transports_id = ''
                axios.get(apiTransport+'/'+this.info.client_id).then(response => {
                    this.transports = response.data
                });
            },
            processForm: function() {
                // let xxx = this.info;
                // console.log(xxx.append('_method', 'PUT'));
                axios.put('/tbs/'+this.info.id, this.info).then(function (response) {
                    // console.log(response);
                    window.location.replace("{{url('tbs')}}");
                });
            }
        }

    })
    </script>
@endsection

@section('content')
<form method="POST" id="app-tbs" action="#" accept-charset="UTF-8" class="form-horizontal" @submit.prevent="processForm">
@csrf
    <div class="box">
        <div class="row">
            <div class="col-md-5">
                <div class="box-header with-border">
                    <a href="{{$url}}" class="btn btn-warning"><i class="fa fa-fw fa-arrow-left"></i> Back</a>
                </div>
                <div class="box-body">
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Client Name</label>
                        <div class="col-sm-8">
                            <select class="form-control" v-model="info.client_id" v-on:change="changeClient">
                                <option disabled value="">- Please Select -</option>
                                <option v-for="client in clients" v-bind:value="client.id">@{{client.name}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Truck Number</label>
                        <div class="col-sm-8">
                            <select class="form-control" v-model="info.transports_id">
                                <option disabled value="">- Please Select -</option>
                                <option v-for="transport in transports" v-bind:value="transport.id">@{{transport.plate_number}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Driver Name</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" v-model="info.driver_name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Product</label>
                        <div class="col-sm-8">
                            <select class="form-control" v-model="info.products_id">
                                <option disabled value="">- Select Product -</option>
                                <option v-for="product in products" v-bind:value="product.id">@{{product.name}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Bruto</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" v-model="info.bruto">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Tara</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" v-model="info.tara">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Netto</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" v-model="info.netto">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Potongan</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" v-model="info.potongan">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Berat Bersih</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" v-model="info.berat_bersih">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Keterangan</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" v-model="info.note">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-7">
                <div class="box-header with-border">
                    <h4>Potongan</h4>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-sm-5 control-label">Wajib <small>(%)</small></label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" v-model="info.potongan_wajib">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-5 control-label">Tangkai <small>(%)</small></label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" v-model="info.potongan_tangkai">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-5 control-label">Air <small>(%)</small></label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" v-model="info.potongan_air">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-sm-5 control-label">Sampah <small>(%)</small></label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" v-model="info.potongan_sampah">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-5 control-label">Pasir <small>(%)</small></label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" v-model="info.potongan_pasir">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-5 control-label">Mutu <small>(%)</small></label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" v-model="info.potongan_mutu">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">

                        <div class="box-header with-border">
                            <h4>Pulangan</h4>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-sm-5 control-label">Mentah <small>(Kg)</small></label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" v-model="info.potongan_mentah">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-5 control-label">Tankos <small>(Kg)</small></label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" v-model="info.potongan_tankos">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-5 control-label">Brondol <small>(%)</small></label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" v-model="info.potongan_brondol">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-sm-5 control-label">Busuk <small>(Kg)</small></label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" v-model="info.potongan_busuk">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-5 control-label">Lainnya <small>(Kg)</small></label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" v-model="info.other_potongan">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-5 control-label">Dura <small>(%)</small></label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" v-model="info.potongan_dura">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-5 control-label">Berat Tandan</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" v-model="info.berat_tandan">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-5 control-label">Jumlah Tandan</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" v-model="info.jumlah_tandan">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box-footer">
                <div class="col-sm-8 col-sm-offset-2">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection