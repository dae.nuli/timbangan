@extends('admin.layouts.app')

@section('body')
<body onload="window.print();">
<div class="wrapper">
  <!-- Main content -->
  <section class="invoice">
    <!-- title row -->
    <div class="row">
      <div class="col-xs-12">
        <h2 class="page-header">
          <i class="fa fa-globe"></i> {{$about->company}}
          <small class="pull-right">Tanggal Masuk : {{$tbs->created_at->format('d F Y')}}</small>
        </h2>
      </div>
      <!-- /.col -->
    </div>
    <!-- info row -->
    <div class="row invoice-info">
      <div class="col-sm-4 invoice-col">
        Perusahaan
        <address>
          <strong>{{$about->name}}</strong><br>
          Telp: {{$about->phone}}<br>
          Email: {{$about->email}}<br>
          Alamat: {{$about->email}}<br>
        </address>
      </div>
      <!-- /.col -->
      <div class="col-sm-4 invoice-col">
        Pelanggan
        <address>
          <strong>{{$tbs->client->name}}</strong><br>
          Nomor Mobil: {{$tbs->transports->plate_number}}<br>
          Nama Sopir: {{$tbs->driver_name}}
        </address>
      </div>
      <!-- /.col -->
      <div class="col-sm-4 invoice-col">
        <b>Invoice #007612</b><br>
        <br>
        <b>Order ID:</b> 4F3S8J<br>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->

    <!-- Table row -->
    <div class="row">
      <div class="col-xs-12 table-responsive">
        <table class="table table-striped">
          <thead>
              <tr>
                <th>Qty</th>
                <th>Harga</th>
                <th>Produk</th>
                <th>Subtotal</th>
              </tr>
          </thead>
          <tbody>
              <?php
                  $total = ($tbs->netto * $tbs->item_price);
                  // $total = ($tbs->netto * $tbs->products->price);
              ?>
              <tr>
                <td>{{$tbs->netto}} Kg</td>
                <td>Rp {{number_format($tbs->item_price, 0, "", ".")}}</td>
                <td>{{$tbs->item_name}}</td>
                <td>Rp {{number_format($total, 0, "", ".")}}</td>
              </tr>
          </tbody>
        </table>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
    <div class="row">
      <!-- accepted payments column -->
      <div class="col-xs-6">
        {{-- <p class="lead">Payment Methods:</p>
        <img src="{{asset('AdminLTE-2.4.5/dist/img/credit/visa.png')}}" alt="Visa">
        <img src="{{asset('AdminLTE-2.4.5/dist/img/credit/mastercard.png')}}" alt="Mastercard">
        <img src="{{asset('AdminLTE-2.4.5/dist/img/credit/american-express.png')}}" alt="American Express">
        <img src="{{asset('AdminLTE-2.4.5/dist/img/credit/paypal2.png')}}" alt="Paypal"> --}}

        {{-- <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
          Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, weebly ning heekya handango imeem plugg dopplr
          jibjab, movity jajah plickers sifteo edmodo ifttt zimbra.
        </p> --}}
      </div>
      <!-- /.col -->
      <div class="col-xs-6">
        {{-- <p class="lead">Amount Due 2/22/2014</p> --}}

        {{-- <div class="table-responsive">
          <table class="table">
            <tr>
              <th style="width:50%">Subtotal:</th>
              <td>Rp {{number_format($total, 0, "", ".")}}</td>
            </tr>
            <tr>
              <th>Potongan ({{$tbs->potongan}} Kg)</th>
              <td>Rp {{number_format($tbs->potongan*$tbs->item_price, 0, "", ".")}}</td>
            </tr>
            <tr>
              <th>Total:</th>
              <td>Rp {{number_format($tbs->total, 0, "", ".")}}</td>
            </tr>
          </table>
        </div> --}}
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- ./wrapper -->
</body>
@endsection
