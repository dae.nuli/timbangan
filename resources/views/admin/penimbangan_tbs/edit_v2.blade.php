@extends('admin.layouts.app')

@section('select2-script')
  <!-- Select2 -->
  <link rel="stylesheet" href="{{ asset('AdminLTE-2.4.5/bower_components/select2/dist/css/select2.min.css') }}">
@endsection

@section('end-script')
    {{-- <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script> --}}
    {{-- <script src="https://unpkg.com/axios/dist/axios.min.js"></script> --}}
    <script src="{{ asset('js/timbangan.js') }}"></script>

    <script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $(document).on('change', '.clients', function() {
      $.ajax({
        type: "POST",
        url: "{{$api_transports}}",
        data: {
          client: $(".clients").val()
        },
        success: function(data) {
          $('.truck-number').html(data);
        }
      })
    });
    $(document).on('click', '.fetchWeight', function() {
      $.ajax({
        type: "GET",
        url: "{{$api_weight}}",
        success: function(data) {
          $('input[name=bruto]').val(data);
          $('input[name=tara]').val(data);
          $('input[name=netto]').val(data);
        }
      })
    })
    </script>
@endsection

@section('content')
<form method="POST" action="{{$store}}" accept-charset="UTF-8" class="form-horizontal">
@method('PUT')
@csrf
    <div class="box">
        <div class="row">
            <div class="col-md-5">
                <div class="box-header with-border">
                    <a href="{{$url}}" class="btn btn-warning"><i class="fa fa-fw fa-arrow-left"></i> Back</a>
                    {{-- <a href="javascript:void(0)" class="btn btn-success fetchWeight"><i class="fa fa-fw fa-cloud-download"></i> Get Weight</a> --}}
                </div>
                <div class="box-body">
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Client Name</label>
                        <div class="col-sm-8">
                            <select class="form-control select2 clients" name="client_id" data-validation="required">
                              <option value="">- Please Select -</option>
                              @foreach($clients as $client)
                                <option value="{{$client->id}}" {{($tbs->client_id == $client->id) ? 'selected' : ''}}>{{$client->name}}</option>
                              @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Truck Number</label>
                        <div class="col-sm-8">
                            <select class="form-control select2 truck-number" name="transports_id" data-validation="required">
                                <option value="">- Please Select -</option>
                                @foreach($transports as $transport)
                                  <option value="{{$transport->id}}" {{($tbs->transports_id == $transport->id) ? 'selected' : ''}}>{{$transport->plate_number}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Driver Name</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="driver_name" value="{{$tbs->driver_name}}" data-validation="required">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Product</label>
                        <div class="col-sm-8">
                            <select class="form-control select2" name="products_id" data-validation="required">
                                <option value="">- Please Select -</option>
                                @foreach($products as $product)
                                  <option value="{{$product->id}}" {{($tbs->products_id == $product->id) ? 'selected' : ''}}>{{$product->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Bruto</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" value="{{$tbs->bruto}}" name="bruto" data-validation="required">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Tara</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" value="{{$tbs->tara}}" name="tara" data-validation="required">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Netto</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" value="{{$tbs->netto}}" name="netto" data-validation="required">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Potongan</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" value="{{$tbs->potongan}}" name="potongan">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Berat Bersih</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" value="{{$tbs->berat_bersih}}" name="berat_bersih">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Keterangan</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" value="{{$tbs->note}}" name="note">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-7">
                <div class="box-header with-border">
                    <h4>Potongan</h4>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-sm-5 control-label">Wajib <small>(%)</small></label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" value="{{$tbs->potongan_wajib}}" name="potongan_wajib">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-5 control-label">Tangkai <small>(%)</small></label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" value="{{$tbs->potongan_tangkai}}" name="potongan_tangkai">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-5 control-label">Air <small>(%)</small></label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" value="{{$tbs->potongan_air}}" name="potongan_air">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-sm-5 control-label">Sampah <small>(%)</small></label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" value="{{$tbs->potongan_sampah}}" name="potongan_sampah">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-5 control-label">Pasir <small>(%)</small></label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" value="{{$tbs->potongan_pasir}}" name="potongan_pasir">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-5 control-label">Mutu <small>(%)</small></label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" value="{{$tbs->potongan_mutu}}" name="potongan_mutu">
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- <div class="row">

                        <div class="box-header with-border">
                            <h4>Pulangan</h4>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-sm-5 control-label">Mentah <small>(Kg)</small></label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="potongan_mentah" value="{{$tbs->potongan_mentah}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-5 control-label">Tankos <small>(Kg)</small></label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="potongan_tankos" value="{{$tbs->potongan_tankos}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-5 control-label">Brondol <small>(%)</small></label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="potongan_brondol" value="{{$tbs->potongan_brondol}}">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-sm-5 control-label">Busuk <small>(Kg)</small></label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="potongan_busuk" value="{{$tbs->potongan_busuk}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-5 control-label">Lainnya <small>(Kg)</small></label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="other_potongan" value="{{$tbs->other_potongan}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-5 control-label">Dura <small>(%)</small></label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="potongan_dura" value="{{$tbs->potongan_dura}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-5 control-label">Berat Tandan</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="berat_tandan" value="{{$tbs->berat_tandan}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-5 control-label">Jumlah Tandan</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="jumlah_tandan" value="{{$tbs->jumlah_tandan}}">
                                </div>
                            </div>
                        </div>
                    </div> --}}
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box-footer">
                <div class="col-sm-8 col-sm-offset-2">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection
