@extends('admin.layouts.app')

@section('select2-script')
  <!-- Select2 -->
  <link rel="stylesheet" href="{{ asset('AdminLTE-2.4.5/bower_components/select2/dist/css/select2.min.css') }}">
@endsection

@section('end-script')
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script src="{{ asset('js/timbangan.js') }}"></script>

    <script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var app = new Vue({
        el: "#app-tbs",
        data: {
            transports: [],
            products: '',
            bruto: null,
            disc: 0,
            clientID: '',
            transports_id: ''
            // readonly: 0
        },
        methods: {
            getBruto: function () {
                // this.readonly = 1;
                axios.post("{{$api_weight}}", {
                    user_id: this.clientID,
                    product_id: this.products,
                    disc: this.bruto_disc
                }).then(response => {
                    this.bruto = response.data
                })
            },
            getDiscount: function (client) {
                axios.post("{{$api_discount}}", {
                    id: client
                }).then(response => {
                    this.disc = response.data
                })
            },
            changeClient: function () {
                this.transports_id = ''
                this.getDiscount(this.clientID);
                axios.post("{{$api_transports}}", {
                    client: this.clientID
                }).then(response => {
                    this.transports = response.data
                });
            },
        },
        computed: {
            discount: function () {
                return (this.disc != 0)? this.disc : 0;
            },
            bruto_disc: function () {
                return (this.bruto != null) ? Math.round((this.disc*this.bruto)/100) : this.bruto;
            }

        }
    });
    // $('.select2').select2();
    // $(document).on('change', '.clients', function() {
    //   $.ajax({
    //     type: "POST",
    //     url: "{{$api_transports}}",
    //     data: {
    //       client: $(".clients").val()
    //     },
    //     success: function(data) {
    //       $('.truck-number').html(data);
    //     }
    //   })
    // });
    // $(document).on('click', '.fetchWeight', function() {
    //   $.ajax({
    //     type: "GET",
    //     url: "{{$api_weight}}",
    //     success: function(data) {
    //       $('input[name=bruto]').val(data);
    //     }
    //   })
    // })
    </script>
@endsection

@section('content')
<div class="box" id="app-tbs">
    <div class="box-header with-border">
        <a href="{{$url}}" class="btn btn-warning"><i class="fa fa-fw fa-arrow-left"></i> Back</a>
        {{-- <a href="javascript:void(0)" class="btn btn-success" v-on:click="getBruto"></a> --}}
    </div>
    <form method="POST" action="{{$store}}" accept-charset="UTF-8" class="form-horizontal">
    @csrf
        <div class="box-body">
            <div class="form-group">
                <label class="col-sm-2 control-label">Nama Suplier</label>
                <div class="col-sm-8">
                    {{-- <select class="form-control" v-model="clientID" v-on:change="changeClient">
                        <option disabled value="">- Please Select -</option>
                        <option v-for="client in clients" v-bind:value="client.id">@{{client.name}}</option>
                    </select> --}}
                    <select class="form-control select22 clients" style='width:100%' name="client_id" required v-on:change="changeClient" v-model="clientID">
                    <option disabled value="">- Please Select -</option>
                      {{-- <option value="">- Please Select -</option> --}}
                      @foreach($clients as $client)
                        <option value="{{$client->id}}">{{$client->name}}</option>
                      @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Nomor Plat</label>
                <div class="col-sm-8">
                        <select class="form-control" name="transports_id" v-model="transports_id">
                            <option disabled value="">- Please Select -</option>
                            <option v-for="transport in transports" v-bind:value="transport.id">@{{transport.plate_number}}</option>
                        </select>
                    {{-- <select class="form-control select22 truck-number" style="width:100%" name="transports_id" data-validation="required">
                        <option value="">- Please Select -</option> --}}
                        {{-- @foreach($transports as $transport)
                          <option value="{{$transport->id}}">{{$transport->plate_number}}</option>
                        @endforeach --}}
                    {{-- </select> --}}
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Diskon (%)</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" name="client_discount" v-model="discount" readonly>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Nama Sopir</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" name="driver_name" required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Product</label>
                <div class="col-sm-8">
                    <select class="form-control select22" style="width:100%" name="products_id" v-model="products" required>
                        <option disabled value="">- Please Select -</option>
                        {{-- <option value="">- Please Select -</option> --}}
                        @foreach($products as $product)
                          <option value="{{$product->id}}">{{$product->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Bruto (KG)</label>
                <div class="col-sm-8">
                    <div class="input-group">
                        <input type="text" class="form-control" v-model="bruto" required readonly>
                        <span class="input-group-btn">
                          <button type="button" v-on:click="getBruto" class="btn btn-success btn-flat"><i class="fa fa-fw fa-cloud-download"></i> Get Bruto</button>
                        </span>
                    </div>
                    {{-- <input type="text" class="form-control" name="bruto" v-model="bruto" data-validation="required" readonly> --}}
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Bruto Setelah Diskon</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" name="bruto" v-model="bruto_disc" required readonly>
                    {{-- <input type="text" class="form-control" name="bruto" v-model="bruto" data-validation="required" readonly> --}}
                </div>
            </div>
        </div>
        <div class="box-footer">
            <div class="col-sm-8 col-sm-offset-2">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </form>
</div>
@endsection
