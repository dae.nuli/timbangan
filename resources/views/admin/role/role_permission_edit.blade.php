<div class="form-group">
  <label for="permission" class="col-sm-2 control-label">Permission</label>
  <div class="col-sm-8">
    {{-- {{dd($options['selectedPermission'])}} --}}
    <select multiple="multiple" size="10" name="permission[]">
      @foreach($options['value'] as $id => $row)
        <option value="{{$id}}" {{in_array($id, $options['selectedPermission']) ? 'selected' : ''}}>{{$row}}</option>
      @endforeach

{{--       <option value="option1">Option 1</option>
      <option value="option2">Option 2</option>
      <option value="option3" selected="selected">Option 3</option>
      <option value="option4">Option 4</option>
      <option value="option5">Option 5</option>
      <option value="option6" selected="selected">Option 6</option>
      <option value="option7">Option 7</option>
      <option value="option8">Option 8</option>
      <option value="option9">Option 9</option>
      <option value="option0">Option 10</option> --}}
    </select>
  </div>
</div>
