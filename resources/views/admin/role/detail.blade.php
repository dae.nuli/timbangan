@extends('admin.layouts.app')

@section('head-script')
  <link rel="stylesheet" type="text/css" href="{{asset('AdminLTE-2.4.5/plugins/bootstrap-duallistbox-master/src/bootstrap-duallistbox.css')}}">
@endsection

@section('end-script')
  <script src="{{asset('AdminLTE-2.4.5/plugins/bootstrap-duallistbox-master/src/jquery.bootstrap-duallistbox.js')}}"></script>
  <script type="text/javascript">
    $( ".status" ).wrapAll( "<div class='col-sm-8'></div>");
    $( ".status" ).find('[name="status"]').css('margin-left','0');

    var demo1 = $('select[name="permission[]"]').bootstrapDualListbox({
      nonSelectedListLabel: 'Non Selected Permission',
      selectedListLabel: 'Selected Permission',
    });
  </script>
@endsection

@section('content')
<div class="box">
	<div class="box-header with-border">
        <a href="{{$url}}" class="btn btn-warning"><i class="fa fa-fw fa-arrow-left"></i> Back</a>
	</div>
    {!! form_start($form, ['class' => 'form-horizontal']) !!}
	<div class="box-body">
	  	{!! form_rest($form) !!}
	</div>
	<div class="box-footer">
		<div class="col-sm-8 col-sm-offset-2">
		</div>
	</div>
    {!! form_end($form) !!}
</div>
@endsection
