@extends('admin.layouts.app')

@section('head-script')
  <link rel="stylesheet" type="text/css" href="{{asset('AdminLTE-2.4.5/plugins/bootstrap-duallistbox-master/plugins/bootstrap-duallistbox-master/src/bootstrap-duallistbox.css')}}">
@endsection

@section('end-script')
    <script src="{{ asset('js/timbangan.js') }}"></script>
    <script src="{{asset('AdminLTE-2.4.5/plugins/bootstrap-duallistbox-master/src/jquery.bootstrap-duallistbox.js')}}"></script>
    <script type="text/javascript">
    $( ".status" ).wrapAll( "<div class='col-sm-8'></div>");
    $( ".status" ).find('[name="status"]').css('margin-left','0');

    var demo1 = $('select[name="permission[]"]').bootstrapDualListbox({
      nonSelectedListLabel: 'Non Selected Permission',
      selectedListLabel: 'Selected Permission',
    });
    $("#demoform").submit(function() {
      alert($('[name="permission[]"]').val());
      return false;
    });
    </script>
@endsection

@section('content')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="box">
	<div class="box-header with-border">
        <a href="{{$url}}" class="btn btn-warning"><i class="fa fa-fw fa-arrow-left"></i> Back</a>
	</div>
    {!! form_start($form, ['class' => 'form-horizontal']) !!}
	<div class="box-body">
	  	{!! form_rest($form) !!}
	</div>
	<div class="box-footer">
		<div class="col-sm-8 col-sm-offset-2">
		  	<button type="submit" class="btn btn-primary">Submit</button>
		</div>
	</div>
    {!! form_end($form) !!}
</div>
@endsection
