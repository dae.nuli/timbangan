@extends('admin.layouts.app')

@section('head-script')
	<link rel="stylesheet" href="{{asset('AdminLTE-2.4.5/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endsection

@section('end-script')
	<script src="{{asset('AdminLTE-2.4.5/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
	<script src="{{asset('AdminLTE-2.4.5/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
	<script src="{{asset('js/timbangan.js')}}"></script>
	<script type="text/javascript">
		var table;
		$(function() {
		    table = $('#example2').DataTable({
		        processing: true,
		        serverSide: true,
		        ajax: '{!!$ajax!!}',
        		order: [[5,'desc']],
		        columns: [
		            { data: 'id', searchable: false, orderable: false},
		            { data: 'name', searchable: false, orderable: false},
		            { data: 'email', searchable: false, orderable: false},
		            { data: 'role', searchable: false, orderable: false},
		            { data: 'status', searchable: false, orderable: false},
		            { data: 'created_at', searchable: false},
		            { data: 'action', searchable: false, orderable: false}
		        ],
		        columnDefs: [{
		          "targets": 0,
		          "searchable": false,
		          "orderable": false,
		          "data": null,
		          // "title": 'No.',
		          "render": function (data, type, full, meta) {
		              return meta.settings._iDisplayStart + meta.row + 1;
		          }
		        }],
		    });
		});
	</script>
@endsection

@section('content')
<div class="box">
	<div class="box-header with-border">
	  	{{-- <h3 class="box-title">Title</h3> --}}
        <a href="{{$create}}" class="btn btn-primary"><i class="fa fa-fw fa-plus"></i> Create</a>
        <a href="#" class="btn btn-success pull-right" data-toggle="modal" data-target="#myModal"><i class="fa fa-fw fa-sliders"></i> Filter</a>
{{--
	  	<div class="box-tools pull-right">
	    	<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
	    	<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
	  	</div> --}}
	</div>
	<div class="box-body">
	  	<table id="example2" class="table table-bordered table-hover">
            <thead>
	            <tr>
	              <th>#</th>
	              <th>Nama</th>
	              <th>Email</th>
	              <th>Role</th>
	              <th>Status</th>
	              <th>Date</th>
	              <th>Action</th>
	            </tr>
            </thead>
            <tbody>
	        </tbody>
	    </table>
	</div>
</div>
<div class="modal fade" id="myModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Filter</h4>
			</div>
			<div class="modal-body">
				<form action="{{$url}}" id="form1" method="GET" class="form-horizontal">
	              <div class="box-body">
	                <div class="form-group">
	                  <label class="col-sm-2 control-label">Status</label>

	                  <div class="col-sm-10">
	                    <select class="form-control" name="status">
	                    	<option value>- Semua -</option>
	                    	<option value="1" {{($status == '1') ? 'selected' : ''}}>AKTIF</option>
	                    	<option value="0" {{($status == '0') ? 'selected' : ''}}>TIDAK AKTIF</option>
	                    </select>
	                  </div>
	                </div>
	                <div class="form-group">
	                  <label class="col-sm-2 control-label">Role</label>

	                  <div class="col-sm-10">
	                    <select class="form-control" name="role">
	                    	<option value>- Semua -</option>
	                    	@foreach($role as $row)
	                    	<option value="{{$row->id}}" {{($row->id == $roleId) ? 'selected' : ''}}>{{$row->name}}</option>
	                    	@endforeach
	                    </select>
	                  </div>
	                </div>
	              </div>
	            </form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-warning pull-left" data-dismiss="modal">Close</button>
				<button type="submit" form="form1" class="btn btn-primary">Submit</button>
			</div>
		</div>
	</div>
</div>
@endsection
