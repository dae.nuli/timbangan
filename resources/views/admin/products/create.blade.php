@extends('admin.layouts.app')

@section('select2-script')
  <!-- Select2 -->
  <link rel="stylesheet" href="{{ asset('AdminLTE-2.4.5/bower_components/select2/dist/css/select2.min.css') }}">
@endsection

@section('end-script')
  <script src="{{ asset('js/timbangan.js') }}"></script>
  <script type="text/javascript">
    // $.validate({
    //     form : '.form-horizontal',
    //     onSuccess : function() {
    //       waiting();
    //     }
    // });
    // $('.select2').select2();
    // $('.status').wrapAll( "<div class='col-sm-8'></div>");
    // $('.status').find('[name="status"]').css('margin-left','0');
    // $('.number').priceFormat({
    //   prefix: '',
    //   thousandsSeparator: '.',
    //   clearPrefix:true,
    //   centsLimit: 0,
    //   // centsSeparator: '.',
    //   // clearOnEmpty: true
    // });

    $( ".type" ).wrapAll( "<div class='col-sm-8'></div>");
    $( ".type" ).find('[name="type"]').css('margin-left','0');
  </script>
@endsection

@section('content')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="box">
	<div class="box-header with-border">
        <a href="{{$url}}" class="btn btn-warning"><i class="fa fa-fw fa-arrow-left"></i> Back</a>
	</div>
    {!! form_start($form, ['class' => 'form-horizontal']) !!}
	<div class="box-body">
	  	{!! form_rest($form) !!}
	</div>
	<div class="box-footer">
		<div class="col-sm-8 col-sm-offset-2">
        <button type="submit" class="btn btn-primary">Submit</button>
		</div>
	</div>
    {!! form_end($form) !!}
</div>
@endsection
