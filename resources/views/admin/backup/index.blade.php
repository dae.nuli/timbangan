@extends('admin.layouts.app')

@section('head-script')
	<link rel="stylesheet" href="{{asset('AdminLTE-2.4.5/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endsection

@section('end-script')
	<script src="{{asset('AdminLTE-2.4.5/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
	<script src="{{asset('AdminLTE-2.4.5/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
	<script src="{{asset('js/timbangan.js')}}"></script>
	<script type="text/javascript">
		var table;
		$(function() {
		    table = $('#data-table').DataTable({
		        processing: true,
		        serverSide: true,
		        ajax: '{{$ajax}}',
                order: [[3,'desc']],
		        columns: [
		            { data: 'id', searchable: false, orderable: false},
					{ data: 'name', searchable: false, orderable: false},
                	{ data: 'size', searchable: false, orderable: false},
					{ data: 'created_at', searchable: false, orderable: true},
		            { data: 'updated_at', searchable: false, orderable: false},
		            { data: 'action', searchable: false, orderable: false}
		        ],
		        columnDefs: [{
		          "targets": 0,
		          "searchable": false,
		          "orderable": false,
		          "data": null,
		          // "title": 'No.',
		          "render": function (data, type, full, meta) {
		              return meta.settings._iDisplayStart + meta.row + 1;
		          }
		        }],
		    });
		});
	</script>
@endsection

@section('content')
@if (session()->has('success'))
<div class="callout callout-success">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <p>{!!session('success')!!}</p>
</div>
@endif
<div class="box">
	<div class="box-header with-border">
        <a href="{{$create}}" class="btn btn-primary"><i class="fa fa-fw fa-refresh"></i> Backup</a>
	</div>
	<div class="box-body">
	  	<table id="data-table" class="table table-bordered table-hover">
            <thead>
	            <tr>
					<th>#</th>
					<th>File</th>
					<th>Size</th>
					<th>Created At</th>
					<th>Sent At</th>
					<th>Action</th>
	            </tr>
            </thead>
            <tbody>
	        </tbody>
	    </table>
	</div>
</div>
@endsection
