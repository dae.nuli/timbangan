    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{asset("AdminLTE-2.4.5/dist/img/user2-160x160.jpg")}}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Alexander Pierce</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        {{-- <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="../../index.html"><i class="fa fa-circle-o"></i> Dashboard v1</a></li>
            <li><a href="../../index2.html"><i class="fa fa-circle-o"></i> Dashboard v2</a></li>
          </ul>
        </li> --}}
        <li class="{{($urlactive == 'home') ? 'active' : ''}}">
          <a href="{{url('home')}}">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        @if(auth()->user()->can('tbs_index'))
            <li class="{{($urlactive == 'tbs') ? 'active' : ''}}">
              <a href="{{url('tbs')}}">
                <i class="fa fa-shopping-cart"></i> <span>Pembelian</span>
              </a>
            </li>
        @endif
        @if(auth()->user()->can('sale_index'))
            <li class="{{($urlactive == 'sales') ? 'active' : ''}}">
              <a href="{{url('sales')}}">
                <i class="fa fa-money"></i> <span>Penjualan</span>
              </a>
            </li>
        @endif
        @if(auth()->user()->can('salary_index'))
            <li class="{{($urlactive == 'salary') ? 'active' : ''}}">
              <a href="{{url('salary')}}">
                <i class="fa fa-dollar"></i> <span>Penggajian</span>
              </a>
            </li>
        @endif
        @if(auth()->user()->can('product_index') && auth()->user()->can('client_index') && auth()->user()->can('job_position_index') && auth()->user()->can('employee_index'))
        <li class="{{($urlactive == 'products' || $urlactive == 'client' || $urlactive == 'position' || $urlactive == 'employee') ? 'treeview active' : 'treeview'}}">
          <a href="#">
            <i class="fa fa-laptop"></i>
            <span>Master</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            @if(auth()->user()->can('product_index'))
                <li {{($urlactive == 'products') ? 'class=active' : ''}}><a href="{{ url('products') }}"><i class="fa fa-circle-o"></i> {{ __('menu.products') }}</a></li>
            @endif
            @if(auth()->user()->can('client_index'))
                <li {{($urlactive == 'client') ? 'class=active' : ''}}><a href="{{ url('client') }}"><i class="fa fa-circle-o"></i> {{ __('menu.suplier') }}</a></li>
            @endif
            @if(auth()->user()->can('job_position_index'))
                <li {{($urlactive == 'position') ? 'class=active' : ''}}><a href="{{ url('position') }}"><i class="fa fa-circle-o"></i> Jabatan</a></li>
            @endif
            @if(auth()->user()->can('employee_index'))
                <li {{($urlactive == 'employee') ? 'class=active' : ''}}><a href="{{ url('employee') }}"><i class="fa fa-circle-o"></i> Karyawan</a></li>
            @endif
          </ul>
        </li>
        @endif
        @if(auth()->user()->can('beban_biaya_index') && auth()->user()->can('pendapatan_index') && auth()->user()->can('data_perkiraan_index'))
        <li class="{{($urlactive == 'bebanBiaya' || $urlactive == 'pendapatan' || $urlactive == 'dataPerkiraan') ? 'treeview active' : 'treeview'}}">
          <a href="#">
            <i class="fa fa-bank"></i>
            <span>Keuangan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
              @if(auth()->user()->can('beban_biaya_index'))
                  <li {{($urlactive == 'bebanBiaya') ? 'class=active' : ''}}><a href="{{ url('bebanBiaya') }}"><i class="fa fa-circle-o"></i> Beban Biaya</a></li>
              @endif
              @if(auth()->user()->can('pendapatan_index'))
                  <li {{($urlactive == 'pendapatan') ? 'class=active' : ''}}><a href="{{ url('pendapatan') }}"><i class="fa fa-circle-o"></i> Pendapatan</a></li>
              @endif
              @if(auth()->user()->can('data_perkiraan_index'))
                  <li {{($urlactive == 'dataPerkiraan') ? 'class=active' : ''}}><a href="{{ url('dataPerkiraan') }}"><i class="fa fa-circle-o"></i> Data Perkiraan</a></li>
              @endif
            {{-- <li><a href="#"><i class="fa fa-circle-o"></i> Morris</a></li>
            <li><a href="#"><i class="fa fa-circle-o"></i> Flot</a></li>
            <li><a href="#"><i class="fa fa-circle-o"></i> Inline charts</a></li> --}}
          </ul>
        </li>
        @endif
        @if(auth()->user()->can('pembelian_report_index') && auth()->user()->can('penjualan_report_index') && auth()->user()->can('pendapatan_report_index') && auth()->user()->can('beban_biaya_report_index') && auth()->user()->can('laba_rugi_report_index'))
        <li class="{{($urlactive == 'reportPembelian' || $urlactive == 'reportPenjualan' || $urlactive == 'reportPendapatan' || $urlactive == 'reportBebanBiaya' || $urlactive == 'reportLabaRugi') ? 'treeview active' : 'treeview'}}">
          <a href="#">
            <i class="fa fa-pie-chart"></i>
            <span>Laporan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
              @if(auth()->user()->can('pembelian_report_index'))
                  <li {{($urlactive == 'reportPembelian') ? 'class=active' : ''}}><a href="{{ url('reportPembelian') }}"><i class="fa fa-circle-o"></i> Pembelian</a></li>
              @endif
              @if(auth()->user()->can('penjualan_report_index'))
                  <li {{($urlactive == 'reportPenjualan') ? 'class=active' : ''}}><a href="{{ url('reportPenjualan') }}"><i class="fa fa-circle-o"></i> Penjualan</a></li>
              @endif
              @if(auth()->user()->can('pendapatan_report_index'))
                  <li {{($urlactive == 'reportPendapatan') ? 'class=active' : ''}}><a href="{{ url('reportPendapatan') }}"><i class="fa fa-circle-o"></i> Pendapatan</a></li>
              @endif
              @if(auth()->user()->can('beban_biaya_report_index'))
                  <li {{($urlactive == 'reportBebanBiaya') ? 'class=active' : ''}}><a href="{{ url('reportBebanBiaya') }}"><i class="fa fa-circle-o"></i> Beban Biaya</a></li>
              @endif
              @if(auth()->user()->can('laba_rugi_report_index'))
                  <li {{($urlactive == 'reportLabaRugi') ? 'class=active' : ''}}><a href="{{ url('reportLabaRugi') }}"><i class="fa fa-circle-o"></i> Laba Rugi</a></li>
              @endif
          </ul>
        </li>
        @endif

        @if(auth()->user()->can('bank_index'))
            <li class="{{($urlactive == 'bank') ? 'active' : ''}}">
              <a href="{{url('bank')}}">
                <i class="fa fa-money"></i> <span>Rekening Bank</span>
              </a>
            </li>
        @endif

        @if(auth()->user()->can('about_index'))
            <li class="{{($urlactive == 'about') ? 'active' : ''}}">
              <a href="{{url('about')}}">
                <i class="fa fa-info"></i> <span>About</span>
              </a>
            </li>
        @endif

        @if(auth()->user()->can('backup_index'))
            <li class="{{($urlactive == 'backup') ? 'active' : ''}}">
              <a href="{{url('backup')}}">
                <i class="fa fa-database"></i> <span>Backup Database</span>
              </a>
            </li>
        @endif

        @if(auth()->user()->can('dbexport_index'))
            <li class="{{($urlactive == 'dbexport') ? 'active' : ''}}">
              <a href="{{url('dbexport')}}">
                <i class="fa fa-database"></i> <span>Sync Database</span>
              </a>
            </li>
        @endif

        @if(auth()->user()->can('admin_index') && auth()->user()->can('role_index') && auth()->user()->can('permission_index'))
            <li class="header">Permission Admin</li>
            @if(auth()->user()->can('admin_index'))
                <li class="{{($urlactive == 'admin') ? 'active' : ''}}">
                  <a href="{{url('admin')}}">
                    <i class="fa fa-gear"></i> <span>Admin</span>
                  </a>
                </li>
            @endif
            @if(auth()->user()->can('role_index'))
                <li class="{{($urlactive == 'role') ? 'active' : ''}}">
                  <a href="{{url('role')}}">
                    <i class="fa fa-gear"></i> <span>Role</span>
                  </a>
                </li>
            @endif
            @if(auth()->user()->can('permission_index'))
                <li class="{{($urlactive == 'permission') ? 'active' : ''}}">
                  <a href="{{url('permission')}}">
                    <i class="fa fa-gear"></i> <span>Permission</span>
                  </a>
                </li>
            @endif
        @endif
        {{-- <li class="treeview {{($urlactive == 'role' || $urlactive == 'permission') ? 'active' : ''}}">
          <a href="#">
            <i class="fa fa-laptop"></i>
            <span>Permission Admin</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            @if(auth()->user()->can('admin_index'))
            <li {{($urlactive == 'admin') ? 'class=active' : ''}}><a href="{{url('admin')}}"><i class="fa fa-circle-o"></i> Admin</a></li>
            @endif

            @if(auth()->user()->can('role_index'))
            <li {{($urlactive == 'role') ? 'class=active' : ''}}><a href="{{url('role')}}"><i class="fa fa-circle-o"></i> Role</a></li>
            @endif

            @if(auth()->user()->can('permission_index'))
            <li {{($urlactive == 'permission') ? 'class=active' : ''}}><a href="{{url('permission')}}"><i class="fa fa-circle-o"></i> Permission</a></li>
            @endif
          </ul>
        </li> --}}
      </ul>
    </section>
