@extends('admin.layouts.app')

@section('select2-script')
  <!-- Select2 -->
  <link rel="stylesheet" href="{{ asset('AdminLTE-2.4.5/bower_components/select2/dist/css/select2.min.css') }}">
@endsection

@section('head-script')
	<link rel="stylesheet" href="{{asset('AdminLTE-2.4.5/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endsection

@section('end-script')
	<script src="{{asset('AdminLTE-2.4.5/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
	<script src="{{asset('AdminLTE-2.4.5/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
	<script src="{{asset('js/timbangan.js')}}"></script>
	<script type="text/javascript">
    $.validate({
        form : '.form-horizontal',
        onSuccess : function() {
          // waiting();
        }
    });
		$('.select2').css('width', '100%').select2();
		var table;
		$(function() {
		    table = $('#data-table').DataTable({
		        processing: true,
		        serverSide: true,
		        ajax: '<?= $ajax ?>',
        		order: [],
		        columns: [
		            { data: 'id', searchable: false, orderable: false},
                	{ data: 'date', searchable: false, orderable: false},
		            { data: 'pendapatan_sub_id', searchable: false, orderable: false},
		            { data: 'amount', searchable: false, orderable: false},
		            { data: 'note', searchable: false, orderable: false},
		            { data: 'action', searchable: false, orderable: false}
		        ],
		        columnDefs: [{
		          "targets": 0,
		          "searchable": false,
		          "orderable": false,
		          "data": null,
		          // "title": 'No.',
		          "render": function (data, type, full, meta) {
		              return meta.settings._iDisplayStart + meta.row + 1;
		          }
		        }],
		    });
		});
	</script>
@endsection

@section('content')
<div class="box">
	<div class="box-header with-border">
	  	{{-- <h3 class="box-title">Title</h3> --}}
        <a href="#" class="btn btn-success" data-toggle="modal" data-target="#myModalFilter" style="margin-left: 5px;"><i class="fa fa-fw fa-sliders"></i> Filter</a>
        <a href="#" class="btn btn-info" data-toggle="modal" data-target="#myModalDownload"><i class="fa fa-fw fa-download"></i> Download</a>
{{--
	  	<div class="box-tools pull-right">
	    	<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
	    	<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
	  	</div> --}}
	</div>
	<div class="box-body">
	  	<table id="data-table" class="table table-bordered table-hover">
            <thead>
	            <tr>
					<th>#</th>
					<th>Tanggal</th>
					<th>Pendapatan</th>
					<th>Jumlah Uang</th>
					<th>Keterangan</th>
					<th>Action</th>
	            </tr>
            </thead>
            <tbody>
	        </tbody>
	    </table>
	</div>
</div>

<div class="modal fade" id="myModalDownload">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Download</h4>
			</div>
			<div class="modal-body">
				<form action="{{$download}}" id="form1" method="POST" class="form-horizontal">
					@csrf
	              <div class="box-body">
	                <div class="form-group">
	                  <label class="col-sm-3 control-label">Tahun</label>

	                  <div class="col-sm-8">
	                    <select class="form-control select2" name="year" required>
	                    	@foreach(range(2017, date('Y')) as $row)
		                    	<option value="{{$row}}" {{($row==date('Y')) ? 'selected': ''}}>{{ $row }}</option>
	                    	@endforeach
	                    </select>
	                  </div>
	                </div>
	                <div class="form-group">
	                  <label class="col-sm-3 control-label">Bulan</label>
	                  <div class="col-sm-8">
	                    <select class="select2 form-control" name="month" required>
	                    	@for($i=1; $i <= 12; $i++)
		                    	<option value="{{$i}}" {{($i==date('n')) ? 'selected': ''}}>{{ date('F', strtotime(date('Y').'-'.$i.'-01')) }}</option>
	                    	@endfor
	                    </select>
	                  </div>
	                </div>
	                <div class="form-group">
	                  <label class="col-sm-3 control-label">Pendapatan</label>
	                  <div class="col-sm-8">
                          <select class="form-control select22" style='width:100%' name="pendapatan_sub_id">
                          <option value="" selected>- Semua -</option>
                            @foreach($main as $row)
                              <option value="{{$row->id}}" style="color:#000; font-weight:bold" disabled>{{$row->name}}</option>
                              @foreach($row->sub as $sub)
                                  <option value="{{$sub->id}}">{{$sub->name}}</option>
                              @endforeach
                            @endforeach
                          </select>
	                  </div>
	                </div>
	              </div>
	            </form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
				<button type="submit" form="form1" class="btn btn-primary">Download</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="myModalFilter">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Filter</h4>
			</div>
			<div class="modal-body">
				<form action="{{$index}}" id="form2" method="GET" class="form-horizontal">
	              <div class="box-body">
	                <div class="form-group">
	                  <label class="col-sm-3 control-label">Tahun</label>

	                  <div class="col-sm-8">
	                    <select class="form-control select2" name="year">
                            <option value="">- Semua -</option>
                            @if(!empty($year))
                                @foreach(range(2017, date('Y')) as $row)
    		                    	<option value="{{$row}}" {{($row==$year) ? 'selected': ''}}>{{ $row }}</option>
    	                    	@endforeach
                            @else
                                @foreach(range(2017, date('Y')) as $row)
    		                    	<option value="{{$row}}" {{($row==date('Y')) ? 'selected': ''}}>{{ $row }}</option>
    	                    	@endforeach
                            @endif
	                    </select>
	                  </div>
	                </div>
	                <div class="form-group">
	                  <label class="col-sm-3 control-label">Bulan</label>
	                  <div class="col-sm-8">
	                    <select class="select2 form-control" name="month">
                            <option value="">- Semua -</option>
                            @if(!empty($month))
    	                    	@for($i=1; $i <= 12; $i++)
    		                    	<option value="{{$i}}" {{($i==$month) ? 'selected': ''}}>{{ date('F', strtotime(date('Y').'-'.$i.'-01')) }}</option>
    	                    	@endfor
                            @else
    	                    	@for($i=1; $i <= 12; $i++)
    		                    	<option value="{{$i}}" {{($i==date('n')) ? 'selected': ''}}>{{ date('F', strtotime(date('Y').'-'.$i.'-01')) }}</option>
    	                    	@endfor
                            @endif
	                    </select>
	                  </div>
	                </div>
	                <div class="form-group">
	                  <label class="col-sm-3 control-label">Pendapatan</label>
	                  <div class="col-sm-8">
                          <select class="form-control select22" style='width:100%' name="pendapatan">
                          <option value="">- Semua -</option>
                            @foreach($main as $row)
                              <option value="{{$row->id}}" style="color:#000; font-weight:bold" disabled>{{$row->name}}</option>
                              @foreach($row->sub as $sub)
                                  <option value="{{$sub->id}}" {{($sub->id==$sub_id) ? 'selected': ''}}>{{$sub->name}}</option>
                              @endforeach
                            @endforeach
                          </select>
	                  </div>
	                </div>
	              </div>
	            </form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
				<button type="submit" form="form2" class="btn btn-primary">Submit</button>
			</div>
		</div>
	</div>
</div>
@endsection
