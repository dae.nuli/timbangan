@extends('admin.layouts.app')

@section('select2-script')
  <!-- Select2 -->
  <link rel="stylesheet" href="{{ asset('AdminLTE-2.4.5/bower_components/select2/dist/css/select2.min.css') }}">
@endsection

@section('head-script')
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="{{asset('AdminLTE-2.4.5/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
@endsection

@section('end-script')
<!-- bootstrap datepicker -->
<script src="{{asset('AdminLTE-2.4.5/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{ asset('js/timbangan.js') }}"></script>
<script type="text/javascript">
  $('#datepicker').datepicker({
      format: 'yyyy-mm-dd',
      autoclose: true
  });
</script>
@endsection

@section('content')
<div class="box">
	<div class="box-header with-border">
        <a href="{{$url}}" class="btn btn-warning"><i class="fa fa-fw fa-arrow-left"></i> Back</a>
	</div>
    <form method="POST" accept-charset="UTF-8" class="form-horizontal">
    	<div class="box-body">
            <div class="form-group">
                <label class="col-sm-2 control-label">Beban Biaya</label>
                <div class="col-sm-8">
                    {{-- <select class="form-control" v-model="clientID" v-on:change="changeClient">
                        <option disabled value="">- Please Select -</option>
                        <option v-for="client in clients" v-bind:value="client.id">@{{client.name}}</option>
                    </select> --}}
                    <select class="form-control select22" style='width:100%' disabled>
                      @foreach($main as $row)
                        <option value="{{$row->id}}" style="color:#000; font-weight:bold" disabled>{{$row->name}}</option>
                        @foreach($row->sub as $sub)
                            <option value="{{$sub->id}}" {{($data->bebanbiaya_sub_id == $sub->id) ? 'selected':''}}>{{$sub->name}}</option>
                        @endforeach
                      @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Tanggal</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" value="{{$data->date}}" disabled id="datepicker">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Jumlah Uang</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control number" value="{{$data->amount}}" disabled>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Keterangan</label>
                <div class="col-sm-8">
                    <textarea class="form-control" rows="5" disabled>{{$data->note}}</textarea>
                </div>
            </div>
    	</div>
    </form>
</div>
@endsection
