<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Laporan Beban Biaya</title>
    <link rel="stylesheet" href="{{asset('css/pdf.css')}}" media="all" />
  </head>
  <body>
  	<h1 class="title">LAPORAN BEBAN BIAYA</h1>
  	<hr>
  	<div>
  		<table class="table-content">
  			<thead style="background-color: darkgray;">
      			<tr>
                    <th style="width:2%">No</th>
                    <th style="width:10%">Tanggal</th>
					<th>Beban Biaya</th>
					<th style="width:10%">Jumlah Uang</th>
					<th>Keterangan</th>
    	        </tr>
  			</thead>
  			<tbody>
  				@php
  				$no = 1;
  				@endphp
                @if(count($data))
					@foreach($data as $row)
				        <tr>
				            <td>{{$no++}}</td>
                            <td>{{($row->date) ? date('d F Y', strtotime($row->date)) : '-'}}</td>
                            <td>{{($row->sub->name) ?? '-'}}</td>
                            <td>{{($row->amount) ? 'Rp '.number_format($row->amount, 0, "", ".") : '-'}}</td>
                            <td>{{($row->note) ?? '-'}}</td>
                        </tr>
			        @endforeach
		        @else
			        <tr class="not">
			        	<td colspan="5">DATA TIDAK TERSEDIA</td>
			        </tr>
		        @endif
	    	</tbody>
  		</table>
  	</div>
  </body>
</html>
