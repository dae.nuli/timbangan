<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title></title>
        <style type="text/css">

        </style>
    </head>
    <body onLoad="window.print()">
<center><h4>{{$about->company}}</h4></center>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td><span style="text-transform:uppercase;">
      Pelanggan: {{$tbs->client->name}}
    </span><span style="text-transform:uppercase;">

    </span></td>
    <td>Produk: {{$tbs->item_name}} (1 Kg = Rp {{number_format($tbs->item_price, 0, "", ".")}})</td>
    {{-- <td><span style="text-transform:uppercase;">
        {{$tbs->created_at->format('d F Y')}}
    </span><span style="text-transform:uppercase;">
        {{$tbs->created_at->format('d F Y')}}
    </span></td> --}}
    <td><div align="right">
        {{$tbs->created_at->format('d F Y')}}
    </div></td>
  </tr>
</table>
<table width="100%" border="2" cellpadding="0" cellspacing="0">
  <tr>
    <td class="style1">&nbsp;Plat Mobil : {{$tbs->transports->plate_number}}</td>
    <td class="hbesar"><div align="center"><strong>potongan</strong></div></td>
    {{-- <td class="hbesar"><div align="center"><strong>pulangan</strong></div></td> --}}
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="5" cellpadding="0">
      <tr>
        <td>Berat Bruto</td>
        <td>:</td>
        <td colspan="2"><div align="right"><span style="text-transform:uppercase;">
          {{number_format($tbs->bruto, 0, "", ".")}}
        </span> Kg</div></td>
      </tr>
      <tr>
        <td>Berat Tara</td>
        <td>:</td>
        <td><div align="right"><span style="text-transform:uppercase;">
            {{number_format($tbs->tara, 0, "", ".")}}
        </span> Kg</div></td>
      </tr>
      <tr>
        <td><strong>Berat Netto</strong></td>
        <td>:</td>
        <td><div align="right"><strong>
            {{number_format($tbs->netto, 0, "", ".")}}
        </strong> Kg</div></td>
      </tr>
      <tr>
        <td>Berat Potongan</td>
        <td>:</td>
        <td><div align="right"><span style="text-transform:uppercase;">
            {{number_format($tbs->potongan, 0, "", ".")}}
        </span> Kg</div></td>
      </tr>
      <tr>
        <td><strong>Berat Terima</strong></td>
        <td>:</td>
        <td><div align="right"><strong>
            {{number_format($tbs->berat_bersih, 0, "", ".")}}
        </strong> Kg</div></td>
      </tr>
      <tr>
        <td><strong>Total</strong></td>
        <td>:</td>
        <td><div align="right"><strong>
            (Rp {{number_format($tbs->item_price, 0, "", ".")}}x{{$tbs->berat_bersih}} Kg) = Rp {{number_format($tbs->total, 0, "", ".")}}
        </strong></div></td>
      </tr>

    </table></td>
    <td><table width="100%" border="0" cellspacing="5" cellpadding="0">
      <tr>
        <td>Wajib</td>
        <td>:</td>
        <td><div align="right"><span style="text-transform:uppercase;">
          {{$tbs->potongan_wajib}}
        </span>%</div></td>
      </tr>
      <tr>
        <td>Sampah</td>
        <td>:</td>
        <td><div align="right"><span style="text-transform:uppercase;">
            {{$tbs->potongan_sampah}}
        </span>%</div></td>
      </tr>
      <tr>
        <td>Tankai</td>
        <td>:</td>
        <td><div align="right"><span style="text-transform:uppercase;">
            {{$tbs->potongan_tangkai}}
        </span>%</div></td>
      </tr>
      <tr>
        <td>Pasir</td>
        <td>:</td>
        <td><div align="right"><span style="text-transform:uppercase;">
            {{$tbs->potongan_pasir}}
        </span>%</div></td>
      </tr>
      <tr>
        <td>Air</td>
        <td>:</td>
        <td><div align="right"><span style="text-transform:uppercase;">
            {{$tbs->potongan_air}}
        </span>%</div></td>
      </tr>
      <tr>
        <td>Mutu</td>
        <td>:</td>
        <td><div align="right"><span style="text-transform:uppercase;">
            {{$tbs->potongan_mutu}}
        </span>%</div></td>
      </tr>
      <tr>
        <td><strong>Total</strong></td>
        <td><strong>:</strong></td>
        <td><div align="right"><strong><span style="text-transform:uppercase;">
        {{$tbs->potongan_wajib+$tbs->potongan_sampah+$tbs->potongan_tangkai+$tbs->potongan_pasir+$tbs->potongan_air+$tbs->potongan_mutu}}
        </span>%</strong></div></td>
      </tr>
    </table></td>

  </tr>
  <tr>
    <td>&nbsp;Catatan:<br />
      <span style="text-transform:uppercase;">
      &nbsp;{{$tbs->note}}
      </span><br />
      <br />
    <br /></td>
    <td><div align="center">Diketahui Oleh<br />
        <br />
          <br />
        <br />
    </div><div align="center">Diterima Oleh<br />
      <br />
      <br />
      <br />
    </div></td>
  </tr>
</table>
    </body>
</html>
