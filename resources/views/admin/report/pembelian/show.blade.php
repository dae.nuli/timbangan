@extends('admin.layouts.app')

@section('select2-script')
  <!-- Select2 -->
  <link rel="stylesheet" href="{{ asset('AdminLTE-2.4.5/bower_components/select2/dist/css/select2.min.css') }}">
@endsection

@section('end-script')
    <script src="{{ asset('js/timbangan.js') }}"></script>
@endsection

@section('content')
<form method="POST" accept-charset="UTF-8" class="form-horizontal">
@csrf
<div class="col-md-7">
    <div class="box">
        <div class="box-header with-border">
            <a href="{{$url}}" class="btn btn-warning"><i class="fa fa-fw fa-arrow-left"></i> Back</a>
            <a href="{{$print}}" target="_blank" class="btn btn-success" v-on:click="getTara"><i class="fa fa-fw fa-print"></i> Print</a>
            <a href="{{$printStruk}}" target="_blank" class="btn btn-info" v-on:click="getTara"><i class="fa fa-fw fa-print"></i> Print Struk</a>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Suplier</label>
                        <div class="col-sm-7">
                            <input class="form-control" disabled value="{{$tbs->client->name}}">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-sm-4 control-label">No. Plat</label>
                        <div class="col-sm-7">
                            <input class="form-control" disabled value="{{$tbs->transports->plate_number}}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Sopir</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" value="{{$tbs->driver_name}}" disabled>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Product</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" disabled value="{{$tbs->item_name}}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Bruto</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" value="{{$tbs->bruto}}" disabled>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Tara</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" value="{{$tbs->tara}}" disabled>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Netto</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" value="{{$tbs->netto}}" disabled>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Potongan</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" value="{{$tbs->potongan}}" disabled>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-sm-5 control-label">Berat Bersih</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" value="{{$tbs->berat_bersih}}" disabled>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Total</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" value="Rp {{number_format($tbs->total, 0, "", ".")}}" disabled>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Keterangan</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" value="{{$tbs->note}}" disabled>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-md-5">
    <div class="box">
        <div class="box-header with-border">
            <h4>Potongan</h4>
        </div>
        <div class="box-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-sm-6 control-label">Wajib <small>(%)</small></label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" value="{{$tbs->potongan_wajib}}" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-6 control-label">Tangkai <small>(%)</small></label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" value="{{$tbs->potongan_tangkai}}" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-6 control-label">Air <small>(%)</small></label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" value="{{$tbs->potongan_air}}" disabled>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-sm-6 control-label">Sampah <small>(%)</small></label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" value="{{$tbs->potongan_sampah}}" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-6 control-label">Pasir <small>(%)</small></label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" value="{{$tbs->potongan_pasir}}" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-6 control-label">Mutu <small>(%)</small></label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" value="{{$tbs->potongan_mutu}}" disabled>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</div>
</form>
@endsection
