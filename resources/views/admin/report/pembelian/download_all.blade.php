<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Laporan Pembelian</title>
    <link rel="stylesheet" href="{{asset('css/pdf.css')}}" media="all" />
  </head>
  <body>
  	<h1 class="title">LAPORAN PEMBELIAN</h1>
  	<hr>
  	<div>
  		<table class="table-content">
  			<thead style="background-color: darkgray;">
      			<tr>
    	            <th>No</th>
                    <th>Pelanggan</th>
                    <th>Nomor Mobil</th>
                    <th>Nama Sopir</th>
                    <th>Total</th>
                    <th>Date</th>
    	        </tr>
  			</thead>
  			<tbody>
  				@php
  				$no = 1;
  				@endphp
                @if(count($data))
					@foreach($data as $row)
				        <tr>
				            <td>{{$no++}}</td>
                            <td>{{($row->client->name) ?? '-'}}</td>
                            <td>{{($row->transports->plate_number) ?? '-'}}</td>
                            <td>{{($row->driver_name) ?? '-'}}</td>
                            <td>Rp {{number_format($row->total, 0, "", ".")}}</td>
				            <td>{{\Carbon\Carbon::parse($row->received_date)->format('d M Y, H:i:s')}}</td>
                        </tr>
			        @endforeach
		        @else
			        <tr class="not">
			        	<td colspan="6">DATA TIDAK TERSEDIA</td>
			        </tr>
		        @endif
	    	</tbody>
  		</table>
  	</div>
  </body>
</html>
