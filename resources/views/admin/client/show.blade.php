@extends('admin.layouts.app')

@section('content')
<form class="form-horizontal">
    <div class="row">
        <div class="col-md-8">
            <div class="box">
            	<div class="box-header with-border">
                    <a href="{{$url}}" class="btn btn-warning"><i class="fa fa-fw fa-arrow-left"></i> Back</a>
            	</div>
            	<div class="box-body">
                    <div class="row">
                        <div class="col-md-6">
                    	  	<div class="form-group">
                                <label class="col-sm-4 control-label">Nama</label>
                                <div class="col-sm-7">
                                    <input class="form-control" type="text" value="{{$client->name}}" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                    	  	<div class="form-group">
                                <label class="col-sm-4 control-label">Handphone</label>
                                <div class="col-sm-7">
                                    <input class="form-control" type="text" value="{{$client->phone}}" disabled>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Bank</label>
                                <div class="col-sm-7">
                                    <input class="form-control" type="text" value="{{$client->bank->name}}" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-sm-4 control-label">No Rekening</label>
                                <div class="col-sm-7">
                                    <input class="form-control" type="text" value="{{$client->bank_number}}" disabled>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Alamat</label>
                                <div class="col-sm-10">
                                    <input class="form-control" value="{{$client->address}}" type="text" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-sm-5 control-label">Potongan</label>
                                <div class="col-sm-6">
                                    <input class="form-control" value="{{$client->discount}}" type="number" disabled>
                                </div>
                            </div>
                        </div>
                    </div>
            	</div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="box">
                <div class="box-header with-border">
                    <h4>Truck</h4>
                </div>
                <div class="box-body">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                              <th>No Plat</th>
                              <th>Berat</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($transport as $row)
                            <tr>
                                <td><input type="text" name="plate_number" value="{{$row->plate_number}}" class="form-control" disabled=""></td>
                                <td><input type="number" name="weight" value="{{$row->weight}}" class="form-control" disabled=""></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection
