@extends('admin.layouts.app')

@section('select2-script')
  <!-- Select2 -->
  <link rel="stylesheet" href="{{ asset('AdminLTE-2.4.5/bower_components/select2/dist/css/select2.min.css') }}">
@endsection

@section('end-script')
  <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
  <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
  <script src="{{ asset('js/timbangan.js') }}"></script>

    <script type="text/javascript">
    var apiEdit = "{{$editUrl}}";
    var apiUpdate = "{{$update}}";
    var app = new Vue({
        el: "#app-client",
        data: {
            // rows: [
            //     { plate_number: "", weight: ""}
            // ],
            rows: null,
            name: "{{$client->name}}",
            discount: "{{$client->discount}}"
        },
        created: function() {
            this.fetchData()
        },
        watch: {

        },
        // mounted() {
        //     axios.get('{{$editUrl}}').then(response => {
        //         this.rows = response.data
        //     });
        // },
        methods: {
            fetchData: function () {
                axios.get(apiEdit).then(response => {
                    this.rows = response.data
                });
            },
            addRow: function() {
                // console.log(this.rows.length);
                if (this.rows.length < 2) {
                    this.rows.push({ plate_number: "", weight: ""});
                } else {
                    alert('Max 2 row');
                }
            },
            removeItem: function(index) {
                if (this.rows.length > 1) {
                    this.rows.splice(index, 1);
                } else {
                    alert('Min 1 row')
                }
            },
            processForm: function() {
                axios.post(apiUpdate, {
                    name: this.name, discount: this.discount, transport: this.rows, _method: 'PUT'
                }).then(function (response) {
                    window.location.replace("{{url('client')}}");
                });
            }
        }

    })
    </script>
@endsection

@section('content')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<form method="POST" action="{{$update}}" accept-charset="UTF-8" class="form-horizontal">
@csrf
@method('PUT')
    <div class="row">
        <div class="col-md-8">
            <div class="box">
            	<div class="box-header with-border">
                    <a href="{{$url}}" class="btn btn-warning"><i class="fa fa-fw fa-arrow-left"></i> Back</a>
            	</div>
            	<div class="box-body">
                    <div class="row">
                        <div class="col-md-6">
                    	  	<div class="form-group">
                                <label class="col-sm-4 control-label">Nama</label>
                                <div class="col-sm-7">
                                    <input class="form-control" name="name" type="text" value="{{$client->name}}" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                    	  	<div class="form-group">
                                <label class="col-sm-4 control-label">Handphone</label>
                                <div class="col-sm-7">
                                    <input class="form-control" name="phone" type="text" value="{{$client->phone}}" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Bank</label>
                                <div class="col-sm-7">
                                    <select class="form-control select2" name="bank_id">
                                        @foreach($bank as $bk)
                                            <option value="{{$bk->id}}" {{($client->bank_id==$bk->id)?'selected':''}}>{{$bk->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-sm-4 control-label">No Rekening</label>
                                <div class="col-sm-7">
                                    <input class="form-control" name="bank_number" type="text" value="{{$client->bank_number}}" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Alamat</label>
                                <div class="col-sm-10">
                                    <input class="form-control" name="address" value="{{$client->address}}" type="text" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-sm-5 control-label">Potongan</label>
                                <div class="col-sm-6">
                                    <input class="form-control" name="discount" value="{{$client->discount}}" type="number" required>
                                </div>
                            </div>
                        </div>
                    </div>
            	</div>
                <div class="box-footer">
                    <div class="col-sm-8 col-sm-offset-2">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="box" id="app-client">
                <div class="box-header with-border">
                    <button type="button" v-on:click="addRow()" class="btn btn-xs btn-success"><i class="fa fa-fw fa-plus"></i> Add Truck</button>
                </div>
                <div class="box-body">
                    <span class="text-red" id="danger" style="display: none">Min 1 row</span>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                              <th>No Plat</th>
                              <th>Berat</th>
                              <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="(row, index) in rows">
                                <td><input type="text" name="plate_number[]" v-model="row.plate_number" class="form-control" required></td>
                                <td><input type="number" name="weight[]" v-model="row.weight" class="form-control" required></td>
                                <td><button type="button" v-on:click="removeItem(index)" class="btn btn-xs btn-danger"><i class="fa fa-fw fa-trash"></i></button></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    {{-- <div class="row">
        <div class="col-md-12">
        </div>
    </div> --}}
</form>
@endsection
