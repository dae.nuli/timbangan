
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Vue.component('example-component', require('./components/ExampleComponent.vue'));
import ExampleComponent from './components/DisplayTimbangan.vue';
Vue.component('display-timbangan', ExampleComponent);

// const app = new Vue({
//     el: '#app'
// });
const app = new Vue({
    el: '#app',

    data: {
        messages: {
            bruto: null,
            disc: null,
            product: null,
            user: null,
        },
        // user: '',
    },

    created() {
        // let _this = this;
        this.listenForChanges();

    },

    methods: {
        listenForChanges() {
            Echo.channel('display')
                .listen('DisplayTimbangan', (e) => {
                    this.messages.bruto = e.bruto;
                    this.messages.user = e.user;
                    this.messages.product = e.product;
                    this.messages.disc = e.disc;
                });
        }
        // isTyping() {
        //     let channel = Echo.private('chat');
        //
        //     setTimeout(function() {
        //         channel.whisper('typing', {
        //             user: Laravel.user,
        //             typing: true
        //         });
        //     }, 300);
        // },
        //
        // sendMessage() {
        //     // add new message to messages array
        //     this.messages.push({
        //         user: Laravel.user,
        //         message: this.newMessage
        //     });
        //
        //     // clear input field
        //     this.newMessage = '';
        //
        //     // persist to database
        // }
    }
});
