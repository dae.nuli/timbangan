<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use App\Models\Backup;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class SendDatabase extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:send {ID}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send DB to Storage';
    protected $table;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Backup $table)
    {
        parent::__construct();
        $this->table = $table;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $ID = $this->argument('ID');
        try {
            $data = $this->table->findOrFail($ID);
        } catch (ModelNotFoundException $e) {
            return $this->error('ID not found.');
        }

        $process = new Process(sprintf(
            "curl -X POST -F 'database=@%s' %s",
            storage_path('app/backups/'.$data->name),
            env('SEND_FILE_URL')
        ));

        try {
            $process->mustRun();
            $this->info('The DB has been sent successfully.');
        } catch (ProcessFailedException $e) {
            $this->error('The DB process has been failed to sent.');
        }
    }
}
