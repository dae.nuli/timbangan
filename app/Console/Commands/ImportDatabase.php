<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Models\DbExport;

class ImportDatabase extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:import {ID}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Database';
    protected $process;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(DbExport $table)
    {
        parent::__construct();
        $this->table = $table;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $ID = $this->argument('ID');
        try {
            $data = $this->table->findOrFail($ID);
        } catch (ModelNotFoundException $e) {
            return $this->error('ID not found.');
        }

        $process = new Process(sprintf(
            'mysql -u%s -p%s %s < %s',
            // 'mysql -u%s -p%s %s < %s'
            config('database.connections.mysql.username'),
            config('database.connections.mysql.password'),
            config('database.connections.mysql.database'),
            // storage_path('app/public/db.sql'),
            storage_path('app/public/'.$data->name)
        ));

        try {
            $process->mustRun();
            $this->info('The Import has been proceed successfully.');
        } catch (ProcessFailedException $e) {
            $this->error('The Import process has been failed.');
        }
    }
}
