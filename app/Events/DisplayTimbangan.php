<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class DisplayTimbangan implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $bruto;
    public $user;
    public $product;
    public $disc;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($bruto, $user, $product, $disc)
    {
        $this->bruto = $bruto;
        $this->user = $user;
        $this->product = $product;
        $this->disc = $disc;
    }
    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('display');
        // return ['display'];
        // return new PrivateChannel('display');
    }
    // public function broadcastWith() {
    //     return [
    //       'messages' => $this->messages,
    //     ];
    //   }
}
