<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Products;
use App\Models\Transport;
use App\Models\Client;
use App\Models\PaymentHistory;
use App\Models\About;
use App\Models\PenimbanganTbs;
use App\Models\BankAccount;
use DataTables;
use Form;
use Illuminate\Support\Facades\Auth;
use App\Events\DisplayTimbangan;

class TbsController extends Controller
{
    private $folder = 'admin.penimbangan_tbs';
    private $uri = 'tbs';
    private $title = 'Penimbangan TBS';
    private $desc = 'Description';

    public function __construct(PenimbanganTbs $table, About $about, Products $product, Client $client)
    {
        $this->middleware('permission:tbs_index', ['only' => ['index','data']]);
        $this->middleware('permission:tbs_create', ['only' => ['create','store']]);
        // $this->middleware('permission:tbs_detail', ['only' => []]);
        $this->middleware('permission:tbs_process', ['only' => ['show', 'process', 'paymentHistory']]);
        $this->middleware('permission:tbs_edit', ['only' => ['edit','update']]);
        $this->middleware('permission:tbs_delete', ['only' => ['destroy']]);
        $this->table = $table;
        $this->about = $about;
        $this->product = $product;
    	$this->client = $client;
    }

    public function index(Request $request)
    {
        $data['title'] = $this->title;
        $data['ajax'] = route($this->uri.'.data');
        $data['create'] = route($this->uri.'.create');
        $data['bank'] = BankAccount::all();
        $data['urls'] = url('tbs/payment/');
        $data['urlCheckPayment'] = url('tbs/checkPayment');
        return view($this->folder.'.index',$data);
    }

    public function data(Request $request)
    {
        if ($request->ajax()) {
            $data = $this->table->select(['id', 'client_id', 'transports_id', 'driver_name', 'total', 'status', 'payment_status', 'created_at']);
            return DataTables::of($data)
                ->editColumn('client_id', function ($index) {
                    return ($index->client->name) ?? '-';
                    // return isset($index->client->name) ?? '-';
                })
                ->editColumn('total', function ($index) {
                    return 'Rp '.number_format($index->total, 0, "", ".");
                })
                ->editColumn('status', function ($index) {
                    return ($index->status == 'in') ? '<small class="label bg-orange">IN</small>' : '<small class="label bg-blue">DONE</small>';
                })
                ->editColumn('payment_status', function ($index) {
                    return ($index->payment_status) ? '<span class="ps" data-payment="'.$index->id.'"><small class="label bg-green">PAID</small></span>' : '<span class="ps" data-payment="'.$index->id.'"><small class="label bg-red">UNPAID</small></span>';
                })
                ->editColumn('transports_id', function ($index) {
                    return ($index->transports->plate_number) ?? '-';
                    // return isset($index->transports->plate_number) ?? '-';
                })
                ->editColumn('created_at', function ($index) {
                    return $index->created_at->format('d F Y H:i');
                    // return isset($index->transports->plate_number) ?? '-';
                })
                ->addColumn('action', function ($index) {
                    $tag = Form::open(array("url" => route($this->uri.'.destroy',$index->id), "method" => "DELETE"));
                    // $tag .= "<a href=".route($this->uri.'.edit',$index->id)." class='btn btn-primary btn-xs'>Edit</a>";
                    if (auth()->user()->can('tbs_payment')) {
                        $tag .= ($index->status == 'out') ? " <a data-url=".route($this->uri.'.payment',$index->id)." data-id=".$index->id." class='btn btn-warning btn-xs payment-button'>payment</a>" : "";
                    }
                    if (auth()->user()->can('tbs_process')) {
                        $tag .= ($index->status == 'in') ? " <a href=".route($this->uri.'.process',$index->id)." class='btn btn-info btn-xs'>process</a>" : " <a href=".route($this->uri.'.show',$index->id)." class='btn btn-success btn-xs'>show</a>";
                    }
                    $tag .= (auth()->user()->can('tbs_delete')) ? " <button type='submit' class='delete btn btn-danger btn-xs'>delete</button>" : '';
                    $tag .= Form::close();
                    return $tag;
                })
                ->rawColumns(['id', 'action', 'status', 'payment_status'])
                ->make(true);
        }
    }

    public function create()
    {
        $data['title'] = $this->title;
        $data['store'] = route($this->uri.'.store');
        $data['clients'] = Client::orderBy('id', 'desc')->get();
        $data['transports'] = Transport::orderBy('id', 'desc')->get();
        $data['products'] = Products::where('type', 1)->orderBy('id', 'desc')->get();
        // $data['form'] = $formBuilder->create('App\Forms\ProductForm', [
        //     'method' => 'POST',
        //     'url' => route($this->uri.'.store')
        // ]);
        // $data['clients'] = url('api/clients');
        $data['api_discount'] = route($this->uri.'.discount.api');
        $data['api_transports'] = route($this->uri.'.truck.api');
        $data['api_weight'] = route($this->uri.'.weight.api');
        // $data['products'] = url('api/product');
        $data['url'] = route($this->uri.'.index');
        return view($this->folder.'.create_v2', $data);
    }

    public function postDiscount(Request $request)
    {
        if (!empty($request->id)) {
                // code...
            $data = $this->client->find($request->id)->discount;
            return response()->json($data);
        }

    }

    public function postCheckPayment(Request $request)
    {
        if (!empty($request->id)) {
                // code...
                $data = $this->table->find($request->id);
            // $data = $this->table->find($request->id)->payment_status;
            return response()->json($data);
        }

    }

    public function paymentHistory($id)
    {
      $data['title'] = $this->title;
      $data['payment'] = PaymentHistory::where('penimbangan_tbs_id',$id)->orderBy('payment_date', 'desc')->get();
      $data['amount'] = PaymentHistory::where('penimbangan_tbs_id',$id)
                              ->orderBy('payment_date', 'desc')
                              ->sum('amount');
      $data['url'] = route($this->uri.'.show', $id);
      return view($this->folder.'.payment_history', $data);
    }

    public function show($id)
    {
      $data['title'] = $this->title;
      $data['tbs'] = $this->table->find($id);
      $data['url'] = route($this->uri.'.index');
      $data['print'] = route($this->uri.'.print', $id);
      $data['printStruk'] = route($this->uri.'.print.struk', $id);
      $data['historyUrl'] = route($this->uri.'.payment.history', $id);
      $data['about'] = $this->about->find(1);
      return view($this->folder.'.show', $data);
    }

    public function print($id)
    {
      $data['title'] = $this->title;
      $data['about'] = $this->about->find(1);
      $data['tbs'] = $this->table->find($id);
      $data['url'] = route($this->uri.'.index');
      return view($this->folder.'.print', $data);
    }

    public function printStruk($id)
    {
        $tbs = $this->table->find($id);
        // if ($tbs->payment_status == 1){
            $data['tbs'] = $tbs;
            $data['title'] = $this->title;
            $data['about'] = $this->about->find(1);
            $data['url'] = route($this->uri.'.index');
            return view($this->folder.'.print_struk', $data);
        // } else {
        //     return redirect()->back()->with('errors', 'Payment not complete');
        // }
    }

    public function postTruck(Request $request)
    {
        if (!empty($request->client)) {
            $data = Transport::where('client_id', $request->client)->orderBy('plate_number')->get();
            return response()->json($data);
        }
    }
    // public function postTruck(Request $request)
    // {
    //   if ($request->ajax() && !empty($request->client)) {
    //     $data = Transport::where('client_id', $request->client)->orderBy('plate_number')->get();
    //     $html[] = '<option value="">- Please Select -</option>';
    //     foreach ($data as $key => $value) {
    //       $html[] = "<option value=".$value->id.">".$value->plate_number."</option>";
    //     }
    //     return $html;
    //   } else {
    //     return '<option value="">- Please Select -</option>';
    //   }
    // }

    public function store(Request $request)
    {
        $item = $this->product->find($request->products_id);
        $request->merge([
            'item_price' => $item->price,
            'item_name' => $item->name,
        ]);
        $this->table->create($request->all());
        return redirect(route($this->uri.'.index'))->with('success',trans('message.create'));
    }

    public function edit($id)
    {
        $data['title'] = $this->title;
        $data['store'] = route($this->uri.'.update', $id);
        $tbs = $this->table->find($id);
        $data['clients'] = Client::orderBy('id', 'desc')->get();
        $data['transports'] = Transport::where('client_id', $tbs->client_id)->orderBy('id', 'desc')->get();
        $data['products'] = Products::orderBy('id', 'desc')->get();

        $data['api_transports'] = route($this->uri.'.truck.api');
        $data['api_weight'] = route($this->uri.'.weight.api');
        // $data['clients'] = url('api/clients');
        // $data['transports'] = url('api/transport');
        // $data['products'] = url('api/product');
        // $data['apiEdit'] = url('api/tbs/'.$id);
        // $data['row'] = $this->table->find($id);
        $data['tbs'] = $tbs;
        $data['url'] = route($this->uri.'.index');

        return view($this->folder.'.edit_v2', $data);
    }

    public function process($id)
    {
        $data['title'] = $this->title;
        $data['store'] = route($this->uri.'.process', $id);
        $tbs = $this->table->find($id);
        $data['clients'] = Client::orderBy('id', 'desc')->get();
        $data['transports'] = Transport::where('client_id', $tbs->client_id)->orderBy('id', 'desc')->get();
        $data['products'] = Products::orderBy('id', 'desc')->get();

        $data['api_transports'] = route($this->uri.'.truck.api');
        $data['api_weight'] = route($this->uri.'.weight.api');
        $data['api_product_price'] = url('api/productPrice/'.$tbs->products_id);
        $data['api_get_tara'] = url('api/tara');
        // $data['clients'] = url('api/clients');
        // $data['transports'] = url('api/transport');
        // $data['products'] = url('api/product');
        // $data['apiEdit'] = url('api/tbs/'.$id);
        // $data['row'] = $this->table->find($id);
        $data['tbs'] = $tbs;
        $data['url'] = route($this->uri.'.index');

        return view($this->folder.'.process', $data);
    }

    public function payment(Request $request, $id)
    {
        $tbs = $this->table->find($id);
        $payment = new PaymentHistory;
        $payment->penimbangan_tbs_id = $id;
        $payment->process_by = Auth::id();
        $payment->type = $request->type;
        $payment->amount = $request->amount;
        // $payment->change = !empty($tbs->total) ? ($request->amount - $tbs->total) : null;
        $payment->payment_date = $request->date;
        $payment->bank_account_id = $request->bank_account_id;
        $payment->save();

        $ph = PaymentHistory::where('penimbangan_tbs_id', $id)->sum('amount');
        if($ph >= $tbs->total) {
            $tbs->payment_status = 1;
            $tbs->save();
        } else {
            $tbs->payment_status = 0;
            $tbs->save();
        }

        // return 'x';
        return response()->json(['msg' => true, 'payment_status' => $tbs->payment_status]);
    }

    public function showEdit($id)
    {
        $data = $this->table->findOrFail($id);
        return response()->json($data);
    }

    public function update(Request $request, $id)
    {
        $item = $this->table->findOrFail($id);
        if ($item->products_id != $request->products_id) {
            $request->merge([
                'item_name' => $item->name,
                'item_price' => $item->price
            ]);
        }
        $item->update($request->all());
        return redirect(route($this->uri.'.index'))->with('success', trans('message.update'));
    }

    public function storeProcess(Request $request, $id)
    {
        // dd($request->all());
        $request->merge(['status' => 'out']);
        $this->table->findOrFail($id)->update($request->all());
        return redirect(route($this->uri.'.show', $id))->with('success', trans('message.update'));
    }

    public function destroy($id)
    {
        $tb = $this->table->findOrFail($id);
        $tb->delete();
        return response()->json(['msg' => true, 'success' => trans('message.delete')]);
    }

    function fetchWeight(Request $request)
    {

      $line = '';

      $f = fopen('capture2.txt', 'r');
      // $char = fgetc($f);

      $cursor = -3;

      fseek($f, $cursor, SEEK_END);
      $char = fgetc($f);
      // dd($char);
      while ($char === "\n" || $char === "\r") {
        fseek($f, $cursor--, SEEK_END);
        // $cr[] = $cursor;

        $char = fgetc($f);
// $xx[] = $char;
      }
      // dd($cr);
      while ($char !== false && $char !== "\n" && $char !== "\r" ) {

      	if (is_numeric($char)) {
             $line = $char . $line;
          }


        fseek($f, $cursor--, SEEK_END);
        // $cc[] = $cursor;

        $char = fgetc($f);

      }
      // dd($char);

      if(strlen($line) == 13){
        $data = substr($line, 3, 5);
        // event(new DisplayTimbangan($data));
        if (!empty($request->user_id)) {
            $username = Client::find($request->user_id)->name;
        } else {
            $username = '';
        }
        if (!empty($request->product_id)) {
            $product = Products::find($request->product_id)->name;
        } else {
            $product = '';
        }
        if (!empty($request->disc)) {
            $disc = $request->disc;
        } else {
            $disc = '';
        }
        broadcast(new DisplayTimbangan($data, $username, $product, $disc))->toOthers();
        return response()->json($data);
      }

    }

    function fetchWeightTara()
    {
      $line = '';
      $f = fopen('capturetara.txt', 'r');
      $cursor = -3;
      fseek($f, $cursor, SEEK_END);
      $char = fgetc($f);
      while ($char === "\n" || $char === "\r") {
        fseek($f, $cursor--, SEEK_END);
        $char = fgetc($f);
      }
      while ($char !== false && $char !== "\n" && $char !== "\r" ) {
      	if (is_numeric($char)) {
             $line = $char . $line;
          }
        fseek($f, $cursor--, SEEK_END);
        $char = fgetc($f);
      }
      if(strlen($line) == 13){
        $data = substr($line, 3, 2);
        return response()->json($data);
      }
    }
}
