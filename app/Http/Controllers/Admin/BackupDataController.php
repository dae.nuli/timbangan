<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Kris\LaravelFormBuilder\FormBuilder;
use App\Models\Backup;
use DataTables;
use Form;
use Artisan;

class BackupDataController extends Controller
{
    private $folder = 'admin.backup';
    private $uri = 'backup';
    private $title = 'Backup Database';
    private $desc = 'Description';

    public function __construct(Backup $table)
    {
        $this->middleware('permission:backup_index', ['only' => ['index','data']]);
        $this->middleware('permission:backup_prepare', ['only' => ['backup']]);
        $this->middleware('permission:backup_send', ['only' => ['send']]);
        // $this->middleware('permission:backup_delete', ['only' => ['destroy']]);
    	$this->table = $table;
    }

    public function index(Request $request)
    {
        $data['title'] = $this->title;
        $data['ajax'] = route($this->uri.'.data');
        $data['create'] = route($this->uri.'.backup');
        return view($this->folder.'.index',$data);
    }

    public function data(Request $request)
    {
        if ($request->ajax()) {
            $data = $this->table->select(['id', 'name', 'size', 'created_at', 'updated_at']);
            return DataTables::of($data)
                ->editColumn('name', function ($index) {
                    return $index->name;
                })
                ->editColumn('size', function ($index) {
                    return $this->formatSizeUnits($index->size);
                })
                ->editColumn('updated_at', function ($index) {
                    return ($index->updated_at)?$index->updated_at:'-';
                })
                ->addColumn('action', function ($index) {
                    // $tag = Form::open(array("url" => route($this->uri.'.destroy',$index->id), "method" => "DELETE"));
                    $tag = (auth()->user()->can('backup_send')) ? "<a href=".route($this->uri.'.send',$index->id)." class='btn btn-primary btn-xs'>send to server</a>" : '';
                    // $tag .= (auth()->user()->can('backup_delete')) ? " <button type='submit' class='delete btn btn-danger btn-xs'>delete</button>" : '';
                    // $tag .= Form::close();
                    return $tag;
                })
                ->rawColumns(['id', 'action'])
                ->make(true);
        }
    }

    public function backup()
    {
        Artisan::call('db:backup');

        return redirect()->back()->with('success','DB has been backed up');
    }
    public function formatSizeUnits($bytes)
    {
        if ($bytes >= 1073741824) {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        } elseif ($bytes >= 1048576) {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        } elseif ($bytes >= 1024) {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        } elseif ($bytes > 1) {
            $bytes = $bytes . ' bytes';
        } elseif ($bytes == 1) {
            $bytes = $bytes . ' byte';
        } else {
            $bytes = '0 bytes';
        }
        return $bytes;
    }
    public function send($id)
    {
        $this->table->findOrFail($id)->update(['updated_at' => date('Y-m-d H:i:s')]);

        Artisan::call('db:send', [
            'ID' => $id
        ]);

        return redirect()->back()->with('success','DB has been sent to server');
    }
}
