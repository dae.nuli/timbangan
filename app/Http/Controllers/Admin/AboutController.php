<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\About;
use Kris\LaravelFormBuilder\FormBuilder;
use DataTables;
use Form;
use File;

class AboutController extends Controller
{
    private $folder = 'admin.about';
    private $uri = 'about';
    private $title = 'About';
    private $desc = 'Description';

    public function __construct(About $table)
    {
        $this->middleware('permission:about_index', ['only' => ['index','data']]);
        $this->middleware('permission:about_edit', ['only' => ['edit','update']]);
    	$this->table = $table;
        $this->storeDir = base_path('public/logo');
    }

    public function index()
    {
        $data['title'] = $this->title;
        $data['about'] = $this->table->find(1);
        $data['edit'] = route($this->uri.'.edit');
        return view($this->folder.'.index',$data);
    }

    public function edit()
    {
        $data['title'] = $this->title;
        $data['about'] = $this->table->find(1);
        $data['url'] = route($this->uri.'.index');
        $data['store'] = route($this->uri.'.update');
        return view($this->folder.'.edit', $data);
    }

    public function update(Request $request)
    {
        if($request->hasFile('logo')) {
            $about = $this->table->find(1);
            if(!File::isDirectory($this->storeDir)){
                File::makeDirectory($this->storeDir, 0777);
            }
            if(!empty($about->image) && file_exists(public_path('logo/'.$about->image))){
                unlink(public_path('logo/'.$about->image));
            }
            $imageName = time().'.'.$request->file('logo')->extension();
            $request->file('logo')->move(public_path('logo'), $imageName);
            $request->merge(['image' => $imageName]);
        }
        $this->table->findOrFail(1)->update($request->all());
        return redirect(route($this->uri.'.index'))->with('success', trans('message.update'));
    }
}
