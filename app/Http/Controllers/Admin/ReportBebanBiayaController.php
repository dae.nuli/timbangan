<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\BebanBiaya;
use App\Models\BebanBiayaMain;
use App\Models\BebanBiayaSub;
use DataTables;
use Form;
use PDF;

class ReportBebanBiayaController extends Controller
{
    private $folder = 'admin.report.beban_biaya';
    private $uri = 'reportBebanBiaya';
    private $title = 'Beban Biaya';
    private $desc = 'Description';

    public function __construct(BebanBiaya $table, BebanBiayaMain $main, BebanBiayaSub $sub)
    {
        $this->middleware('permission:beban_biaya_report_index', ['only' => ['index','data']]);
        $this->middleware('permission:beban_biaya_report_detail', ['only' => ['show']]);
        $this->table = $table;
        $this->main = $main;
        $this->sub = $sub;
        $this->fullUrl = isset(explode('?', url()->full())[1]) ? route($this->uri.'.data').'?'.explode('?', url()->full())[1] : route($this->uri.'.data');
    }

    public function index(Request $request)
    {
        $data['title'] = $this->title;
        // $data['client'] = $this->client->orderBy('name')->get();
        $data['main'] = $this->main->orderBy('id', 'desc')->get();
        $data['ajax'] = $this->fullUrl;
        $data['month'] = $request->month;
        $data['year'] = $request->year;
        $data['sub_id'] = $request->bebanbiaya_sub_id;
        // $data['ajax'] = route($this->uri.'.data');
        $data['download'] = route($this->uri.'.download');
        $data['index'] = route($this->uri.'.index');
        return view($this->folder.'.index',$data);
    }

    public function data(Request $request)
    {
        if ($request->ajax()) {
            $data = $this->table->select(['id', 'date', 'bebanbiaya_sub_id', 'amount', 'note']);
            if($request->bebanbiaya_sub_id != null) {
                $data = $data->where('bebanbiaya_sub_id', $request->bebanbiaya_sub_id);
            }
            if ($request->month != null) {
                $data = $data->whereMonth('date', $request->month);
            }
            if ($request->year != null) {
                $data = $data->whereYear('date', $request->year);
            }
            return DataTables::of($data)
                ->editColumn('bebanbiaya_sub_id', function ($index) {
                    return ($index->sub->name) ?? '-';
                })
                ->editColumn('amount', function ($index) {
                    return 'Rp '.number_format($index->amount, 0, "", ".");
                })
                ->editColumn('date', function ($index) {
                    return date('d F Y', strtotime($index->date));
                })
                ->editColumn('note', function ($index) {
                    return str_limit($index->note, 10);
                })
                ->addColumn('action', function ($index) {
                    $tag = (auth()->user()->can('beban_biaya_report_detail')) ? " <a href=".route($this->uri.'.show',$index->id)." class='btn btn-success btn-xs'>Show</a>" : '';
                    return $tag;
                })
                ->rawColumns(['id', 'action'])
                ->make(true);
        }
    }
    //
    public function show($id)
    {
        $data['title'] = $this->title;
        $data['data'] = $this->table->find($id);
        $data['main'] = BebanBiayaMain::orderBy('id', 'desc')->get();
        $data['url'] = route($this->uri.'.index');
    //   $data['print'] = route($this->uri.'.print', $id);
    //   $data['printStruk'] = route($this->uri.'.print.struk', $id);
    //   $data['about'] = $this->about->find(1);
        return view($this->folder.'.show', $data);
    }
    public function download(Request $request)
    {
        if (!empty($request->month) && !empty($request->year)) {
            $down = $this->table->whereMonth('date', $request->month)->whereYear('date', $request->year);
            if (!empty($request->bebanbiaya_sub_id)) {
                $down = $down->where('bebanbiaya_sub_id', $request->bebanbiaya_sub_id);
            }
            $data['data'] =  $down->get();
            $pdf = PDF::loadView($this->folder.'.download', $data);
            return $pdf->setPaper('a4', 'landscape')->download(time().'.pdf');
        }
    }
}
