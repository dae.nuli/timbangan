<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\BebanBiaya;
use App\Models\Pendapatan;
use App\Models\Products;
use App\Models\Transport;
use App\Models\Client;
use App\Models\About;
use App\Models\PenimbanganTbs;
use DataTables;
use Form;
use PDF;

class ReportLabaRugiController extends Controller
{
    private $folder = 'admin.report.laba_rugi';
    private $uri = 'reportLabaRugi';
    private $title = 'Laba Rugi';
    private $desc = 'Description';

    public function __construct(Pendapatan $table, BebanBiaya $beban)
    {
        $this->middleware('permission:laba_rugi_report_index', ['only' => ['index']]);
        $this->table = $table;
        $this->beban = $beban;
        // $this->fullUrl = isset(explode('?', url()->full())[1]) ? route($this->uri.'.data').'?'.explode('?', url()->full())[1] : route($this->uri.'.data');
    }

    public function index(Request $request)
    {
        $data['title'] = $this->title;
        // $data['ajax'] = $this->fullUrl;
        $data['month'] = !empty($request->month)?$request->month:date('m');
        $data['year'] = !empty($request->year)?$request->year:date('Y');
        $data['client_id'] = $request->client;
        // $data['download'] = route($this->uri.'.download');
        $data['index'] = route($this->uri.'.index');

        $pendapatan = $this->table;
        if (!empty($request->month)) {
            $pendapatan = $pendapatan->whereMonth('date', $request->month);
        } else {
            $pendapatan = $pendapatan->whereMonth('date', date('m'));
        }
        if (!empty($request->year)) {
            $pendapatan = $pendapatan->whereYear('date', $request->year);
        } else {
            $pendapatan = $pendapatan->whereYear('date', date('Y'));
        }
        $data['pendapatan'] = $pendapatan->sum('amount');


        $beban = $this->beban;
        if (!empty($request->month)) {
            $beban = $beban->whereMonth('date', $request->month);
        } else {
            $beban = $beban->whereMonth('date', date('m'));
        }
        if (!empty($request->year)) {
            $beban = $beban->whereYear('date', $request->year);
        } else {
            $beban = $beban->whereYear('date', date('Y'));
        }
        $data['beban'] = $beban->sum('amount');
        $data['labaRugi'] = $pendapatan->sum('amount') - $beban->sum('amount');
        $data['dataBeban'] = $beban->get();
        $data['dataPendapatan'] = $pendapatan->get();

        return view($this->folder.'.index',$data);
    }
}
