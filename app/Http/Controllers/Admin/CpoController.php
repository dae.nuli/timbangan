<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\PenimbanganCpo;
use DataTables;
use Form;


class CpoController extends Controller
{
    private $folder = 'admin.penimbangan_cpo';
    private $uri = 'cpo';
    private $title = 'Penimbangan CPO';
    private $desc = 'Description';

    public function __construct(PenimbanganCpo $table)
    {
    	$this->table = $table;
    }

    public function index(Request $request)
    {
        $data['title'] = $this->title;
        $data['ajax'] = route($this->uri.'.data');
        $data['create'] = route($this->uri.'.create');
        return view($this->folder.'.index',$data);
    }

    public function data(Request $request)
    {
        if ($request->ajax()) {
            $data = $this->table->select(['id', 'client_id', 'transports_id', 'driver_name', 'created_at']);
            return DataTables::of($data)
                ->editColumn('client_id', function ($index) {
                    return ($index->client->name) ?? '-';
                    // return isset($index->client->name) ?? '-';
                })
                ->editColumn('transports_id', function ($index) {
                    return ($index->transports->plate_number) ?? '-';
                    // return isset($index->transports->plate_number) ?? '-';
                })
                ->addColumn('action', function ($index) {
                    $tag = Form::open(array("url" => route($this->uri.'.destroy',$index->id), "method" => "DELETE"));
                    $tag .= "<a href=".route($this->uri.'.edit',$index->id)." class='btn btn-primary btn-xs'>EDIT</a>";
                    // $tag .= " <a href=".route($this->uri.'.show',$index->id)." class='btn btn-success btn-xs'>Show</a>";
                    $tag .= " <button type='submit' class='delete btn btn-danger btn-xs'>Delete</button>";
                    $tag .= Form::close();
                    return $tag;
                })
                ->rawColumns(['id', 'action'])
                ->make(true);
        }
    }

    public function create()
    {
        $data['title'] = $this->title;
        $data['store'] = route($this->uri.'.store');
        // $data['form'] = $formBuilder->create('App\Forms\ProductForm', [
        //     'method' => 'POST',
        //     'url' => route($this->uri.'.store')
        // ]);
        $data['clients'] = url('api/clients');
        $data['transports'] = url('api/transport');
        $data['products'] = url('api/product');
        $data['url'] = route($this->uri.'.index');
        return view($this->folder.'.create', $data);
    }

    public function store(Request $request)
    {
        $data = $this->table->create($request->all());
        if ($data) {
            return response()->json(['success' => true]);
        }
    }

    public function edit($id)
    {
        $data['title'] = $this->title;
        $data['store'] = route($this->uri.'.update', $id);
        $data['clients'] = url('api/clients');
        $data['transports'] = url('api/transport');
        $data['products'] = url('api/product');
        $data['apiEdit'] = url('api/cpo/'.$id);
        $data['row'] = $this->table->find($id);
        $data['url'] = route($this->uri.'.index');
        $data['clientID'] = $id;
        return view($this->folder.'.edit', $data);
    }

    public function showEdit($id)
    {
        $data = $this->table->findOrFail($id);
        return response()->json($data);
    }

    public function update(Request $request, $id)
    {
        $this->table->findOrFail($id)->update($request->all());
        return response()->json(['success' => true]);
    }
}
