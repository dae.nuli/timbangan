<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\AdminRole;
use App\Models\Role;
use App\User;
use Kris\LaravelFormBuilder\FormBuilder;
use DataTables;
use Form;

class AdminController extends Controller
{
    private $folder = 'admin.admin';
    private $uri = 'admin';
    private $title = 'Admin';
    private $desc = 'Description';


    public function __construct(User $table)
    {
        $this->middleware('permission:admin_index', ['only' => ['index','data']]);
        $this->middleware('permission:admin_create', ['only' => ['create','store']]);
        $this->middleware('permission:admin_edit', ['only' => ['edit','update']]);
        $this->middleware('permission:admin_delete', ['only' => ['destroy']]);
        $this->table = $table;
        $this->fullUrl = isset(explode('?', url()->full())[1]) ? route($this->uri.'.data').'?'.explode('?', url()->full())[1] : route($this->uri.'.data');
        // $this->fullUrl = explode('?', url()->full());
    }

    public function index(Request $request)
    {
        // $segment = ;
        $data['title'] = $this->title;
        $data['ajax'] = $this->fullUrl;
        $data['create'] = route($this->uri.'.create');
        $data['roleId'] = $request->role;
        $data['status'] = $request->status;
        $data['url'] = route($this->uri.'.index');
        $data['role'] = Role::all();
        return view($this->folder.'.index',$data);
    }

    public function data(Request $request)
    {
        if ($request->ajax()) {
            $data = $this->table->select(['id', 'name', 'email', 'created_at', 'status']);
            if ($request->status != null) {
                $data = $data->where('status', $request->status);
            }
            if ($request->role != null) {
                $data = $data->whereHas('roles', function ($query) use ($request) {
                    $query->where('role_id', $request->role);
                });
            }
            return DataTables::of($data)
            // ->editColumn('id','<input type="checkbox" class="checkbox" name="id[]" value="{{$id}}"/>')
                ->editColumn('status', function ($index) {
                    if($index->status) {
                        return '<span class="label label-success">ACTIVE</span>';
                    } else {
                        return '<span class="label label-danger">NOT ACTIVE</span>';
                    }
                })
                ->addColumn('role', function ($index) {
                    foreach ($index->roles as $key => $value) {
                        $html[] = '<span class="label label-info">'.$value->role->name.'</span>';
                    }
                    return implode(' ', $html);
                })
                ->addColumn('action', function ($index) {
                    $tag = Form::open(array("url" => route($this->uri.'.destroy',$index->id), "method" => "DELETE"));
                    $tag .= (auth()->user()->can('admin_edit')) ? "<a href=".route($this->uri.'.edit',$index->id)." class='btn btn-primary btn-xs'>EDIT</a>" : '';
                    // $tag .= " <a href=".route($this->uri.'.show',$index->id)." class='btn btn-success btn-xs'>Show</a>";
                    $tag .= (auth()->user()->can('admin_delete')) ? " <button type='submit' class='delete btn btn-danger btn-xs'>Delete</button>" : '';
                    $tag .= Form::close();
                    return $tag;
                })
                ->rawColumns(['id', 'role', 'status', 'action'])
                ->make(true);
        }
    }

    public function create(FormBuilder $formBuilder)
    {
        $data['title'] = $this->title;
        $data['form'] = $formBuilder->create('App\Forms\AdminForm', [
            'method' => 'POST',
            'url' => route($this->uri.'.store')
        ]);
        $data['url'] = route($this->uri.'.index');
        return view($this->folder.'.create', $data);
    }

    public function edit(FormBuilder $formBuilder, $id)
    {
        $data['title'] = $this->title;
        $tbl = $this->table->find($id);
        $role = AdminRole::where('user_id', $id)->pluck('role_id')->toArray();
        $data['form'] = $formBuilder->create('App\Forms\AdminForm', [
            'method' => 'PUT',
            'model' => $tbl,
            'url' => route($this->uri.'.update', $id)
        ])
        ->modify('status', 'choice', [
            'selected' => null
        ])
        ->remove('role_id')
        ->addAfter('password', 'role_id', 'select', [
                'choices' => Role::pluck('name', 'id')->toArray(),
                // 'empty_value' => '- Please Select -',
                'label' => 'Role',
                'attr' => [
                    'data-validation' => 'required',
                    'class' => 'select2 form-control',
                    'multiple' => 'multiple'
                ],
            'selected' => $role
        ])
        ->modify('password', 'password', [
            'value' => '',
            'attr' => ['data-validation' => '']
        ]);

        $data['url'] = route($this->uri.'.index');
        return view($this->folder.'.create', $data);
    }

    public function store(Request $request)
    {
        $request->validate([
            'email' => 'required|unique:users,email',
            'password' => 'required|min:6'
        ]);
        $request->merge([
            'password' => bcrypt($request->password)
        ]);
        $row = $this->table->create($request->all());
        foreach ($request->role_id as $key => $value) {
            AdminRole::create([
                'user_id' => $row->id,
                'role_id' => $value
            ]);
        }
        return redirect(route($this->uri.'.index'))->with('success',trans('message.create'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'email' => 'required|unique:users,email,'.$id,
            'password' => 'nullable|min:6'
        ]);
        if(empty($request->password)){
            unset($request['password']);
        } else {
            $request->merge([
                'password' => bcrypt($request->password)
            ]);
        }
        $this->table->findOrFail($id)->update($request->all());

        AdminRole::where('user_id', $id)->delete();

        foreach ($request->role_id as $key => $value) {
            AdminRole::create([
                'user_id' => $id,
                'role_id' => $value
            ]);
        }
        return redirect(route($this->uri.'.index'))->with('success', trans('message.update'));
    }

    public function destroy($id)
    {
        $tb = $this->table->findOrFail($id);
        AdminRole::where('user_id', $id)->delete();
        $tb->delete();
        return response()->json(['msg' => true,'success' => trans('message.delete')]);
    }
}
