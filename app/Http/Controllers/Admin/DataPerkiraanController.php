<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\BebanBiayaMain;
use App\Models\BebanBiayaSub;
use App\Models\BebanBiaya;
use App\Models\PendapatanMain;
use App\Models\PendapatanSub;
use App\Models\Pendapatan;
use DataTables;
use Form;

class DataPerkiraanController extends Controller
{
    private $folder = 'admin.keuangan.data_perkiraan';
    private $uri = 'perkiraan';
    private $title = 'Data Perkiraan';
    private $desc = 'Description';

    public function index(Request $request)
    {
        $data['title'] = $this->title;
        $pendapatan = PendapatanMain::select(['id','name']);
        $data['data'] = BebanBiayaMain::select(['id','name'])
                        ->union($pendapatan)
                        ->get();
        return view($this->folder.'.index',$data);
    }
}
