<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\EmployeeSalary;
use App\Models\Employee;
use Kris\LaravelFormBuilder\FormBuilder;
use DataTables;
use Form;

class EmployeeController extends Controller
{
    private $folder = 'admin.employee';
    private $uri = 'employee';
    private $title = 'Employee';
    private $desc = 'Description';

    public function __construct(Employee $table)
    {
        $this->middleware('permission:employee_index', ['only' => ['index','data']]);
        $this->middleware('permission:employee_create', ['only' => ['create','store']]);
        $this->middleware('permission:employee_edit', ['only' => ['edit','update']]);
        $this->middleware('permission:employee_delete', ['only' => ['destroy']]);
    	$this->table = $table;
    }

    public function index(Request $request)
    {
        $data['title'] = $this->title;
        $data['ajax'] = route($this->uri.'.data');
        $data['create'] = route($this->uri.'.create');
        return view($this->folder.'.index',$data);
    }

    public function data(Request $request)
    {
        if ($request->ajax()) {
            $data = $this->table->select(['id', 'name', 'phone', 'job_position_id', 'bank_id', 'bank_account']);
            return DataTables::of($data)
                ->addColumn('salary', function ($index) {
                    return number_format($index->position->price, 0, "", ".");
                })
                ->editColumn('job_position_id', function ($index) {
                    return ($index->position->name) ?? '-';
                })
                ->editColumn('bank_id', function ($index) {
                    return ($index->bank->name) ?? '-';
                })
                ->addColumn('action', function ($index) {
                    $tag = Form::open(array("url" => route($this->uri.'.destroy',$index->id), "method" => "DELETE"));
                    $tag .= (auth()->user()->can('employee_edit')) ? "<a href=".route($this->uri.'.edit',$index->id)." class='btn btn-primary btn-xs'>EDIT</a>" : '';
                    $tag .= " <a href=".route($this->uri.'.show',$index->id)." class='btn btn-success btn-xs'>Show</a>";
                    $tag .= (auth()->user()->can('employee_delete')) ? " <button type='submit' class='delete btn btn-danger btn-xs'>Delete</button>" : '';
                    $tag .= Form::close();
                    return $tag;
                })
                ->rawColumns(['id', 'action'])
                ->make(true);
        }
    }

    public function create(FormBuilder $formBuilder)
    {
        $data['title'] = $this->title;
        $data['form'] = $formBuilder->create('App\Forms\EmployeeForm', [
            'method' => 'POST',
            'url' => route($this->uri.'.store')
        ]);
        $data['url'] = route($this->uri.'.index');
        return view($this->folder.'.create', $data);
    }

    public function edit(FormBuilder $formBuilder, $id)
    {
        $data['title'] = $this->title;
        $tbl = $this->table->find($id);
        $data['form'] = $formBuilder->create('App\Forms\EmployeeForm', [
            'method' => 'PUT',
            'model' => $tbl,
            'url' => route($this->uri.'.update', $id)
        ]);
        // ->modify('type', 'choice', [
        //     'selected' => null
        // ]);

        $data['url'] = route($this->uri.'.index');
        return view($this->folder.'.create', $data);
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'unique:employees,name',
        ]);
        $data = $this->table->create($request->all());
        EmployeeSalary::create([
            'employee_id' => $data->id,
            'salary' => $data->position->salary,
            'addition_salary' => $data->position->salary
        ]);
        return redirect(route($this->uri.'.index'))->with('success',trans('message.create'));
    }

    public function update(Request $request, $id)
    {
        $this->table->findOrFail($id)->update($request->all());
        return redirect(route($this->uri.'.index'))->with('success', trans('message.update'));
    }

    public function destroy($id)
    {
        $tb = $this->table->findOrFail($id);
        $tb->delete();
        return response()->json(['msg' => true,'success' => trans('message.delete')]);
    }
}
