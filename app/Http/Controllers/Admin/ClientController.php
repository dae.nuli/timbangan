<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Kris\LaravelFormBuilder\FormBuilder;
use App\Models\Transport;
use App\Models\Client;
use App\Models\Bank;
use DataTables;
use Form;


class ClientController extends Controller
{
    private $folder = 'admin.client';
    private $uri = 'client';
    private $title = 'Suplier';
    private $desc = 'Description';

    public function __construct(Client $table)
    {
        $this->middleware('permission:client_index', ['only' => ['index','data']]);
        $this->middleware('permission:client_create', ['only' => ['create','store']]);
        $this->middleware('permission:client_detail', ['only' => ['show']]);
        $this->middleware('permission:client_edit', ['only' => ['edit','update']]);
        $this->middleware('permission:client_delete', ['only' => ['destroy']]);
    	$this->table = $table;
    }

    public function index(Request $request)
    {
        $data['title'] = $this->title;
        $data['ajax'] = route($this->uri.'.data');
        $data['create'] = route($this->uri.'.create');
        return view($this->folder.'.index',$data);
    }

    public function data(Request $request)
    {
        if ($request->ajax()) {
            $data = $this->table->select(['id', 'name', 'phone', 'discount', 'created_at']);
            return DataTables::of($data)
                ->editColumn('phone', function ($index) {
                    return ($index->phone) ?? '-';
                })
                ->editColumn('discount', function ($index) {
                    return ($index->discount).' %';
                })
                ->addColumn('truck', function ($index) {
                    return $index->truck->count();
                })
                ->addColumn('action', function ($index) {
                    $tag = Form::open(array("url" => route($this->uri.'.destroy',$index->id), "method" => "DELETE"));
                    $tag .= (auth()->user()->can('client_edit')) ? "<a href=".route($this->uri.'.edit',$index->id)." class='btn btn-primary btn-xs'>EDIT</a>" : '';
                    $tag .= (auth()->user()->can('client_detail')) ? " <a href=".route($this->uri.'.show',$index->id)." class='btn btn-success btn-xs'>Detail</a>" : '';
                    $tag .= (auth()->user()->can('client_delete')) ? " <button type='submit' class='delete btn btn-danger btn-xs'>Delete</button>" : '';
                    $tag .= Form::close();
                    return $tag;
                })
                ->rawColumns(['id', 'action'])
                ->make(true);
        }
    }

    public function create()
    {
        $data['title'] = $this->title;
        $data['store'] = route($this->uri.'.store');
        // $data['form'] = $formBuilder->create('App\Forms\ProductForm', [
        //     'method' => 'POST',
        //     'url' => route($this->uri.'.store')
        // ]);
        $data['bank'] = Bank::orderBy('name')->get();
        $data['url'] = route($this->uri.'.index');
        return view($this->folder.'.create', $data);
    }

    public function edit($id)
    {
        $data['title'] = $this->title;
        $data['client'] = $this->table->find($id);
        $data['bank'] = Bank::orderBy('name')->get();
        $data['transport'] = Transport::where('client_id', $id)->get();
        // $data['form'] = $formBuilder->create('App\Forms\ProductForm', [
        //     'method' => 'PUT',
        //     'model' => $tbl,
        //     'url' => route($this->uri.'.update', $id)
        // ]);
        //     'url' =>
        $data['editUrl'] = url('api/client/'.$id);
        $data['update'] = route($this->uri.'.update', $id);
        $data['url'] = route($this->uri.'.index');
        return view($this->folder.'.edit', $data);
    }

    public function show($id)
    {
        $data['title'] = $this->title;
        $data['client'] = $this->table->find($id);
        $data['transport'] = Transport::where('client_id', $id)->get();
        $data['url'] = route($this->uri.'.index');
        return view($this->folder.'.show', $data);
    }

    public function editApi($id)
    {
        $data = Transport::where('client_id', $id)->get();
        return response()->json($data);
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $row = $this->table->create([
            'name' => $request->name,
            'address' => $request->address,
            'phone' => $request->phone,
            'bank_id' => $request->bank_id,
            'bank_number' => $request->bank_number,
            'discount' => $request->discount
        ]);
        // foreach ($request->transport as $key => $value) {
        //     Transport::create([
        //         'client_id' => $row->id,
        //         'plate_number' => $value['plate_number'],
        //         'weight' => $value['weight']]
        //     );
        // }
        foreach ($request->plate_number as $key => $value) {
            Transport::create([
                'client_id' => $row->id,
                'plate_number' => $value,
                'weight' => $request->weight[$key]
            ]);
        }
        // return response()->json(['success' => true]);
        // $this->table->create($request->all());
        return redirect(route($this->uri.'.index'))->with('success',trans('message.create'));
    }

    public function update(Request $request, $id)
    {
        // $row = $this->table->findOrFail($id)->update([
        //             'name' => $request->name,
        //             'discount' => $request->discount
        //         ]);
        $row = $this->table->findOrFail($id)->update([
            'name' => $request->name,
            'address' => $request->address,
            'phone' => $request->phone,
            'bank_id' => $request->bank_id,
            'bank_number' => $request->bank_number,
            'discount' => $request->discount
        ]);
        Transport::where('client_id', $id)->delete();
        foreach ($request->plate_number as $key => $value) {
            Transport::create([
                'client_id' => $id,
                'plate_number' => $value,
                'weight' => $request->weight[$key]
            ]);
        }
        // return response()->json(['success' => true]);
        // $this->table->findOrFail($id)->update($request->all());
        return redirect(route($this->uri.'.index'))->with('success', trans('message.update'));
    }

    public function destroy($id)
    {
        $tb = $this->table->findOrFail($id);
        $tb->delete();
        return response()->json(['msg' => true, 'success' => trans('message.delete')]);
    }

    public function list()
    {
        $data = $this->table->select('id', 'name')
                ->orderBy('id', 'desc')
                ->get();
        return response()->json($data);
    }

}
