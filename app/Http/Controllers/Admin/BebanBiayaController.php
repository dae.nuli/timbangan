<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\BebanBiayaMain;
use App\Models\BebanBiayaSub;
use App\Models\BebanBiaya;
use DataTables;
use Form;

class BebanBiayaController extends Controller
{
    private $folder = 'admin.keuangan.beban_biaya';
    private $uri = 'bebanBiaya';
    private $title = 'Beban Biaya';
    private $desc = 'Description';

    public function __construct(BebanBiaya $table, BebanBiayaMain $main, BebanBiayaSub $sub)
    {
        $this->middleware('permission:beban_biaya_index', ['only' => ['index','data']]);
        $this->middleware('permission:beban_biaya_create', ['only' => ['create','store']]);
        $this->middleware('permission:beban_biaya_edit', ['only' => ['edit','update']]);
        $this->middleware('permission:beban_biaya_delete', ['only' => ['destroy']]);
        $this->table = $table;
        $this->main = $main;
        $this->sub = $sub;
    }

    public function index(Request $request)
    {
        $data['title'] = $this->title;
        $data['ajax'] = route($this->uri.'.data');
        $data['create'] = route($this->uri.'.create');
        return view($this->folder.'.index',$data);
    }

    public function data(Request $request)
    {
        if ($request->ajax()) {
            $data = $this->table->select(['id', 'date', 'bebanbiaya_sub_id', 'amount', 'note']);
            return DataTables::of($data)
                ->editColumn('bebanbiaya_sub_id', function ($index) {
                    return ($index->sub->name) ?? '-';
                })
                ->editColumn('amount', function ($index) {
                    return 'Rp '.number_format($index->amount, 0, "", ".");
                })
                ->editColumn('date', function ($index) {
                    return date('d F Y', strtotime($index->date));
                })
                ->editColumn('note', function ($index) {
                    return str_limit($index->note, 10);
                })
                ->addColumn('action', function ($index) {
                    $tag = Form::open(array("url" => route($this->uri.'.destroy',$index->id), "method" => "DELETE"));
                    $tag .= (auth()->user()->can('beban_biaya_edit')) ? "<a href=".route($this->uri.'.edit',$index->id)." class='btn btn-primary btn-xs'>EDIT</a>" : '';
                    // $tag .= " <a href=".route($this->uri.'.show',$index->id)." class='btn btn-success btn-xs'>Show</a>";
                    $tag .= (auth()->user()->can('beban_biaya_delete')) ? " <button type='submit' class='delete btn btn-danger btn-xs'>Delete</button>" : '';
                    $tag .= Form::close();
                    return $tag;
                })
                ->rawColumns(['id', 'action'])
                ->make(true);
        }
    }

    public function create()
    {
        $data['title'] = $this->title;
        $data['store'] = route($this->uri.'.store');
        $data['main'] = BebanBiayaMain::orderBy('id', 'desc')->get();
        // $data['sub'] = BebanBiayaSub::orderBy('id', 'desc')->get();
        $data['url'] = route($this->uri.'.index');
        return view($this->folder.'.create', $data);
    }

    public function store(Request $request)
    {
        $this->table->create($request->all());
        return redirect(route($this->uri.'.index'))->with('success',trans('message.create'));
    }

    public function update(Request $request, $id)
    {
        $this->table->findOrFail($id)->update($request->all());
        return redirect(route($this->uri.'.index'))->with('success', trans('message.update'));
    }

    public function destroy($id)
    {
        $tb = $this->table->findOrFail($id);
        $tb->delete();
        return response()->json(['msg' => true,'success' => trans('message.delete')]);
    }

    public function edit($id)
    {
        $data['title'] = $this->title;
        $data['store'] = route($this->uri.'.update', $id);
        $data['main'] = BebanBiayaMain::orderBy('id', 'desc')->get();
        $data['data'] = $this->table->find($id);
        $data['url'] = route($this->uri.'.index');

        return view($this->folder.'.edit', $data);
    }
}
