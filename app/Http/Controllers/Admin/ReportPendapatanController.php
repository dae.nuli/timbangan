<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Pendapatan;
use App\Models\PendapatanMain;
use App\Models\PendapatanSub;
use DataTables;
use Form;
use PDF;

class ReportPendapatanController extends Controller
{
    private $folder = 'admin.report.pendapatan';
    private $uri = 'reportPendapatan';
    private $title = 'Pendapatan';
    private $desc = 'Description';

    public function __construct(Pendapatan $table, PendapatanMain $main, PendapatanSub $sub)
    {
        $this->middleware('permission:pendapatan_report_index', ['only' => ['index','data']]);
        $this->middleware('permission:pendapatan_report_detail', ['only' => ['show']]);
        $this->table = $table;
        $this->main = $main;
        $this->sub = $sub;
        $this->fullUrl = isset(explode('?', url()->full())[1]) ? route($this->uri.'.data').'?'.explode('?', url()->full())[1] : route($this->uri.'.data');

    }

    public function index(Request $request)
    {
        $data['title'] = $this->title;
        // $data['client'] = $this->client->orderBy('name')->get();
        $data['main'] = $this->main->orderBy('id', 'desc')->get();
        $data['ajax'] = $this->fullUrl;
        $data['month'] = $request->month;
        $data['year'] = $request->year;
        $data['sub_id'] = $request->pendapatan;
        $data['download'] = route($this->uri.'.download');
        $data['index'] = route($this->uri.'.index');
        return view($this->folder.'.index',$data);
    }

    public function data(Request $request)
    {
        if ($request->ajax()) {
            $data = $this->table->select(['id', 'date', 'pendapatan_sub_id', 'amount', 'note']);
            if(!empty($request->pendapatan)) {
                $data = $data->where('pendapatan_sub_id', $request->pendapatan);
            }
            if (!empty($request->month)) {
                $data = $data->whereMonth('date', $request->month);
            }
            if (!empty($request->year)) {
                $data = $data->whereYear('date', $request->year);
            }
            return DataTables::of($data)
                ->editColumn('pendapatan_sub_id', function ($index) {
                    return ($index->sub->name) ?? '-';
                })
                ->editColumn('amount', function ($index) {
                    return 'Rp '.number_format($index->amount, 0, "", ".");
                })
                ->editColumn('date', function ($index) {
                    return date('d F Y', strtotime($index->date));
                })
                ->editColumn('note', function ($index) {
                    return str_limit($index->note, 10);
                })
                ->addColumn('action', function ($index) {
                    $tag = (auth()->user()->can('pendapatan_report_detail')) ? " <a href=".route($this->uri.'.show',$index->id)." class='btn btn-success btn-xs'>Show</a>" : '';
                    return $tag;
                })
                ->rawColumns(['id', 'action'])
                ->make();
        }
    }
    //
    public function show($id)
    {
        $data['title'] = $this->title;
        $data['data'] = $this->table->find($id);
        $data['main'] = PendapatanMain::orderBy('id', 'desc')->get();
        $data['url'] = route($this->uri.'.index');
    //   $data['print'] = route($this->uri.'.print', $id);
    //   $data['printStruk'] = route($this->uri.'.print.struk', $id);
    //   $data['about'] = $this->about->find(1);
        return view($this->folder.'.show', $data);
    }
    //
    // public function print($id)
    // {
    //   $data['title'] = $this->title;
    //   $data['about'] = $this->about->find(1);
    //   $data['tbs'] = $this->table->find($id);
    //   $data['url'] = route($this->uri.'.index');
    //   return view($this->folder.'.print', $data);
    // }
    //
    // public function printStruk($id)
    // {
    //     $data['title'] = $this->title;
    //     $data['about'] = $this->about->find(1);
    //     $data['tbs'] = $this->table->find($id);
    //     $data['url'] = route($this->uri.'.index');
    //     return view($this->folder.'.print_struk', $data);
    // }

    public function download(Request $request)
    {
        if (!empty($request->month) && !empty($request->year)) {
            $down = $this->table->whereMonth('date', $request->month)->whereYear('date', $request->year);
            if (!empty($request->pendapatan_sub_id)) {
                $down = $down->where('pendapatan_sub_id', $request->pendapatan_sub_id);
            }
            // if (!empty($request->client)) {
            //     $down = $down->where('client_id', $request->client);
            // }
            $data['data'] =  $down->get();
            $pdf = PDF::loadView($this->folder.'.download', $data);
            return $pdf->setPaper('a4', 'landscape')->download(time().'.pdf');
        }
    }
}
