<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Kris\LaravelFormBuilder\FormBuilder;
use App\Models\Permission;
use DataTables;
use Form;
use Str;

class PermissionController extends Controller
{
    private $folder = 'admin.permission';
    private $uri = 'permission';
    private $title = 'Permission';
    private $desc = 'Description';


    public function __construct(Permission $table)
    {
        $this->middleware('permission:permission_index', ['only' => ['index','data']]);
        $this->table = $table;
    }

    public function index(Request $request)
    {
        $data['title'] = $this->title;
        $data['ajax'] = route($this->uri.'.data');
        $data['create'] = route($this->uri.'.create');
        return view($this->folder.'.index',$data);
    }

    public function data(Request $request)
    {
        if ($request->ajax()) {
            $data = $this->table->select(['id', 'name', 'description', 'created_at']);
            return DataTables::of($data)
                ->editColumn('description', function ($index) {
                    return Str::words($index->description, 3);
                })
                ->addColumn('action', function ($index) {
                    $tag = Form::open(array("url" => route($this->uri.'.destroy',$index->id), "method" => "DELETE"));
                    $tag .= "<a href='#' disabled='' class='btn btn-primary btn-xs'>EDIT</a>";
                    // $tag .= " <a href=".route($this->uri.'.show',$index->id)." class='btn btn-success btn-xs'>Show</a>";
                    $tag .= " <button type='submit' disabled='' class='delete btn btn-danger btn-xs'>Delete</button>";
                    $tag .= Form::close();
                    return $tag;
                })
                ->rawColumns(['id', 'action'])
                ->make(true);
        }
    }
}
