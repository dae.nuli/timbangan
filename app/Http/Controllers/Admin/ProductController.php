<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Products;
use Kris\LaravelFormBuilder\FormBuilder;
use DataTables;
use Form;

class ProductController extends Controller
{
    private $folder = 'admin.products';
    private $uri = 'products';
    private $title = 'Products';
    private $desc = 'Description';

    public function __construct(Products $table)
    {
        $this->middleware('permission:product_index', ['only' => ['index','data']]);
        $this->middleware('permission:product_create', ['only' => ['create','store']]);
        $this->middleware('permission:product_edit', ['only' => ['edit','update']]);
        $this->middleware('permission:product_delete', ['only' => ['destroy']]);
    	$this->table = $table;
    }

    public function index(Request $request)
    {
        $data['title'] = $this->title;
        $data['ajax'] = route($this->uri.'.data');
        $data['create'] = route($this->uri.'.create');
        return view($this->folder.'.index',$data);
    }

    public function data(Request $request)
    {
        if ($request->ajax()) {
            $data = $this->table->select(['id', 'name', 'qty', 'unit', 'price', 'type', 'created_at']);
            return DataTables::of($data)
                ->editColumn('price', function ($index) {
                    return number_format($index->price, 0, "", ".");
                })
                ->editColumn('type', function ($index) {
                    if ($index->type == 1) {
                      return 'TBS';
                    } elseif ($index->type == 2) {
                      return 'CPO';
                    } elseif ($index->type == 3) {
                      return 'BUAH';
                    }
                })
                ->addColumn('action', function ($index) {
                    $tag = Form::open(array("url" => route($this->uri.'.destroy',$index->id), "method" => "DELETE"));
                    $tag .= (auth()->user()->can('product_edit')) ? "<a href=".route($this->uri.'.edit',$index->id)." class='btn btn-primary btn-xs'>EDIT</a>" : '';
                    // $tag .= " <a href=".route($this->uri.'.show',$index->id)." class='btn btn-success btn-xs'>Show</a>";
                    $tag .= (auth()->user()->can('product_delete')) ? " <button type='submit' class='delete btn btn-danger btn-xs'>Delete</button>" : '';
                    $tag .= Form::close();
                    return $tag;
                })
                ->rawColumns(['id', 'action'])
                ->make(true);
        }
    }

    public function create(FormBuilder $formBuilder)
    {
        $data['title'] = $this->title;
        $data['form'] = $formBuilder->create('App\Forms\ProductForm', [
            'method' => 'POST',
            'url' => route($this->uri.'.store')
        ]);
        $data['url'] = route($this->uri.'.index');
        return view($this->folder.'.create', $data);
    }

    public function edit(FormBuilder $formBuilder, $id)
    {
        $data['title'] = $this->title;
        $tbl = $this->table->find($id);
        $data['form'] = $formBuilder->create('App\Forms\ProductForm', [
            'method' => 'PUT',
            'model' => $tbl,
            'url' => route($this->uri.'.update', $id)
        ])
        ->modify('type', 'choice', [
            'selected' => null
        ]);

        $data['url'] = route($this->uri.'.index');
        return view($this->folder.'.create', $data);
    }

    public function store(Request $request)
    {
        $this->table->create($request->all());
        return redirect(route($this->uri.'.index'))->with('success',trans('message.create'));
    }

    public function update(Request $request, $id)
    {
        $this->table->findOrFail($id)->update($request->all());
        return redirect(route($this->uri.'.index'))->with('success', trans('message.update'));
    }

    public function destroy($id)
    {
        $tb = $this->table->findOrFail($id);
        $tb->delete();
        return response()->json(['msg' => true,'success' => trans('message.delete')]);
    }

    public function listProduct()
    {
        $data = $this->table->orderBy('id', 'desc')->get();
        return response()->json($data);
    }

    public function getPrice($id)
    {
        $data = $this->table->find($id)->price;
        return response()->json($data);
    }
}
