<?php

namespace App\Http\Controllers\Admin;

use Kris\LaravelFormBuilder\FormBuilder;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Products;
use App\Models\Transport;
use App\Models\Client;
use App\Models\PenimbanganFruit;
use DataTables;
use Form;

class FruitController extends Controller
{
    private $folder = 'admin.fruits';
    private $uri = 'fruit';
    private $title = 'Penimbangan Buah';
    private $desc = 'Description';

    public function __construct(PenimbanganFruit $table)
    {
        $this->middleware('permission:product_index', ['only' => ['index','data']]);
        $this->middleware('permission:product_create', ['only' => ['create','store']]);
        $this->middleware('permission:product_detail', ['only' => ['show']]);
        $this->middleware('permission:product_edit', ['only' => ['edit','update']]);
        $this->middleware('permission:product_delete', ['only' => ['destroy']]);
    	$this->table = $table;
    }

    public function index(Request $request)
    {
        $data['title'] = $this->title;
        $data['ajax'] = route($this->uri.'.data');
        $data['create'] = route($this->uri.'.create');
        return view($this->folder.'.index',$data);
    }

    public function data(Request $request)
    {
        if ($request->ajax()) {
            $data = $this->table->select(['id', 'products_id', 'total', 'berat', 'created_at']);
            return DataTables::of($data)
                ->editColumn('berat', function ($index) {
                    return ($index->berat) ?? '-';
                    // return isset($index->client->name) ?? '-';
                })
                ->editColumn('total', function ($index) {
                    return 'Rp '.number_format($index->total, 0, "", ".");
                })
                ->editColumn('products_id', function ($index) {
                    return ($index->products->name) ?? '-';
                    // return isset($index->transports->plate_number) ?? '-';
                })
                ->editColumn('created_at', function ($index) {
                    return $index->created_at->format('d F Y H:i');
                    // return isset($index->transports->plate_number) ?? '-';
                })
                ->addColumn('action', function ($index) {
                    $tag = Form::open(array("url" => route($this->uri.'.destroy',$index->id), "method" => "DELETE"));
                    $tag .= "<a href=".route($this->uri.'.edit',$index->id)." class='btn btn-primary btn-xs'>EDIT</a>";
                    // $tag .= " <a href=".route($this->uri.'.show',$index->id)." class='btn btn-success btn-xs'>Show</a>";
                    $tag .= " <button type='submit' class='delete btn btn-danger btn-xs'>Delete</button>";
                    $tag .= Form::close();
                    return $tag;
                })
                ->rawColumns(['id', 'action'])
                ->make(true);
        }
    }

    public function create(FormBuilder $formBuilder)
    {
        $data['title'] = $this->title;
        $data['form'] = $formBuilder->create('App\Forms\FruitForm', [
            'method' => 'POST',
            'url' => route($this->uri.'.store')
        ]);
        $data['url'] = route($this->uri.'.index');
        $data['products'] = Products::where('type', 3)->orderBy('id', 'desc')->get();
        return view($this->folder.'.create', $data);
    }

    public function edit(FormBuilder $formBuilder, $id)
    {
        $data['title'] = $this->title;
        $tbl = $this->table->find($id);
        $data['form'] = $formBuilder->create('App\Forms\FruitForm', [
            'method' => 'PUT',
            'model' => $tbl,
            'url' => route($this->uri.'.update', $id)
        ]);
        // ->modify('type', 'choice', [
        //     'selected' => null
        // ]);

        $data['url'] = route($this->uri.'.index');
        return view($this->folder.'.create', $data);
    }

    public function store(Request $request)
    {
        $this->table->create($request->all());
        return redirect(route($this->uri.'.index'))->with('success',trans('message.create'));
    }

    public function update(Request $request, $id)
    {
        $this->table->findOrFail($id)->update($request->all());
        return redirect(route($this->uri.'.index'))->with('success', trans('message.update'));
    }

    public function destroy($id)
    {
        $tb = $this->table->findOrFail($id);
        $tb->delete();
        return response()->json(['msg' => true,'success' => trans('message.delete')]);
    }

    public function show($id)
    {
      $data['title'] = $this->title;
      $data['tbs'] = $this->table->find($id);
      $data['url'] = route($this->uri.'.index');
      return view($this->folder.'.show', $data);
    }
    //
}
