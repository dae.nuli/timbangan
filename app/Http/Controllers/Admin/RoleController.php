<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Kris\LaravelFormBuilder\FormBuilder;
use App\Models\RolePermission;
use App\Models\Permission;
use App\Models\Role;
use DataTables;
use Form;
use Str;

class RoleController extends Controller
{
    private $folder = 'admin.role';
    private $uri = 'role';
    private $title = 'Role';
    private $desc = 'Description';

    public function __construct(Role $table)
    {
        $this->middleware('permission:role_index', ['only' => ['index','data']]);
        $this->middleware('permission:role_create', ['only' => ['create','store']]);
        $this->middleware('permission:role_edit', ['only' => ['edit','update']]);
        $this->middleware('permission:role_delete', ['only' => ['destroy']]);
        $this->table = $table;
    }

    public function index(Request $request)
    {
        $data['title'] = $this->title;
        $data['ajax'] = route($this->uri.'.data');
        $data['create'] = route($this->uri.'.create');
        return view($this->folder.'.index',$data);
    }

    public function data(Request $request)
    {
        if ($request->ajax()) {
            $data = $this->table->select(['id', 'slug', 'name', 'description', 'created_at']);
            return DataTables::of($data)
                ->editColumn('description', function ($index) {
                    return Str::words($index->description, 3);
                })
                ->addColumn('action', function ($index) {
                    $tag = Form::open(array("url" => route($this->uri.'.destroy',$index->id), "method" => "DELETE"));
                    $tag .= (auth()->user()->can('role_edit')) ? "<a href=".route($this->uri.'.edit',$index->id)." class='btn btn-primary btn-xs'>EDIT</a>" : '';
                    // $tag .= " <a href=".route($this->uri.'.show',$index->id)." class='btn btn-success btn-xs'>Show</a>";
                    $tag .= (auth()->user()->can('role_delete')) ? " <button type='submit' class='delete btn btn-danger btn-xs'>Delete</button>" : '';
                    $tag .= Form::close();
                    return $tag;
                })
                ->rawColumns(['id', 'action'])
                ->make(true);
        }
    }

    public function create(FormBuilder $formBuilder)
    {
        $data['title'] = $this->title;
        $data['form'] = $formBuilder->create('App\Forms\RoleForm', [
            'method' => 'POST',
            'url' => route($this->uri.'.store')
        ]);
        $data['url'] = route($this->uri.'.index');
        return view($this->folder.'.create', $data);
    }

    public function edit(FormBuilder $formBuilder, $id)
    {
        $data['title'] = $this->title;
        $tbl = $this->table->find($id);
        $rp = RolePermission::where('role_id', $id)->pluck('permission_id')->toArray();
        $data['form'] = $formBuilder->create('App\Forms\RoleForm', [
            'method' => 'PUT',
            'model' => $tbl,
            'url' => route($this->uri.'.update', $id)
        ])
        ->modify('role', 'text', [
            'template' => 'admin.role.role_permission_edit',
            'value' => function ($val) {
                return Permission::pluck('description', 'id')->toArray();
            },
            'selectedPermission' => $rp
        ]);

        $data['url'] = route($this->uri.'.index');
        return view($this->folder.'.create', $data);
    }

    public function store(Request $request)
    {
        $row = $this->table->create($request->all());
        foreach ($request->permission as $key => $value) {
            RolePermission::create([
                'role_id' => $row->id,
                'permission_id' => $value,
            ]);
        }
        return redirect(route($this->uri.'.index'))->with('success',trans('message.create'));
    }

    public function update(Request $request, $id)
    {
        $this->table->findOrFail($id)->update($request->all());
        RolePermission::where('role_id', $id)->delete();
        foreach ($request->permission as $key => $value) {
            RolePermission::create([
                'role_id' => $id,
                'permission_id' => $value,
            ]);
        }
        return redirect(route($this->uri.'.index'))->with('success', trans('message.update'));
    }

    public function destroy($id)
    {
        $tb = $this->table->findOrFail($id);
        RolePermission::where('role_id', $id)->delete();
        $tb->delete();
        return response()->json(['msg' => true,'success' => trans('message.delete')]);
    }
}
