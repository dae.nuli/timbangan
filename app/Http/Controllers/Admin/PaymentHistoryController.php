<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\PaymentHistory;
use DataTables;
use Form;

class PaymentHistoryController extends Controller
{
    private $folder = 'admin.payment_history';
    private $uri = 'paymentHistory';
    private $title = 'Payment History';
    private $desc = 'Description';

    public function __construct(PaymentHistory $table)
    {
        $this->middleware('permission:payment_history_index', ['only' => ['index','data']]);
        $this->middleware('permission:payment_history_create', ['only' => ['create','store']]);
        $this->middleware('permission:payment_history_edit', ['only' => ['edit','update']]);
        $this->middleware('permission:payment_history_delete', ['only' => ['destroy']]);
    	$this->table = $table;
    }

    public function index(Request $request)
    {
        $data['title'] = $this->title;
        $data['ajax'] = route($this->uri.'.data');
        $data['create'] = route($this->uri.'.create');
        return view($this->folder.'.index',$data);
    }

    public function data(Request $request)
    {
        if ($request->ajax()) {
            $data = $this->table->select(['id', 'penimbangan_tbs_id', 'process_by', 'type', 'total', 'change', 'payment_date']);
            return DataTables::of($data)
                ->editColumn('total', function ($index) {
                    return 'Rp '.number_format($index->total, 0, "", ".");
                })
                ->editColumn('change', function ($index) {
                    return 'Rp '.number_format($index->change, 0, "", ".");
                })
                ->editColumn('payment_date', function ($index) {
                    return date('d F Y, H:i:s', strtotime($index->payment_date));
                })
                ->addColumn('action', function ($index) {
                    $tag = Form::open(array("url" => route($this->uri.'.destroy',$index->id), "method" => "DELETE"));
                    // $tag .= (auth()->user()->can('payment_history_edit')) ? "<a href=".route($this->uri.'.edit',$index->id)." class='btn btn-primary btn-xs'>edit</a>" : '';
                    $tag .= (auth()->user()->can('payment_history_delete')) ? " <button type='submit' class='delete btn btn-danger btn-xs'>delete</button>" : '';
                    $tag .= Form::close();
                    return $tag;
                })
                ->rawColumns(['id', 'action'])
                ->make(true);
        }
    }

    public function destroy($id)
    {
        $tb = $this->table->findOrFail($id);
        $tb->delete();
        return response()->json(['msg' => true,'success' => trans('message.delete')]);
    }
}
