<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\EmployeeSalaryStatus;
use App\Models\EmployeeSalary;
use App\Models\Employee;
use DataTables;
use Form;
use DB;

class EmployeeSalaryStatusController extends Controller
{
    private $folder = 'admin.salary';
    private $uri = 'salary';
    private $title = 'Penggajian';
    private $desc = 'Description';

    public function __construct(EmployeeSalary $table)
    {
        $this->middleware('permission:salary_index', ['only' => ['index','data']]);
        $this->middleware('permission:salary_done', ['only' => ['salaryDone']]);
    	$this->table = $table;
    }

    public function index(Request $request)
    {
        $data['title'] = $this->title;
        $data['ajax'] = route($this->uri.'.data');
        $data['create'] = route($this->uri.'.create');
        return view($this->folder.'.index',$data);
    }

    public function data(Request $request)
    {
        if ($request->ajax()) {
            // $data = DB::table('employee_salary_statuses')
            //             ->rightJoin('employee_salaries', 'employee_salary_statuses.employee_salary_id', '=', 'employee_salaries.id')
            //             ->select([
            //                 'employee_salary_statuses.id as id_status',
            //                 'employee_salary_statuses.created_at as status_created_at', 'employee_salary_statuses.status as status',
            //                 'employee_salaries.employee_id as employee_id',
            //                 'employee_salaries.salary as salary',
            //                 'employee_salaries.id as id'
            //             ]);
                        // ->whereMonth('employee_salary_statuses.created_at', '05');
            // $data = DB::table('employee_salary_statuses')
            $data = $this->table->whereMonth('created_at', date('m'))->select(['id', 'employee_id', 'salary']);
            return DataTables::of($data)
                ->editColumn('salary', function ($index) {
                    return 'Rp '.number_format($index->salary, 0, "", ".");
                })
                ->editColumn('employee_id', function ($index) {
                    return ($index->employee->name) ?? '-';
                })
                // ->editColumn('status', function ($index) {
                //     return ($index->status) ? 'SUDAH GAJIAN' : 'BELUM GAJIAN';
                // })
                ->addColumn('status', function ($index) {
                    return ($index->salaryStatus($index->id)) ? 'SUDAH GAJIAN' : 'BELUM GAJIAN';
                })
                ->addColumn('salarydate', function ($index) {
                    return ($index->salaryDate($index->id) != 0) ? $index->salaryDate($index->id) : '-';
                })
                // ->editColumn('status_created_at', function ($index) {
                //     return ($index->status_created_at) ? $index->status_created_at : '-';
                //     // return ($index->status_created_at) ? $index->status_created_at->format('d F Y, H:i:s') : '-';
                // })
                ->addColumn('action', function ($index) {
                    $tag = Form::open(array("url" => route($this->uri.'.destroy',$index->id), "method" => "DELETE"));
                    // $tag .= "<a href=".route($this->uri.'.edit',$index->id)." class='btn btn-primary btn-xs'>EDIT</a>";
                    // $tag .= " <a href=".route($this->uri.'.show',$index->id)." class='btn btn-success btn-xs'>Show</a>";
                    if (auth()->user()->can('salary_done')) {
                        $tag .= ($index->salaryStatus($index->id)) ? " <a class='btn btn-success btn-xs disabled'>DONE</a>" : " <a href=".route($this->uri.'.done',$index->id)." class='submit btn btn-success btn-xs'>DONE</a>";
                    }
                    // $tag .= " <button type='submit' class='delete btn btn-danger btn-xs'>Delete</button>";
                    $tag .= Form::close();
                    return $tag;
                })
                ->rawColumns(['id', 'action'])
                ->make(true);
        }
    }

    public function salaryDone($id)
    {
        EmployeeSalaryStatus::create([
            'employee_salary_id' => $id,
            'status' => 1,
        ]);
        return redirect(route($this->uri.'.index'))->with('success',trans('message.create'));
    }
}
