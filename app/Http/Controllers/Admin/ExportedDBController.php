<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\DbExport;
use DataTables;
use Artisan;
use Form;

class ExportedDBController extends Controller
{
    private $folder = 'admin.db_export';
    private $uri = 'dbexport';
    private $title = 'Database';
    private $desc = 'Description';

    public function __construct(DbExport $table)
    {
        $this->middleware('permission:dbexport_index', ['only' => ['index','data']]);
        $this->middleware('permission:dbexport_import', ['only' => ['import']]);
        $this->middleware('permission:dbexport_delete', ['only' => ['destroy']]);
    	$this->table = $table;
    }

    public function index(Request $request)
    {
        $data['title'] = $this->title;
        $data['ajax'] = route($this->uri.'.data');
        // $data['create'] = route($this->uri.'.create');
        return view($this->folder.'.index',$data);
    }

    public function data(Request $request)
    {
        if ($request->ajax()) {
            $data = $this->table->select(['id', 'name', 'size', 'created_at', 'updated_at']);
            return DataTables::of($data)
                ->editColumn('name', function ($index) {
                    return $index->name;
                })
                ->editColumn('size', function ($index) {
                    return $this->formatSizeUnits($index->size);
                })
                ->editColumn('updated_at', function ($index) {
                    return ($index->updated_at)?$index->updated_at:'-';
                })
                ->addColumn('action', function ($index) {
                    $tag = Form::open(array("url" => route($this->uri.'.destroy',$index->id), "method" => "DELETE"));
                    $tag .= (auth()->user()->can('dbexport_import')) ? "<a href=".route($this->uri.'.import',$index->id)." class='btn btn-primary btn-xs'>import</a>" : '';
                    // $tag .= (auth()->user()->can('dbexport_delete')) ? " <button type='submit' class='delete btn btn-danger btn-xs'>delete</button>" : '';
                    $tag .= Form::close();
                    return $tag;
                })
                ->rawColumns(['id', 'action'])
                ->make(true);
        }
    }
    public function formatSizeUnits($bytes)
    {
        if ($bytes >= 1073741824) {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        } elseif ($bytes >= 1048576) {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        } elseif ($bytes >= 1024) {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        } elseif ($bytes > 1) {
            $bytes = $bytes . ' bytes';
        } elseif ($bytes == 1) {
            $bytes = $bytes . ' byte';
        } else {
            $bytes = '0 bytes';
        }
        return $bytes;
    }

    public function import($id)
    {
        $this->table->findOrFail($id)->update(['updated_at' => date('Y-m-d H:i:s')]);

        Artisan::call('db:import', [
            'ID' => $id
        ]);

        return redirect()->back()->with('success','DB has been imported');
    }

    public function destroy($id)
    {
        $tb = $this->table->findOrFail($id);
        $tb->delete();
        return response()->json(['msg' => true,'success' => trans('message.delete')]);
    }
}
