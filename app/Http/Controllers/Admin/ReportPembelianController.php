<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Products;
use App\Models\Transport;
use App\Models\Client;
use App\Models\About;
use App\Models\PenimbanganTbs;
use DataTables;
use Form;
use PDF;

class ReportPembelianController extends Controller
{
    private $folder = 'admin.report.pembelian';
    private $uri = 'reportPembelian';
    private $title = 'Pembelian';
    private $desc = 'Description';

    public function __construct(PenimbanganTbs $table, About $about, Products $product, Client $client)
    {
        $this->middleware('permission:pembelian_report_index', ['only' => ['index','data']]);
        $this->middleware('permission:pembelian_report_detail', ['only' => ['show']]);
        $this->table = $table;
        $this->about = $about;
        $this->product = $product;
    	$this->client = $client;
        $this->fullUrl = isset(explode('?', url()->full())[1]) ? route($this->uri.'.data').'?'.explode('?', url()->full())[1] : route($this->uri.'.data');
    }

    public function index(Request $request)
    {
        $data['title'] = $this->title;
        $data['client'] = $this->client->orderBy('name')->get();
        $data['ajax'] = $this->fullUrl;
        $data['month'] = $request->month;
        $data['year'] = $request->year;
        $data['client_id'] = $request->client;
        $data['download'] = route($this->uri.'.download');
        $data['index'] = route($this->uri.'.index');

        return view($this->folder.'.index',$data);
    }

    public function data(Request $request)
    {
        if ($request->ajax()) {
            $data = $this->table->where('status', 'out')
            ->select(['id', 'client_id', 'transports_id', 'driver_name', 'total', 'status', 'created_at']);
            if($request->client != null) {
                $data = $data->where('client_id', $request->client);
            }
            if ($request->month != null) {
                $data = $data->whereMonth('created_at', $request->month);
            }
            if ($request->year != null) {
                $data = $data->whereYear('created_at', $request->year);
            }
            return DataTables::of($data)
                ->editColumn('client_id', function ($index) {
                    return ($index->client->name) ?? '-';
                    // return isset($index->client->name) ?? '-';
                })
                ->editColumn('total', function ($index) {
                    return 'Rp '.number_format($index->total, 0, "", ".");
                    // return isset($index->client->name) ?? '-';
                })
                ->editColumn('status', function ($index) {
                    return ($index->status == 'in') ? '<small class="label bg-orange">IN</small>' : '<small class="label bg-blue">DONE</small>';
                })
                ->editColumn('transports_id', function ($index) {
                    return ($index->transports->plate_number) ?? '-';
                    // return isset($index->transports->plate_number) ?? '-';
                })
                ->editColumn('created_at', function ($index) {
                    return $index->created_at->format('d F Y H:i');
                    // return isset($index->transports->plate_number) ?? '-';
                })
                ->addColumn('action', function ($index) {
                    // $tag .= "<a href=".route($this->uri.'.edit',$index->id)." class='btn btn-primary btn-xs'>Edit</a>";
                    $tag = "<a target='_blank' href=".route($this->uri.'.print',$index->id)." class='btn btn-info btn-xs'>Print</a>";
                    $tag .= (auth()->user()->can('pembelian_report_detail')) ? " <a href=".route($this->uri.'.show',$index->id)." class='btn btn-success btn-xs'>Show</a>" : '';
                    return $tag;
                })
                ->rawColumns(['id', 'action', 'status'])
                ->make(true);
        }
    }

    public function show($id)
    {
      $data['title'] = $this->title;
      $data['tbs'] = $this->table->find($id);
      $data['url'] = route($this->uri.'.index');
      $data['print'] = route($this->uri.'.print', $id);
      $data['printStruk'] = route($this->uri.'.print.struk', $id);
      $data['about'] = $this->about->find(1);
      return view($this->folder.'.show', $data);
    }

    public function print($id)
    {
      $data['title'] = $this->title;
      $data['about'] = $this->about->find(1);
      $data['tbs'] = $this->table->find($id);
      $data['url'] = route($this->uri.'.index');
      return view($this->folder.'.print', $data);
    }

    public function printStruk($id)
    {
        $data['title'] = $this->title;
        $data['about'] = $this->about->find(1);
        $data['tbs'] = $this->table->find($id);
        $data['url'] = route($this->uri.'.index');
        return view($this->folder.'.print_struk', $data);
    }

    public function download(Request $request)
    {
        if (!empty($request->month) && !empty($request->year)) {
            // $pisah = explode(' - ', $request->letter_entry);
            // $from = Carbon::parse($pisah[0])->format('Y-m-d H:i:s');
            // $to = Carbon::parse($pisah[1])->format('Y-m-d').' 24:60:60';
            // dd($pisah);
            $data['type'] = 1;
            $down = $this->table->whereMonth('created_at', $request->month)->whereYear('created_at', $request->year);
            if (!empty($request->client)) {
                $down = $down->where('client_id', $request->client);
            }
            $data['data'] =  $down->get();
            $pdf = PDF::loadView($this->folder.'.download_all', $data);
            return $pdf->setPaper('a4', 'landscape')->download(time().'.pdf');
        }
    }
}
