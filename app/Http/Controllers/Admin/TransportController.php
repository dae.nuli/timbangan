<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Transport;

class TransportController extends Controller
{
    public function show($id)
    {
        $data = Transport::select('id', 'plate_number')
                ->orderBy('id', 'desc')
                ->where('client_id', $id)
                ->get();
        return response()->json($data);
    }
}
