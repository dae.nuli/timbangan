<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\BebanBiaya;
use App\Models\Pendapatan;
use App\Models\PenimbanganTbs;
use App\Models\Sale;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Pendapatan $table, BebanBiaya $beban, PenimbanganTbs $timbang, Sale $jual)
    {
        $this->middleware('auth');
        $this->table = $table;
        $this->beban = $beban;
        $this->timbang = $timbang;
        $this->jual = $jual;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['title'] = 'Grafik Bulan ini';
        $data['desc'] = '';
        $data['pembelian'] = $this->timbang->whereMonth('created_at', date('m'))->sum('total');
        $data['penjualan'] = $this->jual->whereMonth('created_at', date('m'))->sum('total');
        $data['cars'] = $this->timbang->count();
        return view('home', $data);
    }
}
