<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\DbExport;
use Illuminate\Support\Facades\Storage;

class SendFileController extends Controller
{
    // public function index()
    public function index(Request $request)
    {
        $fileName = 'dbs_'.date('YmdHis').'.sql';
        $directory = storage_path('app/public');
        $path = $request->file('database')->move($directory, $fileName);
        $photoURL = url('/'.$fileName);

        DbExport::create([
            'name' => $fileName,
            'path' => storage_path('app/public/'.$fileName),
            'size' => Storage::size('public/'.$fileName),
            'updated_at' => null
        ]);
        // $process->mustRun();
        return response()->json(['url' => $photoURL], 200);
    }
}
