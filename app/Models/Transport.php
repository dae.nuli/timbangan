<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transport extends Model
{
    protected $fillable = ['client_id', 'plate_number', 'weight'];

    protected $hidden = [
        'client_id', 'created_at', 'updated_at',
    ];
}
