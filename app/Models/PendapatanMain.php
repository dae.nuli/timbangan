<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PendapatanMain extends Model
{
    protected $fillable = ['name'];

    public function sub()
    {
        return $this->hasMany(PendapatanSub::class, 'pendapatan_main_id');
    }
}
