<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Pendapatan extends Model
{
    protected $fillable = ['pendapatan_sub_id','date','amount','note','created_by'];

    public function sub()
    {
        return $this->belongsTo(PendapatanSub::class, 'pendapatan_sub_id');
    }
    public function setAmountAttribute($val)
    {
        $this->attributes['amount'] = str_replace('.','',$val);
    }
    public static function boot()
    {
       parent::boot();
       static::creating(function ($model) {
            $model->created_by = Auth::id();
       });
    }
}
