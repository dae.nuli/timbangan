<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class BebanBiaya extends Model
{
    protected $fillable = ['bebanbiaya_sub_id','date','amount','note','created_by'];

    public function sub()
    {
        return $this->belongsTo(BebanBiayaSub::class, 'bebanbiaya_sub_id');
    }
    public function setAmountAttribute($val)
    {
        $this->attributes['amount'] = str_replace('.','',$val);
    }
    public static function boot()
    {
       parent::boot();
       static::creating(function ($model) {
            $model->created_by = Auth::id();
       });
    }
}
