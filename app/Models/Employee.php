<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $fillable = ['job_position_id', 'name', 'phone', 'address', 'bank_id', 'bank_account'];

    public function position()
    {
        return $this->belongsTo(JobPosition::class, 'job_position_id');
    }

    public function bank()
    {
        return $this->belongsTo(Bank::class);
    }
}
