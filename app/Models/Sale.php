<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    protected $fillable = [
        'invoice', 'client_id', 'products_id', 'transports_id', 'driver_name', 'bruto', 'bruto_asli', 'tara', 'netto', 'out_date', 'potongan', 'potongan_persen', 'potongan_wajib', 'potongan_sampah', 'potongan_tangkai', 'potongan_pasir', 'potongan_air', 'potongan_mutu', 'item_name',
        'item_price', 'client_discount', 'berat_bersih', 'total', 'note', 'status'
    ];

	// protected $hidden = [
 //        'id', 'updated_at',
 //    ];
    public static function boot()
    {
        parent::boot();
        static::saving(function ($model) {
            if (!empty($model->berat_bersih)) {
                $model->total = ($model->item_price * $model->berat_bersih);
	            // $model->total = ($model->products->price * $model->berat_bersih);
            }
        });
    }

    public function client()
    {
    	return $this->belongsTo(Client::class);
    }

    public function transports()
    {
    	return $this->belongsTo(Transport::class);
    }

    public function products()
    {
    	return $this->belongsTo(Products::class);
    }
}
