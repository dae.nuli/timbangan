<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JobPosition extends Model
{
    protected $fillable = ['name', 'salary'];

    public function setSalaryAttribute($val)
    {
        $this->attributes['salary'] = str_replace('.','',$val);
    }
}
