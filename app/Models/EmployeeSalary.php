<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmployeeSalary extends Model
{
    protected $fillable = ['employee_id', 'salary', 'addition_salary'];

    public function employee()
    {
        return $this->belongsTo(Employee::class, 'employee_id');
    }

    public function employeeStatus()
    {
        return $this->hasMany(EmployeeSalaryStatus::class, 'employee_salary_id');
    }

    public function salaryStatus($id)
    {
        return EmployeeSalaryStatus::where('employee_salary_id', $id)
        ->whereMonth('created_at', date('m'))
        ->whereYear('created_at', date('Y'))
        ->count();
    }

    public function salaryDate($id)
    {
        $data = EmployeeSalaryStatus::where('employee_salary_id', $id)
        ->whereMonth('created_at', date('m'))
        ->whereYear('created_at', date('Y'))
        ->first();
        if ($data) {
            return $data->created_at->format('d F Y, H:i:s');
        } else {
            return 0;
        }
    }
}
