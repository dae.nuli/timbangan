<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    protected $fillable = ['name', 'unit', 'qty', 'price', 'type'];

    public function setPriceAttribute($val)
    {
        $this->attributes['price'] = str_replace('.','',$val);
    }
}
