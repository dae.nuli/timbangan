<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DbExport extends Model
{
    protected $fillable = [
        'name', 'path', 'size', 'updated_at'
    ];
}
