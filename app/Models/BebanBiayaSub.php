<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BebanBiayaSub extends Model
{
    protected $fillable = ['bebanbiaya_main_id' ,'name'];

    public function main()
    {
        return $this->belongsTo(BebanBiayaMain::class, 'bebanbiaya_main_id');
    }
}
