<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PenimbanganCpo extends Model
{
    protected $fillable = [
		'client_id', 'products_id', 'transports_id', 'driver_name', 'bruto', 'do_name', 'bruto_asli', 'tara', 'netto', 
		'out_date', 'potongan', 'potongan_ffa', 'potongan_air', 'potongan_kotoran', 'jumlah_segel', 'segel', 
		'berat_bersih', 'note'
	];

    public function client()
    {
    	return $this->belongsTo(Client::class);
    }

    public function transports()
    {
    	return $this->belongsTo(Transport::class);
    }
}
