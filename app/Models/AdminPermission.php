<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdminPermission extends Model
{
    protected $primaryKey = null;
	public $timestamps = false;
    public $incrementing = false;

    protected $fillable = [
        'user_id', 'permission_id'
    ];
}
