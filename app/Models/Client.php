<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $fillable = ['name', 'discount', 'name', 'address', 'phone', 'bank_id', 'bank_number', 'discount'];

    public function truck()
    {
        return $this->hasMany(Transport::class, 'client_id');
    }

    public function bank()
    {
        return $this->belongsTo(Bank::class, 'bank_id');
    }
}
