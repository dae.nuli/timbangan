<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BebanBiayaMain extends Model
{
    protected $fillable = ['name'];

    public function sub()
    {
        return $this->hasMany(BebanBiayaSub::class, 'bebanbiaya_main_id');
    }
}
