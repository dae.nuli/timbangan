<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmployeeSalaryStatus extends Model
{
    protected $fillable = ['employee_salary_id', 'status'];
}
