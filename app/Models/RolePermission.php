<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RolePermission extends Model
{
    protected $primaryKey = null;
	public $timestamps = false;
    public $incrementing = false;

    protected $fillable = [
        'role_id', 'permission_id'
    ];
}
