<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BankAccount extends Model
{
    protected $fillable = ['bank_id', 'owner', 'number'];

    public function bank()
    {
        return $this->belongsTo(Bank::class);
    }
}
