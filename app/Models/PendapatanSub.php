<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PendapatanSub extends Model
{
    protected $fillable = ['pendapatan_main_id','name'];

    public function main()
    {
        return $this->belongsTo(PendapatanMain::class, 'pendapatan_main_id');
    }
}
