<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class PaymentHistory extends Model
{
    public function timbang()
    {
        return $this->belongsTo(PenimbanganTbs::class, 'penimbangan_tbs_id');
    }
    public function user()
    {
        return $this->belongsTo(User::class, 'process_by');
    }
    public function account()
    {
        return $this->belongsTo(BankAccount::class, 'bank_account_id');
    }
    public function setAmountAttribute($val)
    {
        $this->attributes['amount'] = str_replace('.','',$val);
    }
}
