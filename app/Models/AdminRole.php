<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdminRole extends Model
{
    protected $primaryKey = null;
	public $timestamps = false;
    public $incrementing = false;

    protected $fillable = [
        'user_id', 'role_id'
    ];

    public function role()
    {
        return $this->belongsTo(Role::class, 'role_id');
    }
}
