<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PenimbanganFruit extends Model
{
    protected $fillable = ['products_id', 'berat', 'note'];

	// protected $hidden = [
 //        'id', 'updated_at',
 //    ];
    public static function boot()
    {
        parent::boot();
        static::saving(function ($model) {
	            $model->total = ($model->products->price * $model->berat);
        });
    }

    // public function client()
    // {
    // 	return $this->belongsTo(Client::class);
    // }

    public function products()
    {
    	return $this->belongsTo(Products::class);
    }
}
