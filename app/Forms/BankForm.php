<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;
use App\Models\Bank;

class BankForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('bank_id', 'select', [
                'choices' => Bank::pluck('name', 'id')->toArray(),
                'empty_value' => '- Please Select -',
                'label' => 'Nama Bank',
                'attr' => [
                    'data-validation' => 'required',
                    'class' => 'select2 form-control'
                ]
            ])
            ->add('owner', 'text', [
                'label' => 'Nama Pemilik',
                'attr' => ['data-validation' => 'required']
            ])
            ->add('number', 'text', [
                'label' => 'Nomor Rekening',
                'attr' => ['data-validation' => 'required']
            ]);
    }
}
