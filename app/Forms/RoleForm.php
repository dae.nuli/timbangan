<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;
use App\Models\Permission;

class RoleForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('name', 'text', [
                'attr' => ['data-validation' => 'required']
            ])
            ->add('description', 'text', [
                'attr' => ['data-validation' => 'required']
            ])
            ->add('role', 'text', [
                'template' => 'admin.role.role_permission',
                'value' => function ($val) {
                    return Permission::pluck('description', 'id')->toArray();
                }
            ]);
    }
}
