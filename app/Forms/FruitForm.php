<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;
use App\Models\Products;
class FruitForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('products_id', 'select', [
                'choices' => Products::where('type', 3)->pluck('name', 'id')->toArray(),
                'empty_value' => '- Please Select -',
                'label' => 'Products',
                'attr' => [
                    'data-validation' => 'required',
                    'class' => 'select2 form-control'
                ]
            ])
            ->add('berat', 'text', [
                'attr' => [
                	'data-validation' => 'required',
                    'class' => 'number form-control'
                ]
            ])
            ->add('note', 'text', [
                'attr' => ['data-validation' => '']
            ]);
    }
}
