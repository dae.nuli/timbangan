<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;
use App\Models\BebanBiayaMain;
use App\Models\BebanBiayaSub;

class BebanBiayaForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('bebanbiaya_sub_id', 'select', [
                'choices' => JobPosition::pluck('name', 'id')->toArray(),
                'empty_value' => '- Please Select -',
                'label' => 'Jabatan',
                'attr' => [
                    'data-validation' => 'required',
                    'class' => 'select2 form-control'
                ]
            ])
            ->add('date', 'text', [
                'attr' => ['data-validation' => 'required']
            ])
            ->add('amount', 'text', [
                'attr' => ['data-validation' => 'required']
            ])
            ->add('note', 'text', [
                'attr' => ['data-validation' => 'required']
            ])
            ->add('bank_id', 'select', [
                'choices' => Bank::pluck('name', 'id')->toArray(),
                'empty_value' => '- Please Select -',
                'label' => 'Bank',
                'attr' => [
                    'data-validation' => 'required',
                    'class' => 'select2 form-control'
                ]
            ])
            ->add('bank_account', 'text', [
                'attr' => [
                	'data-validation' => 'required',
                    'class' => 'form-control'
                ]
            ]);
    }
}
