<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class JobPositionForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('name', 'text', [
                'label' => 'Nama Jabatan',
                'attr' => ['data-validation' => 'required']
            ])
            ->add('salary', 'text', [
                'label' => 'Gaji',
                'attr' => [
                	'data-validation' => 'required',
                    'class' => 'number form-control'
                ]
            ]);
    }
}
