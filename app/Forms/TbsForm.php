<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class TbsForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('name', 'text', [
                'attr' => ['data-validation' => 'required']
            ])
            ->add('qty', 'text', [
                'attr' => ['data-validation' => 'required']
            ])
            ->add('unit', 'text', [
                'attr' => ['data-validation' => 'required']
            ])
            ->add('price', 'text', [
                'attr' => [
                	'data-validation' => 'required',
                    'class' => 'number form-control'
                ]
            ]);
    }
}
