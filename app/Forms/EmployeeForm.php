<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;
use App\Models\JobPosition;
use App\Models\Bank;

class EmployeeForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('job_position_id', 'select', [
                'choices' => JobPosition::pluck('name', 'id')->toArray(),
                'empty_value' => '- Please Select -',
                'label' => 'Jabatan',
                'attr' => [
                    'data-validation' => 'required',
                    'class' => 'select2 form-control'
                ]
            ])
            ->add('name', 'text', [
                'attr' => ['data-validation' => 'required']
            ])
            ->add('phone', 'text', [
                'attr' => ['data-validation' => 'required']
            ])
            ->add('address', 'text', [
                'attr' => ['data-validation' => 'required']
            ])
            ->add('bank_id', 'select', [
                'choices' => Bank::pluck('name', 'id')->toArray(),
                'empty_value' => '- Please Select -',
                'label' => 'Bank',
                'attr' => [
                    'data-validation' => 'required',
                    'class' => 'select2 form-control'
                ]
            ])
            ->add('bank_account', 'text', [
                'attr' => [
                	'data-validation' => 'required',
                    'class' => 'form-control'
                ]
            ]);
    }
}
