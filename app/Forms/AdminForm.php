<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;
use App\Models\Role;

class AdminForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('name', 'text', [
                'attr' => ['data-validation' => 'required']
            ])
            ->add('email', 'text', [
                'attr' => ['data-validation' => 'required']
            ])
            ->add('password', 'password', [
                'attr' => ['data-validation' => 'required']
            ])
            // ->add('role', 'text', [
            //     'template' => 'admin.admin.role'
            // ])
            ->add('role_id', 'select', [
                'choices' => Role::pluck('name', 'id')->toArray(),
                // 'empty_value' => '- Please Select -',
                'label' => 'Role',
                'attr' => [
                    'data-validation' => 'required',
                    'class' => 'select2 form-control',
                    'multiple' => 'multiple'
                ]
            ])
            ->add('status', 'choice', [
                'choices' => [1 => 'ACTIVE', 0 => 'NOT ACTIVE'],
                'choice_options' => [
                    'wrapper' => ['class' => 'radio status'],
                    'label_attr' => ['class' => ''],
                ],
                'attr' => ['data-validation' => 'required'],
                'selected' => [0],
                'expanded' => true,
                'multiple' => false
            ]);
    }
}
