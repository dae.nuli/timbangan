<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class ProductForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('name', 'text', [
                'attr' => ['data-validation' => 'required']
            ])
            ->add('qty', 'text', [
                'attr' => ['data-validation' => 'required']
            ])
            ->add('unit', 'text', [
                'attr' => ['data-validation' => 'required']
            ])
            ->add('price', 'text', [
                'attr' => [
                	'data-validation' => 'required',
                    'class' => 'number form-control'
                ]
            ])
            ->add('type', 'choice', [
                'choices' => [1 => 'TBS', 2 => 'CPO', 3 => 'BUAH'],
                'choice_options' => [
                    'wrapper' => ['class' => 'radio type'],
                    'label_attr' => ['class' => ''],
                ],
                'attr' => ['data-validation' => 'required'],
                'selected' => [1],
                'expanded' => true,
                'multiple' => false
            ]);
    }
}
