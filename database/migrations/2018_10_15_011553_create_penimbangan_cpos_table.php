<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePenimbanganCposTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penimbangan_cpos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('client_id')->nullable();
            $table->integer('products_id')->nullable();
            $table->integer('transports_id')->nullable();
            $table->string('driver_name')->nullable();
            $table->integer('bruto')->nullable();
            $table->string('do_name')->nullable();
            $table->integer('bruto_asli')->nullable();
            $table->integer('tara')->nullable();
            $table->integer('netto')->nullable();
            $table->dateTime('out_date')->nullable();
            $table->integer('potongan')->nullable();
            $table->decimal('potongan_ffa', 11, 2)->nullable();
            $table->decimal('potongan_air', 11, 2)->nullable();
            $table->decimal('potongan_kotoran', 11, 2)->nullable();
            $table->integer('jumlah_segel')->nullable();
            $table->text('segel')->nullable();
            $table->integer('berat_bersih')->nullable();
            $table->text('note')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penimbangan_cpos');
    }
}
