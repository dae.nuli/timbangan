<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePenimbanganTbsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penimbangan_tbs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('invoice')->nullable();
            $table->integer('client_id')->nullable();
            $table->integer('products_id')->nullable();
            $table->integer('transports_id')->nullable();
            $table->string('driver_name')->nullable();
            $table->integer('bruto')->nullable();
            $table->integer('bruto_asli')->nullable();
            $table->integer('tara')->nullable();
            $table->integer('netto')->nullable();
            $table->dateTime('out_date')->nullable();
            $table->integer('potongan')->nullable();
            $table->decimal('potongan_persen', 11, 2)->nullable();
            $table->decimal('potongan_wajib', 11, 2)->nullable();
            $table->decimal('potongan_sampah', 11, 2)->nullable();
            $table->decimal('potongan_tangkai', 11, 2)->nullable();
            $table->decimal('potongan_pasir', 11, 2)->nullable();
            $table->decimal('potongan_air', 11, 2)->nullable();
            $table->decimal('potongan_mutu', 11, 2)->nullable();
            // $table->integer('potongan_mentah')->nullable();
            // $table->integer('potongan_busuk')->nullable();
            // $table->integer('potongan_tankos')->nullable();
            // $table->integer('other_potongan')->nullable();
            // $table->decimal('potongan_brondol', 11, 2)->nullable();
            // $table->decimal('potongan_dura', 11, 2)->nullable();
            // $table->integer('jumlah_tandan')->nullable();
            // $table->integer('berat_tandan')->nullable();
            $table->integer('client_discount')->nullable();
            $table->string('item_name')->nullable();
            $table->integer('item_price')->nullable();
            $table->integer('berat_bersih')->nullable();
            $table->integer('total')->nullable();
            $table->text('note')->nullable();
            $table->enum('status',['in', 'out'])->default('in');
            $table->integer('payment_status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penimbangan_tbs');
    }
}
