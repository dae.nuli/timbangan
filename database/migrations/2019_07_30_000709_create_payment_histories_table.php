<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('penimbangan_tbs_id');
            $table->integer('process_by'); // User Who process this payment.
            // $table->integer('');
            $table->enum('type', [1, 2]); // 1. Cash, 2. Transfer
            $table->integer('bank_id')->default(0); // Bernilai 0 apabila cash.
            $table->integer('amount'); // Total bayar
            $table->integer('change')->nullable(); // Uang kembalian
            $table->dateTime('payment_date')->nullable();
            $table->integer('bank_account_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_histories');
    }
}
