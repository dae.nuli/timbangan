<?php

use Illuminate\Database\Seeder;
use App\Models\RolePermission;
use App\Models\Permission;
use App\Models\Role;

class RolePermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	RolePermission::truncate();

        $pm = Permission::all();
        $rl = Role::where('slug', 'administrator')->first();

        $kp = Role::where('slug', 'kasir_pembayaran')->first();
        $pm_kp = Permission::whereIn('name', ['tbs_index','tbs_payment'])->get();

    	$kt = Role::where('slug', 'kasir_timbangan')->first();
        $pm_kt = Permission::whereIn('name', ['tbs_index','tbs_process'])->get();

    	foreach ($pm as $key => $value) {
	        $rp = new RolePermission;
	        $rp->role_id = $rl->id;
	        $rp->permission_id = $value->id;
	        $rp->save();
    	}
        foreach ($pm_kp as $key => $value) {
	        $rp = new RolePermission;
	        $rp->role_id = $kp->id;
	        $rp->permission_id = $value->id;
	        $rp->save();
        }
        foreach ($pm_kt as $key => $value) {
	        $rp = new RolePermission;
	        $rp->role_id = $kt->id;
	        $rp->permission_id = $value->id;
	        $rp->save();
        }
    }
}
