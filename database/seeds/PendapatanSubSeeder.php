<?php

use Illuminate\Database\Seeder;
use App\Models\PendapatanSub;

class PendapatanSubSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	PendapatanSub::truncate();
        PendapatanSub::insert([
        	[
                'pendapatan_main_id' => 1,
                'name' => 'Penjualan Barang'
        	],
        	[
                'pendapatan_main_id' => 2,
	        	'name' => 'Biaya Pengiriman (Ongkir)'
        	],
        	[
                'pendapatan_main_id' => 2,
	        	'name' => 'Diskon Biaya Pengiriman'
        	],
        	[
                'pendapatan_main_id' => 2,
	        	'name' => 'Potongan Pembelian'
        	],
        	[
                'pendapatan_main_id' => 2,
	        	'name' => 'Pendapatan Bunga'
        	]
        ]);
    }
}
