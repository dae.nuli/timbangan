<?php

use Illuminate\Database\Seeder;
use App\Models\JobPosition;

class JobPositionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	JobPosition::truncate();
        JobPosition::insert([
        	[
                'name' => 'Manager',
	        	'salary' => 2000000,
				'created_at' => date('Y-m-d H:i:s')
        	],
        	[
	        	'name' => 'Operator',
	        	'salary' => 1500000,
				'created_at' => date('Y-m-d H:i:s')
        	]
        ]);
    }
}
