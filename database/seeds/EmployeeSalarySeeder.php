<?php

use Illuminate\Database\Seeder;
use App\Models\EmployeeSalary;

class EmployeeSalarySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	EmployeeSalary::truncate();
        EmployeeSalary::insert([
        	[
                'employee_id' => 1,
                'salary' => 2000000,
                'addition_salary' => 2000000,
				'created_at' => date('Y-m-d H:i:s')
        	],
            [
                'employee_id' => 2,
                'salary' => 1500000,
                'addition_salary' => 1500000,
				'created_at' => date('Y-m-d H:i:s')
        	],
            [
                'employee_id' => 3,
                'salary' => 1500000,
                'addition_salary' => 1500000,
				'created_at' => date('Y-m-d H:i:s')
        	]
        ]);
    }
}
