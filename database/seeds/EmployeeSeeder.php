<?php

use Illuminate\Database\Seeder;
use App\Models\Employee;

class EmployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	Employee::truncate();
        Employee::insert([
        	[
                'job_position_id' => 1,
                'name' => 'Alam Guntur',
                'phone' => '12346',
                'address' => 'JL. Lowanu',
                'bank_id' => 1,
                'bank_account' => '212117',
				'created_at' => date('Y-m-d H:i:s')
        	],
            [
                'job_position_id' => 2,
                'name' => 'Mizan Rizqi',
                'phone' => '12346213',
                'address' => 'JL. Tamsis',
                'bank_id' => 2,
                'bank_account' => '007008',
				'created_at' => date('Y-m-d H:i:s')
        	],
            [
                'job_position_id' => 2,
                'name' => 'Fuad Bakhtiar',
                'phone' => '12346',
                'address' => 'JL. Gitogati',
                'bank_id' => 3,
                'bank_account' => '748569123',
				'created_at' => date('Y-m-d H:i:s')
        	]
        ]);
    }
}
