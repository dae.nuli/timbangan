<?php

use Illuminate\Database\Seeder;
use App\Models\BebanBiayaMain;

class BebanBiayaMainSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	BebanBiayaMain::truncate();
        BebanBiayaMain::insert([
        	[
                'id' => 1,
                'name' => 'Beban Usaha atau Operasi'
        	],
        	[
                'id' => 2,
	        	'name' => 'Beban Penjualan'
        	],
        	[
                'id' => 3,
	        	'name' => 'Beban Lain-lain'
        	]
        ]);
    }
}
