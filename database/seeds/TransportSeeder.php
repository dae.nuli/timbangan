<?php

use Illuminate\Database\Seeder;
use App\Models\Transport;
class TransportSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	Transport::truncate();
        Transport::insert([
        	[
        		'client_id' => 1,
				'plate_number' => 'AB1234EQ',
				'weight' => 30,
				'created_at' => date('Y-m-d H:i:s')
        	],
            [
                'client_id' => 1,
                'plate_number' => 'AD3262ZE',
                'weight' => 30,
                'created_at' => date('Y-m-d H:i:s')
            ],
            // [
            //     'client_id' => 1,
            //     'plate_number' => 'AA9484ET',
            //     'weight' => 30,
            //     'created_at' => date('Y-m-d H:i:s')
            // ],
            [
                'client_id' => 2,
                'plate_number' => 'KT3849SW',
                'weight' => 30,
                'created_at' => date('Y-m-d H:i:s')
            ],
            [
                'client_id' => 2,
                'plate_number' => 'DD3740UT',
                'weight' => 30,
                'created_at' => date('Y-m-d H:i:s')
            ]
        ]);
    }
}
