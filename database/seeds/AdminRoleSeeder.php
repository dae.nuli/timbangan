<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Models\Role;
use App\Models\AdminRole;

class AdminRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ad = User::find(1);
        $u_kt = User::find(2); //Kasir Timbangan
        $u_kp = User::find(3); //Kasir Pembayaran
    	$u_kpen = User::find(4); //Kasir Penjualan

        $rl = Role::where('slug', 'administrator')->first();
        $kt = Role::where('slug', 'kasir_timbangan')->first();
        $kp = Role::where('slug', 'kasir_pembayaran')->first();
        $kpen = Role::where('slug', 'kasir_penjualan')->first();
    	AdminRole::truncate();
        AdminRole::insert([
            [
                'user_id' => $ad->id,
                'role_id' => $rl->id,
            ],
            [
                'user_id' => $u_kt->id,
                'role_id' => $kt->id,
            ],
            [
                'user_id' => $u_kp->id,
                'role_id' => $kp->id,
            ],
            [
                'user_id' => $u_kpen->id,
                'role_id' => $kpen->id,
            ]
        ]);
    }
}
