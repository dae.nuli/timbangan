<?php

use Illuminate\Database\Seeder;
use App\Models\BebanBiayaSub;

class BebanBiayaSubSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	BebanBiayaSub::truncate();
        BebanBiayaSub::insert([
        	[
                'bebanbiaya_main_id' => 1,
                'name' => 'Beban Gaji Karyawan'
        	],
        	[
                'bebanbiaya_main_id' => 1,
	        	'name' => 'Beban Biaya Pengiriman'
        	],
        	[
                'bebanbiaya_main_id' => 1,
	        	'name' => 'Beban Listrik, Telepon dan Air'
        	],
        	[
                'bebanbiaya_main_id' => 1,
	        	'name' => 'Beban Angkutan'
        	],
        	[
                'bebanbiaya_main_id' => 1,
	        	'name' => 'Beban Perlengkapan'
        	],
        	[
                'bebanbiaya_main_id' => 2,
	        	'name' => 'Return Penjualan'
        	],
        	[
                'bebanbiaya_main_id' => 2,
	        	'name' => 'Beban Iklan'
        	],
        	[
                'bebanbiaya_main_id' => 3,
	        	'name' => 'Beban Pajak Penjualan'
        	],
        	[
                'bebanbiaya_main_id' => 3,
	        	'name' => 'Beban Bunga'
        	]
        ]);
    }
}
