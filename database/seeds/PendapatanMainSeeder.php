<?php

use Illuminate\Database\Seeder;
use App\Models\PendapatanMain;

class PendapatanMainSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	PendapatanMain::truncate();
        PendapatanMain::insert([
        	[
                'id' => 1,
                'name' => 'Penjualan'
        	],
        	[
                'id' => 2,
	        	'name' => 'Pendapatan Lain-lain'
        	]
        ]);
    }
}
