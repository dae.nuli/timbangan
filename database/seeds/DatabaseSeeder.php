<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(ClientSeeder::class);
        $this->call(TransportSeeder::class);
        $this->call(ProductSeeder::class);
        $this->call(AboutSeeder::class);
        $this->call(BankSeeder::class);
        $this->call(JobPositionSeeder::class);
        $this->call(EmployeeSeeder::class);
        $this->call(EmployeeSalarySeeder::class);
        $this->call(BebanBiayaMainSeeder::class);
        $this->call(BebanBiayaSubSeeder::class);
        $this->call(PendapatanMainSeeder::class);
        $this->call(PendapatanSubSeeder::class);
        $this->call(BankAccountSeeder::class);

        $this->call(RoleSeeder::class);
        $this->call(PermissionSeeder::class);
        $this->call(RolePermissionSeeder::class);
        $this->call(AdminRoleSeeder::class);
    }
}
