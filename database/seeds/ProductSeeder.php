<?php

use Illuminate\Database\Seeder;
use App\Models\Products;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Products::truncate();
        Products::insert([
        	[
	        	'name' => 'Kelapa Sawit',
	        	'qty' => 1,
	        	'unit' => 'Kg',
    				'price' => 20000,
            'type' => 1,
    				'created_at' => date('Y-m-d H:i:s')
        	],
        	[
	        	'name' => 'Batu Bara',
	        	'qty' => 1,
	        	'unit' => 'Kg',
    				'price' => 30000,
            'type' => 1,
    				'created_at' => date('Y-m-d H:i:s')
        	],
        	[
	        	'name' => 'Apel',
	        	'qty' => 1,
	        	'unit' => 'Kg',
    				'price' => 20000,
            'type' => 3,
    				'created_at' => date('Y-m-d H:i:s')
        	],
        	[
	        	'name' => 'Anggur',
	        	'qty' => 1,
	        	'unit' => 'Kg',
    				'price' => 50000,
            'type' => 3,
    				'created_at' => date('Y-m-d H:i:s')
        	]
        ]);
    }
}
