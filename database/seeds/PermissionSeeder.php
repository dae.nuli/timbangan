<?php

use Illuminate\Database\Seeder;
use App\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	Permission::truncate();
        Permission::insert([
        	[
	        	'name' => 'tbs_index',
				'description' => 'Menu Pembelian - List'
        	],
        	[
	        	'name' => 'tbs_create',
				'description' => 'Menu Pembelian - Create'
        	],
        	[
	        	'name' => 'tbs_edit',
				'description' => 'Menu Pembelian - Edit'
        	],
            [
                'name' => 'tbs_process',
                'description' => 'Menu Pembelian - Proses'
            ],
            [
                'name' => 'tbs_payment',
                'description' => 'Menu Pembelian - Payment'
            ],
            [
                'name' => 'tbs_delete',
                'description' => 'Menu Pembelian - Delete'
            ],
            [
                'name' => 'tbs_print',
                'description' => 'Menu Pembelian - Print'
            ],

        	[
	        	'name' => 'sale_index',
				'description' => 'Menu Penjualan - List'
        	],
        	[
	        	'name' => 'sale_create',
				'description' => 'Menu Penjualan - Create'
        	],
        	[
	        	'name' => 'sale_edit',
				'description' => 'Menu Penjualan - Edit'
        	],
            [
                'name' => 'sale_process',
                'description' => 'Menu Penjualan - Proses'
            ],
            [
                'name' => 'sale_delete',
                'description' => 'Menu Penjualan - Delete'
            ],

        	[
	        	'name' => 'salary_index',
				'description' => 'Menu Penggajian - List'
        	],
        	[
	        	'name' => 'salary_done',
				'description' => 'Menu Penggajian - Proses'
        	],

        	[
	        	'name' => 'product_index',
				'description' => 'Menu Product - List'
        	],
        	[
	        	'name' => 'product_create',
				'description' => 'Menu Product - Create'
        	],
        	[
	        	'name' => 'product_edit',
				'description' => 'Menu Product - Edit'
        	],
            [
                'name' => 'product_delete',
                'description' => 'Menu Product - Delete'
            ],

        	[
	        	'name' => 'bank_index',
				'description' => 'Menu Bank Account - List'
        	],
        	[
	        	'name' => 'bank_create',
				'description' => 'Menu Bank Account - Create'
        	],
        	[
	        	'name' => 'bank_edit',
				'description' => 'Menu Bank Account - Edit'
        	],
            [
                'name' => 'bank_delete',
                'description' => 'Menu Bank Account - Delete'
            ],

        	[
	        	'name' => 'client_index',
				'description' => 'Menu Suplier - List'
        	],
        	[
	        	'name' => 'client_create',
				'description' => 'Menu Suplier - Create'
        	],
        	[
	        	'name' => 'client_edit',
				'description' => 'Menu Suplier - Edit'
        	],
        	[
	        	'name' => 'client_detail',
				'description' => 'Menu Suplier - Detail'
        	],
            [
                'name' => 'client_delete',
                'description' => 'Menu Suplier - Delete'
            ],

            [
                'name' => 'job_position_index',
                'description' => 'Menu Jabatan - List'
            ],
            [
                'name' => 'job_position_create',
                'description' => 'Menu Jabatan - Create'
            ],
            [
                'name' => 'job_position_edit',
                'description' => 'Menu Jabatan - Edit'
            ],
            [
                'name' => 'job_position_delete',
                'description' => 'Menu Jabatan - Delete'
            ],

            [
                'name' => 'employee_index',
                'description' => 'Menu Karyawan - List'
            ],
            [
                'name' => 'employee_create',
                'description' => 'Menu Karyawan - Create'
            ],
            [
                'name' => 'employee_edit',
                'description' => 'Menu Karyawan - Edit'
            ],
            [
                'name' => 'employee_delete',
                'description' => 'Menu Karyawan - Delete'
            ],

            [
                'name' => 'beban_biaya_index',
                'description' => 'Menu Beban Biaya - List'
            ],
            [
                'name' => 'beban_biaya_create',
                'description' => 'Menu Beban Biaya - Create'
            ],
            [
                'name' => 'beban_biaya_edit',
                'description' => 'Menu Beban Biaya - Edit'
            ],
            [
                'name' => 'beban_biaya_delete',
                'description' => 'Menu Beban Biaya - Delete'
            ],

            [
                'name' => 'pendapatan_index',
                'description' => 'Menu Pendapatan - List'
            ],
            [
                'name' => 'pendapatan_create',
                'description' => 'Menu Pendapatan - Create'
            ],
            [
                'name' => 'pendapatan_edit',
                'description' => 'Menu Pendapatan - Edit'
            ],
            [
                'name' => 'pendapatan_delete',
                'description' => 'Menu Pendapatan - Delete'
            ],
            [
                'name' => 'data_perkiraan_index',
                'description' => 'Menu Data Perkiraan - List'
            ],

            [
                'name' => 'pembelian_report_index',
                'description' => 'Menu Laporan Pembelian - List'
            ],
            [
                'name' => 'pembelian_report_detail',
                'description' => 'Menu Laporan Pembelian - Detail'
            ],

            [
                'name' => 'penjualan_report_index',
                'description' => 'Menu Laporan Penjualan - List'
            ],
            [
                'name' => 'penjualan_report_detail',
                'description' => 'Menu Laporan Penjualan - Detail'
            ],

            [
                'name' => 'pendapatan_report_index',
                'description' => 'Menu Laporan Pendapatan - List'
            ],
            [
                'name' => 'pendapatan_report_detail',
                'description' => 'Menu Laporan Pendapatan - Detail'
            ],

            [
                'name' => 'beban_biaya_report_index',
                'description' => 'Laporan Beban Biaya - List'
            ],
            [
                'name' => 'beban_biaya_report_detail',
                'description' => 'Laporan Beban Biaya - Detail'
            ],

            [
                'name' => 'laba_rugi_report_index',
                'description' => 'Laporan Laba Rugi - List'
            ],

            [
                'name' => 'about_index',
                'description' => 'Menu About - List'
            ],
            [
                'name' => 'about_edit',
                'description' => 'Menu About - Edit'
            ],

            [
                'name' => 'backup_index',
                'description' => 'Menu Backup - List'
            ],
            [
                'name' => 'backup_send',
                'description' => 'Menu Backup - Send'
            ],
            [
                'name' => 'backup_prepare',
                'description' => 'Menu Backup - Prepare'
            ],

            [
                'name' => 'dbexport_index',
                'description' => 'Menu Sync - List'
            ],
            [
                'name' => 'dbexport_import',
                'description' => 'Menu Sync - Import'
            ],
            [
                'name' => 'dbexport_delete',
                'description' => 'Menu Sync - Delete'
            ],

            [
                'name' => 'role_index',
                'description' => 'Menu Role - List'
            ],
            [
                'name' => 'role_create',
                'description' => 'Menu Role - Create'
            ],
            [
                'name' => 'role_edit',
                'description' => 'Menu Role - Edit'
            ],
            [
                'name' => 'role_delete',
                'description' => 'Menu Role - Delete'
            ],

            [
                'name' => 'admin_index',
                'description' => 'Menu Admin - List'
            ],
            [
                'name' => 'admin_create',
                'description' => 'Menu Admin - Create'
            ],
            [
                'name' => 'admin_edit',
                'description' => 'Menu Admin - Edit'
            ],
            [
                'name' => 'admin_delete',
                'description' => 'Menu Admin - Delete'
            ],

            [
                'name' => 'permission_index',
                'description' => 'Menu Permission - List'
            ],
        ]);
    }
}
