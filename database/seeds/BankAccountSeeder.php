<?php

use Illuminate\Database\Seeder;
use App\Models\BankAccount;

class BankAccountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	BankAccount::truncate();
        BankAccount::insert([
        	[
                'bank_id' => 1,
                'owner' => 'Irham Panrita',
	        	'number' => '12354-456879-2158',
				'created_at' => date('Y-m-d H:i:s')
        	],
        	[
                'bank_id' => 2,
                'owner' => 'Nuli',
	        	'number' => '12451-875963-845216',
				'created_at' => date('Y-m-d H:i:s')
        	]
        ]);
    }
}
