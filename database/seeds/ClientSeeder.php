<?php

use Illuminate\Database\Seeder;
use App\Models\Client;

class ClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	Client::truncate();
        Client::insert([
        	[
                'name' => 'Nuli Giarsyani',
                'bank_id' => 1,
	        	'bank_number' => 1234567,
				'discount' => '20',
				'created_at' => date('Y-m-d H:i:s')
        	],
        	[
	        	'name' => 'Irham',
                'bank_id' => 1,
	        	'bank_number' => 1234567,
				'discount' => '10',
				'created_at' => date('Y-m-d H:i:s')
        	]
        ]);
    }
}
