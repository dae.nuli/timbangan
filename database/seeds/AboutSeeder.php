<?php

use Illuminate\Database\Seeder;
use App\Models\About;
class AboutSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	About::truncate();
        About::insert([
	        	'name' => 'Solichin',
                'email' => 'lolichin@gmail.com',
                'phone' => '0818184848488',
				'company' => 'PT Sawit',
                'created_at' => date('Y-m-d H:i:s')
        ]);
    }
}
