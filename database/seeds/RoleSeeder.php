<?php

use Illuminate\Database\Seeder;
use App\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	Role::truncate();
        Role::insert([
        	[
                'slug' => 'administrator',
	        	'name' => 'Administrator',
				'description' => 'Super User',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
        	],
        	[
                'slug' => 'kasir_timbangan',
	        	'name' => 'Kasir Timbangan',
				'description' => 'Kasir Timbangan',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
        	],
        	[
                'slug' => 'kasir_pembayaran',
	        	'name' => 'Kasir Pembayaran',
				'description' => 'Kasir Pembayaran',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
        	],
        	[
                'slug' => 'kasir_penjualan',
	        	'name' => 'Kasir Penjualan',
				'description' => 'Kasir Penjualan',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
        	]
        ]);
    }
}
