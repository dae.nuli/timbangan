<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	User::truncate();
        User::insert([
            [
                'id' =>1,
            	'name' => 'Nuli Giarsyani',
    			'email' => 'admin@example.com',
    			'password' => bcrypt(111111),
                'status' => 1,
    			'created_at' => date('Y-m-d H:i:s')
            ],
            [
                'id' =>2,
            	'name' => 'Kasir Timbangan',
    			'email' => 'timbangan@example.com',
    			'password' => bcrypt(111111),
                'status' => 1,
    			'created_at' => date('Y-m-d H:i:s')
            ],
            [
                'id' =>3,
            	'name' => 'Kasir Pembayaran',
    			'email' => 'pembayaran@example.com',
    			'password' => bcrypt(111111),
                'status' => 1,
    			'created_at' => date('Y-m-d H:i:s')
            ],
            [
                'id' =>4,
            	'name' => 'Kasir Penjualan',
    			'email' => 'penjualan@example.com',
    			'password' => bcrypt(111111),
                'status' => 1,
    			'created_at' => date('Y-m-d H:i:s')
            ]
        ]);
    }
}
