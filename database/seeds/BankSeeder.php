<?php

use Illuminate\Database\Seeder;
use App\Models\Bank;

class BankSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	Bank::truncate();
        Bank::insert([
        	[
	        	'name' => 'Mandiri',
				'created_at' => date('Y-m-d H:i:s')
        	],
        	[
	        	'name' => 'BCA',
				'created_at' => date('Y-m-d H:i:s')
        	],
        	[
	        	'name' => 'Mega',
				'created_at' => date('Y-m-d H:i:s')
        	],
        	[
	        	'name' => 'BNI',
				'created_at' => date('Y-m-d H:i:s')
        	],
        	[
	        	'name' => 'BRI',
				'created_at' => date('Y-m-d H:i:s')
        	],
        	[
	        	'name' => 'Muamalat',
				'created_at' => date('Y-m-d H:i:s')
        	],
        	[
	        	'name' => 'Permata',
				'created_at' => date('Y-m-d H:i:s')
        	],
        	[
	        	'name' => 'BRI syariah',
				'created_at' => date('Y-m-d H:i:s')
        	],
        	[
	        	'name' => 'Syariah Mandiri',
				'created_at' => date('Y-m-d H:i:s')
        	],
        	[
	        	'name' => 'Bukopin',
				'created_at' => date('Y-m-d H:i:s')
        	],
        	[
	        	'name' => 'Danamon',
				'created_at' => date('Y-m-d H:i:s')
        	],
        	[
	        	'name' => 'Bukopin Syariah',
				'created_at' => date('Y-m-d H:i:s')
        	],
        	[
	        	'name' => 'Danamon Syariah',
				'created_at' => date('Y-m-d H:i:s')
        	],
        	[
	        	'name' => 'Permata Syariah',
				'created_at' => date('Y-m-d H:i:s')
        	],
        	[
	        	'name' => 'BNI Syariah',
				'created_at' => date('Y-m-d H:i:s')
        	]
        ]);
    }
}
