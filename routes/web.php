<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    // return env('SEND_FILE_URL');
    return view('admin.auth.login');
    // dd(curl_version());
});

Route::get('/display', function () {
    return view('display');
});

Route::get('file', 'Admin\TbsController@getFile');
Route::post('sendFile', 'SendFileController@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['auth']], function () {

	Route::get('products/data', 'Admin\ProductController@data')->name('products.data');
	Route::resource('products', 'Admin\ProductController');

	Route::get('client/data', 'Admin\ClientController@data')->name('client.data');
	Route::resource('client', 'Admin\ClientController');

	Route::get('bank/data', 'Admin\BankAccountController@data')->name('bank.data');
	Route::resource('bank', 'Admin\BankAccountController');

	Route::get('position/data', 'Admin\JobPositionController@data')->name('position.data');
	Route::resource('position', 'Admin\JobPositionController');

	Route::get('employee/data', 'Admin\EmployeeController@data')->name('employee.data');
	Route::resource('employee', 'Admin\EmployeeController');

    Route::get('sales/struk/{id}', 'Admin\SaleController@printStruk')->name('sales.print.struk');
    Route::get('sales/print/{id}', 'Admin\SaleController@print')->name('sales.print');
    Route::get('sales/weight', 'Admin\SaleController@fetchWeight')->name('sales.weight.api');
    Route::post('sales/discount', 'Admin\SaleController@postDiscount')->name('sales.discount.api');
    Route::post('sales/truck', 'Admin\SaleController@postTruck')->name('sales.truck.api');
    Route::post('sales/process/{id}', 'Admin\SaleController@storeProcess');
    Route::get('sales/process/{id}', 'Admin\SaleController@process')->name('sales.process');
    Route::get('sales/data', 'Admin\SaleController@data')->name('sales.data');
    Route::resource('sales', 'Admin\SaleController');

    Route::get('salary/salaryDone/{id}', 'Admin\EmployeeSalaryStatusController@salaryDone')->name('salary.done');
	Route::get('salary/data', 'Admin\EmployeeSalaryStatusController@data')->name('salary.data');
	Route::resource('salary', 'Admin\EmployeeSalaryStatusController');

    Route::post('tbs/checkPayment', 'Admin\TbsController@postCheckPayment')->name('tbs.payment.check');
    Route::get('tbs/struk/{id}', 'Admin\TbsController@printStruk')->name('tbs.print.struk');
    Route::get('tbs/print/{id}', 'Admin\TbsController@print')->name('tbs.print');
    Route::post('tbs/payment/{id}', 'Admin\TbsController@payment')->name('tbs.payment');
    Route::get('tbs/paymentHistory/{id}', 'Admin\TbsController@paymentHistory')->name('tbs.payment.history');
    Route::post('tbs/weight', 'Admin\TbsController@fetchWeight')->name('tbs.weight.api');
    Route::post('tbs/discount', 'Admin\TbsController@postDiscount')->name('tbs.discount.api');
    Route::post('tbs/truck', 'Admin\TbsController@postTruck')->name('tbs.truck.api');
    Route::post('tbs/process/{id}', 'Admin\TbsController@storeProcess');
    Route::get('tbs/process/{id}', 'Admin\TbsController@process')->name('tbs.process');
    Route::get('tbs/data', 'Admin\TbsController@data')->name('tbs.data');
    Route::resource('tbs', 'Admin\TbsController');

    Route::get('fruit/data', 'Admin\FruitController@data')->name('fruit.data');
    Route::resource('fruit', 'Admin\FruitController');

    Route::get('bebanBiaya/data', 'Admin\BebanBiayaController@data')->name('bebanBiaya.data');
    Route::resource('bebanBiaya', 'Admin\BebanBiayaController');

    Route::get('pendapatan/data', 'Admin\PendapatanController@data')->name('pendapatan.data');
    Route::resource('pendapatan', 'Admin\PendapatanController');

    Route::get('dataPerkiraan', 'Admin\DataPerkiraanController@index')->name('perkiraan.index');

    Route::get('reportPembelian/struk/{id}', 'Admin\ReportPembelianController@printStruk')->name('reportPembelian.print.struk');
    Route::get('reportPembelian/print/{id}', 'Admin\ReportPembelianController@print')->name('reportPembelian.print');
    Route::get('reportPembelian/data', 'Admin\ReportPembelianController@data')->name('reportPembelian.data');
    Route::get('reportPembelian', 'Admin\ReportPembelianController@index')->name('reportPembelian.index');
    Route::get('reportPembelian/{id}', 'Admin\ReportPembelianController@show')->name('reportPembelian.show');
    Route::post('reportPembelian/download', 'Admin\ReportPembelianController@download')->name('reportPembelian.download');

    Route::get('reportPenjualan/struk/{id}', 'Admin\ReportPenjualanController@printStruk')->name('reportPenjualan.print.struk');
    Route::get('reportPenjualan/print/{id}', 'Admin\ReportPenjualanController@print')->name('reportPenjualan.print');
    Route::get('reportPenjualan/data', 'Admin\ReportPenjualanController@data')->name('reportPenjualan.data');
    Route::get('reportPenjualan', 'Admin\ReportPenjualanController@index')->name('reportPenjualan.index');
    Route::get('reportPenjualan/{id}', 'Admin\ReportPenjualanController@show')->name('reportPenjualan.show');
    Route::post('reportPenjualan/download', 'Admin\ReportPenjualanController@download')->name('reportPenjualan.download');
    // Route::resource('reportPembelian', 'Admin\ReportPembelianController');

    Route::get('reportLabaRugi', 'Admin\ReportLabaRugiController@index')->name('reportLabaRugi.index');


    Route::get('about', 'Admin\AboutController@index')->name('about.index');
    Route::get('about/edit', 'Admin\AboutController@edit')->name('about.edit');
    Route::post('about', 'Admin\AboutController@update')->name('about.update');

    Route::post('reportPendapatan/download', 'Admin\ReportPendapatanController@download')->name('reportPendapatan.download');
    Route::get('reportPendapatan/data', 'Admin\ReportPendapatanController@data')->name('reportPendapatan.data');
    Route::get('reportPendapatan', 'Admin\ReportPendapatanController@index')->name('reportPendapatan.index');
    Route::get('reportPendapatan/{id}', 'Admin\ReportPendapatanController@show')->name('reportPendapatan.show');

    Route::post('reportBebanBiaya/download', 'Admin\ReportBebanBiayaController@download')->name('reportBebanBiaya.download');
    Route::get('reportBebanBiaya/data', 'Admin\ReportBebanBiayaController@data')->name('reportBebanBiaya.data');
    Route::get('reportBebanBiaya', 'Admin\ReportBebanBiayaController@index')->name('reportBebanBiaya.index');
    Route::get('reportBebanBiaya/{id}', 'Admin\ReportBebanBiayaController@show')->name('reportBebanBiaya.show');
	// Route::get('cpo/data', 'Admin\CpoController@data')->name('cpo.data');
	// Route::resource('cpo', 'Admin\CpoController');
    Route::get('backup', 'Admin\BackupDataController@index')->name('backup.index');
    Route::get('backup/data', 'Admin\BackupDataController@data')->name('backup.data');
    Route::get('backup/prepare', 'Admin\BackupDataController@backup')->name('backup.backup');
    Route::get('backup/{id}/send', 'Admin\BackupDataController@send')->name('backup.send');

    Route::get('dbexport', 'Admin\ExportedDBController@index')->name('dbexport.index');
    Route::get('dbexport/data', 'Admin\ExportedDBController@data')->name('dbexport.data');
    Route::get('dbexport/{id}/import', 'Admin\ExportedDBController@import')->name('dbexport.import');
    Route::delete('dbexport/{id}', 'Admin\ExportedDBController@destroy')->name('dbexport.destroy');

    Route::get('permission/data', 'Admin\PermissionController@data')->name('permission.data');
    Route::resource('permission', 'Admin\PermissionController');

    Route::get('role/data', 'Admin\RoleController@data')->name('role.data');
    Route::resource('role', 'Admin\RoleController');

    Route::get('admin/data', 'Admin\AdminController@data')->name('admin.data');
    Route::resource('admin', 'Admin\AdminController');

    Route::get('api/productPrice/{id}', 'Admin\ProductController@getPrice');
    Route::get('api/client/{id}', 'Admin\ClientController@editApi');
	Route::get('api/tara', 'Admin\TbsController@fetchWeightTara');

});
