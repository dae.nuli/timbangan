<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('clients', 'Admin\ClientController@list');
Route::get('transport/{id}', 'Admin\TransportController@show');
Route::get('product', 'Admin\ProductController@listProduct');
Route::get('tbs/{id}', 'Admin\TbsController@showEdit');
Route::get('weight', 'Admin\TbsController@getFile');
Route::get('cpo/{id}', 'Admin\CpoController@showEdit');
